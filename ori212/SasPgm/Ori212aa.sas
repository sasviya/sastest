/* Test */
%JobDate4(Ori212a,Ori274aa,IMF-CONSTANT Master String,SC) ;
Title 'Ori212aa - Master String' ;
Options NoCenter Mprint Symbolgen Macrogen Source2 ;

Libname 	LIBRARY 	'/sasdata/supplychain/sastest/orm000'		Access=READONLY ;
Libname 	ORI212		'/sasdata/supplychain/sastest/ori212' ;
FileName 	RDC     	'/sasdata/supplychain/sastest/sasrdc' ; 

FileName 	ORS212 		'/sasfiles/ftp/ori212/ors212aa.txt' LRecl=4000 BlkSize=24000 Recfm=f ;

Options Source2 MLogic Symbolgen ;                                                        

Data Ors212 ;                              
  	Infile ORS212 ;                          
                                           
  	%Include RDC(ORS212PC) ;                 
  	Drop                                     
    	FimRecStat                             
    	FimStat                                
    	FimDesc                                
    	;                                      
Run ;                                      
                                           
Proc Sort Data=Ors212 Out=ORI212.Ors212 ;  
  	By FimNo ;                               
Run ;                                                                     
                                                                     
Libname ORI212 Clear ;
	
Data _null_ ;
Run ;

*ENDSAS ;
