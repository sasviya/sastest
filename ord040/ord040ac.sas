%JobDate4(Ord040ac,Ord040aa,IMF Con Prc SHF to CKB,SC) ;
Title 'Ord040ac - IMF Constant - Price and DHF to CKB CASM' ;

/*Testing pull to new path*/
%Let OrProdTest = PROD ;

%Let RootFolder = /sasdata/ord040/ ;			

Libname 	LIBRARY 	'/sasdata/orm000' 				Access=ReadOnly ; 
Libname 	SAVE    	"&RootFolder.Save" 										;
Libname 	ORI276  	'/sasdata/ori276'		 		Access=ReadOnly ; *price;
Libname 	ORI274  	'/sasdata/ori274' 				Access=ReadOnly ; *vendor;
Libname 	ORI802  	'/sasdata/ori802'	 			Access=ReadOnly ; *dhf;
Libname 	ORI295  	'/sasdata/ori295' 				Access=ReadOnly ;

**NOTE  -  Ori275 is called in two places (ori275, ord040) ;
*Filename 	ORS275   	'\\Usserv02\NOBK\Ors2\Ors275aa.txt' LRecl=2700 BlkSize=27000 recfm=f  ; *item;
* Filename 	ORS275   	'\\Usserv63\DOR\NOBK\Ors2\Ors275aa.txt' LRecl=2600 BlkSize=26000 recfm=f  ; *item;
FileName 	ORS275  	'/sasfiles/ftp/ori275/Ors275aa.txt' LRecl=2700 BlkSize=27000 Recfm=f ;*New FTP fileserver location;

Filename 	RDC      	'/sasdata/sasrdc' 								;
Filename 	PRTERR   	"/sasfiles/logs/ord040/Err.txt" 									;

Options FmtSearch=(SAVE Ori295) Source2 MPrint Symbolgen MLogic NoCenter Orientation=Portrait ;

* Feb 2005 - Added Pricing Categories (FricMajorDeptNo, etc.) ;
*   BZ     - Concatenated these 4 variables into 'Category' ;
* Mar 2007 - Adding MinOrddQty into the Space Planning Database ;
*   BL     - Used Value 10 ;
* May07 - BZ - Fixed pgm to base regions on Ors267 FimRgnId, not SRWCE ;
* 26Sep2011 - PO - Convert to use new Item file Ori275 and Price File Ori276 ; 
* 12Jun2018 - LL - Remapped fields for Category Advisor implementation ; 
* 22Aug2018 - LL - Set DBstatus by FimStat ; 

Data _null_ ;
	File PRTERR ;
	DateTime = DateTime() ;
	Put 
		DateTime=	DateTime. 
		;
Run ; 

*  READ IN Imf Constant ;
Data Ors275a ; 
  	Infile ORS275 ; 
  	File PRTERR Mod ;
	
  	Attrib
    	FimNo					Format=Z7.						Label='Fim No'
    	FimRecStat      		Format=$1.		Length=$1.  	Label='Fim Rec Stat'
		
    	FimStat         		Format=$1.      Length=$1.      Label='Fim Stat'
    	FimStatDate     		Format=Date9.                   Label='Fim Stat Date'
    	FdsDptNo        		Format=Deptn.                   Label='Fds Dpt No'
    	FdsSectNo       		Format=Z2.                      Label='Fds Sect No'
    	FdsGrpNo        		Format=Z3.                      Label='Fds Grp No'
    	FimSeqNo        		Format=Z4.                      Label='Fim Seq No'
    	FimDesc         		Format=$35.                     Label='Fim Desc'
    	FimStkNo        		Format=$18.                     Label='Fim Stk No'
    	FimFclPkQty     		Format=5.                       Label='Fim Fcl Pk Qty'
    	FimWholeNo      		Format=5.                       Label='Fim Whole No'
    	FimFractDesc    		Format=$5.  	Length=$5.      Label='Fim Fract Desc'
    	FimTypMeasCd    		Format=$2.      Length=$2.      Label='Fim Typ Meas Cd'
    	FimMeasCd           	Format=$11.     Length=$11.     Label='Fim Meas Cd'
    	FimCaseWholeNo      	Format=5.                       Label='Fim Case Whole No'
    	FimCaseFractDesc    	Format=$5.      Length=$5.      Label='Fim Case Fract Desc'
    	FimCaseTypMeasCd    	Format=$2.      Length=$2.      Label='Fim Case Typ Meas Cd'
    	FimUnitWght         	Format=8.2                      Label='Fim Unit Wght'
    	FimWghtScaleCd      	Format=$2.      Length=$2.      Label='Fim Wght Scale Cd'
    	FimCubeLgthDim      	Format=5.1                      Label='Fim Cube Lgth Dim'
    	FimCubeWidthDim     	Format=5.1                      Label='Fim Cube Width Dim'
    	FimCubeHighDim      	Format=5.1                      Label='Fim Cube High Dim'
    	FimCubeCd           	Format=$1.      Length=$1.      Label='Fim Cube Cd'
    	FimCubeMeasCd       	Format=$1.      Length=$1.      Label='Fim Cube Meas Cd'
    	FimCubeVolDim       	Format=9.2                      Label='Fim Cube Vol Dim'
    	FimMfrLgthDim       	Format=5.1                      Label='Fim Mfr Lgth Dim'
    	FimMfrWidthDim      	Format=5.1                      Label='Fim Mfr Width Dim'
    	FimMfrHighDim       	Format=5.1                      Label='Fim Mfr High Dim'
    	FimMfrCd            	Format=$1.		Length=$1.      Label='Fim Mfr Cd'
    	FimMfrQty           	Format=5.                       Label='Fim Mfr Qty'
    	FimMfrShpgUnitCd    	Format=$2.      Length=$2.      Label='Fim Mfr Shpg Unit Cd'
    	FimUnitLgthDim      	Format=5.1                      Label='Fim Unit Lgth Dim'
    	FimUnitWidthDim     	Format=5.1                      Label='Fim Unit Width Dim'
    	FimUnitHighDim      	Format=5.1                      Label='Fim Unit High Dim'
    	FimUnitCd           	Format=$1.      Length=$1.      Label='Fim Unit Cd'
    	FimSpeedToMktCd     	Format=$1.      Length=$1.      Label='Fim Speed To Mkt Cd'
    	FimSpeedToMktQty    	Format=5.                       Label='Fim Speed To Mkt Qty'
    	FimSpeedToMktDate   	Format=Date7.                   Label='Fim Speed To Mkt Date'
    	FimOrddUnitsCd      	Format=$1.      Length=$1.      Label='Fim Ordd Units Cd'
    	FimRtlUnitQty       	Format=Best5.                   Label='Fim Rtl Unit Qty'
    	FimRtlUnitCd        	Format=$2.      Length=$2.      Label='Fim Rtl Unit Cd'
    	FimCaseRtlUnitsQty  	Format=Best5.                   Label='Fim Case Rtl Units Qty'
    	FimLowCostCd        	Format=$1.      Length=$1.      Label='Fim Low Cost Cd'
    	FimBoCd             	Format=$1.      Length=$1.      Label='Fim Bo Cd'
    	FimRdrCd            	Format=$1.      Length=$1.      Label='Fim Rdr Cd'
    	FimWarrCd           	Format=$1.      Length=$1.      Label='Fim Warr Cd'
    	FimWarrAmt          	Format=7.2                      Label='Fim Warr Amt'
    	FimServFeeNo        	Format=1.                       Label='Fim Serv Fee No'
    	FimFrtAllowCd       	Format=$1.      Length=$1.      Label='Fim Frt Allow Cd'
    	FimLksSwlsCd        	Format=$1.      Length=$1.      Label='Fim Lks Swls Cd'
    	FimPartAdvCd        	Format=$1.      Length=$1.      Label='Fim Part Adv Cd'
    	FimTireCd           	Format=$1.      Length=$1.      Label='Fim Tire Cd'
    	FimRebateCd         	Format=$1.      Length=$1.      Label='Fim Rebate Cd'
    	FimRebatePcnt       	Format=5.                       Label='Fim Rebate Pcnt'
    	FimMstItemCd        	Format=$1.      Length=$1.      Label='Fim Mst Item Cd'
    	FimZonePrcCd        	Format=$2.      Length=$2.      Label='Fim Zone Prc Cd'
    	FdsSectModNo        	Format=Z2.                      Label='Fds Sect Mod No'
    	FstBuyerCd          	Format=$1.      Length=$1.      Label='Fst Buyer Cd'
    	FimDirCd            	Format=$1.      Length=$1.      Label='Fim Dir Cd'
		FimDptScaleItemCd		Format=$1.		Length=$1.		Label='Fim Dpt Scale Item Cd'
		FimTrayDesc	        	Length=$10.		Format=$10. 	Label='FimTrayDesc	 '
		FimGlutenFreeCd 		Format=$1.		Length=$1.		Label='Fim Gluten Free Cd'
		FimFairTrdCd			Length=$1.     	Format=$1.		Label='Fim Fair Trd Cd'
		FimOrganicCd			Length=$1.     	Format=$1.		Label='Fim Organic Cd'
		FimHalalCertCd			Length=$1.     	Format=$1.		Label='Fim Halal Cert Cd'
		FimKosherCertCd			Length=$1.     	Format=$1.		Label='Fim Kosher Cert Cd'

    	FimScc14No1         	Format=14.                      Label='Fim Scc14 No1'
    	FimScc14No2         	Format=14.                      Label='Fim Scc14 No2'
    	FimScc14No3         	Format=14.                      Label='Fim Scc14 No3'
    	FimScc14No4         	Format=14.                      Label='Fim Scc14 No4'
    	FimScc14No5         	Format=14.                      Label='Fim Scc14 No5'

    	FimUpcNo1           	Format=18.                      Label='Fim Upc No1'   	
		FimUpcNo2           	Format=18.                      Label='Fim Upc No2'
    	FimUpcNo3           	Format=18.                      Label='Fim Upc No3'
    	FimUpcNo4           	Format=18.                      Label='Fim Upc No4'
    	FimUpcNo5           	Format=18.                      Label='Fim Upc No5'

    	FimCaseUpcNo1       	Format=18.                      Label='Fim Case Upc No1'
    	FimCaseUpcNo2       	Format=18.                      Label='Fim Case Upc No2'
    	FimCaseUpcNo3       	Format=18.                      Label='Fim Case Upc No3'
    	FimCaseUpcNo4       	Format=18.                      Label='Fim Case Upc No4'
    	FimCaseUpcNo5       	Format=18.                      Label='Fim Case Upc No5'

    	FimCompetitorUpcNo1 	Format=18.                      Label='Fim Competitor Upc No1'
    	FimCompetitorUpcNo2 	Format=18.                      Label='Fim Competitor Upc No2'
    	FimCompetitorUpcNo3 	Format=18.                      Label='Fim Competitor Upc No3'
    	FimCompetitorUpcNo4 	Format=18.                      Label='Fim Competitor Upc No4'

    	FimEdiCaseUpcNo1    	Format=18.                      Label='Fim Edi Case Upc No1'
    	FimEdiCaseUpcNo2    	Format=18.                      Label='Fim Edi Case Upc No2'
    	FimEdiCaseUpcNo3    	Format=18.                      Label='Fim Edi Case Upc No3'
    	FimEdiCaseUpcNo4    	Format=18.                      Label='Fim Edi Case Upc No4'
    	FimEdiCaseUpcNo5    	Format=18.                      Label='Fim Edi Case Upc No5'

    	FimItemRstrCd1      	Format=$1.		Length=$1.      Label='Fim Item Rstr Cd1'
    	FimItemRstrCd2      	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd2'
    	FimItemRstrCd3      	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd3'
    	FimItemRstrCd4      	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd4'
    	FimItemRstrCd5      	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd5'
    	FimItemRstrCd6      	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd6'
    	FimItemRstrCd7      	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd7'
    	FimItemRstrCd8      	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd8'
    	FimItemRstrCd9      	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd9'
    	FimItemRstrCd10     	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd10'
    	FimItemRstrCd11     	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd11'
    	FimItemRstrCd12     	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd12'
    	FimItemRstrCd13     	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd13'
    	FimItemRstrCd14     	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd14'
    	FimItemRstrCd15     	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd15'
    	FimItemRstrCd16     	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd16'
    	FimItemRstrCd17     	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd17'
    	FimItemRstrCd18     	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd18'
    	FimItemRstrCd19     	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd19'
    	FimItemRstrCd20     	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd20'
    	FimItemRstrCd21     	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd21'
    	FimItemRstrCd22     	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd22'
    	FimItemRstrCd23     	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd23'
    	FimItemRstrCd24     	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd24'
    	FimItemRstrCd25     	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd25'
    	FimItemRstrCd26     	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd26'
    	FimItemRstrCd27     	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd27'
    	FimItemRstrCd28     	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd28'
    	FimItemRstrCd29     	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd29'
    	FimItemRstrCd30     	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd30'
    	FimItemRstrCd31     	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd31'
    	FimItemRstrCd32     	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd32'
    	FimItemRstrCd33     	Format=$1.      Length=$1.      Label='Fim Item Rstr Cd33'

		FimLblTypCd				Format=$1.		Length=$1.		Label='Fim Lbl Typ Cd'
    	
		FwsRgnId1				Format=$1.		length=$1.		Label='Fws Rgn Id S1'
		FwsRgnId2				Format=$1.		length=$1.		Label='Fws Rgn Id R2'
		FwsRgnId3				Format=$1.		length=$1.		Label='Fws Rgn Id W3'
		FwsRgnId4				Format=$1.		length=$1.		Label='Fws Rgn Id C4'
		FwsRgnId5				Format=$1.		length=$1.		Label='Fws Rgn Id E5'

		FimStkStat1				Format=$1.		length=$1.		Label='Fim Stk Stat S1'
		FimStkStat2				Format=$1.		length=$1.		Label='Fim Stk Stat R2'
		FimStkStat3				Format=$1.		length=$1.		Label='Fim Stk Stat W3'
		FimStkStat4				Format=$1.		length=$1.		Label='Fim Stk Stat C4'
		FimStkStat5				Format=$1.		length=$1.		Label='Fim Stk Stat E5'

    	FimStkStatDate1         Format=Date9.                   Label='Fim Stk Stat Date S1'
    	FimStkStatDate2         Format=Date9.                   Label='Fim Stk Stat Date R2'
    	FimStkStatDate3         Format=Date9.                   Label='Fim Stk Stat Date W3'
    	FimStkStatDate4         Format=Date9.                   Label='Fim Stk Stat Date C4'
    	FimStkStatDate5         Format=Date9.                   Label='Fim Stk Stat Date E5'

		FvmNo1                  Format=5.                       Label='Fvm No S1'
    	FvmNo2                  Format=5.                       Label='Fvm No R2'
    	FvmNo3                  Format=5.                       Label='Fvm No W3'
    	FvmNo4                  Format=5.                       Label='Fvm No C4'
    	FvmNo5                  Format=5.                       Label='Fvm No E5'

		FimBuyerCd1				Format=$1.		Length=$1.		Label='Fim Buyer Cd S1'
		FimBuyerCd2				Format=$1.		Length=$1.		Label='Fim Buyer Cd R2'
		FimBuyerCd3				Format=$1.		Length=$1.		Label='Fim Buyer Cd W3'
		FimBuyerCd4				Format=$1.		Length=$1.		Label='Fim Buyer Cd C4'
		FimBuyerCd5				Format=$1.		Length=$1.		Label='Fim Buyer Cd E5'

		FimMovmntClassCd1		Format=$1.		Length=$1.		Label='Fim Movmnt Class Cd S1'
		FimMovmntClassCd2		Format=$1.		Length=$1.		Label='Fim Movmnt Class Cd R2'
		FimMovmntClassCd3		Format=$1.		Length=$1.		Label='Fim Movmnt Class Cd W3'
		FimMovmntClassCd4		Format=$1.		Length=$1.		Label='Fim Movmnt Class Cd C4'
		FimMovmntClassCd5		Format=$1.		Length=$1.		Label='Fim Movmnt Class Cd E5'

		FimDltDate1             Format=Date9.                   Label='Fim Dlt Date S1'
    	FimDltDate2             Format=Date9.                   Label='Fim Dlt Date R2'
    	FimDltDate3             Format=Date9.                   Label='Fim Dlt Date W3'
    	FimDltDate4             Format=Date9.                   Label='Fim Dlt Date C4'
    	FimDltDate5             Format=Date9.                   Label='Fim Dlt Date E5'

		FimSubNo1				Format=Z7.						Label='Fim Sub No S1'
		FimSubNo2				Format=Z7.						Label='Fim Sub No R2'
		FimSubNo3				Format=Z7.						Label='Fim Sub No W3'
		FimSubNo4				Format=Z7.						Label='Fim Sub No C4'
		FimSubNo5				Format=Z7.						Label='Fim Sub No E5'

		FimMinOrddQty1			Format=7.						Label='Fim Min Ordd Qty S1'
		FimMinOrddQty2			Format=7.						Label='Fim Min Ordd Qty R2'
		FimMinOrddQty3			Format=7.						Label='Fim Min Ordd Qty W3'
		FimMinOrddQty4			Format=7.						Label='Fim Min Ordd Qty C4'
		FimMinOrddQty5			Format=7.						Label='Fim Min Ordd Qty E5'
    	 
	    FimReclmCd             	Format=$1.  	Length=$1.      Label='Fim Reclm Cd'
    	FimCatlgCd            	Format=$1.     	Length=$1.      Label='Fim Catlg Cd'
    	FimHdwePartCd        	Format=$1.    	Length=$1.      Label='Fim Hdwe Part Cd'
    	FimDptStoreStkCd      	Format=$1.   	Length=$1.      Label='Fim Dpt Store Stk Cd'
    	FimNFoodStkCd        	Format=$1.    	Length=$1.      Label='Fim NFood Stk Cd'
    	FimHomeCentreStkCd    	Format=$1.   	Length=$1.      Label='Fim Home Centre Stk Cd'
    	FimFarmSuplStkCd      	Format=$1.   	Length=$1.      Label='Fim Farm Supl Stk Cd'
    	FimServStnStkCd   		Format=$1.	    Length=$1.      Label='Fim Serv Stn Stk Cd'
    	FimRtlSeasnlCd       	Format=$1.    	Length=$1.      Label='Fim Rtl Seasnl Cd'
    	FimPenaltyCd         	Format=$1.    	Length=$1.      Label='Fim Penalty Cd'
    	FimRtlWarrCd       		Format=$1.      Length=$1.      Label='Fim Rtl Warr Cd'
	    FimWarrSuppCd          	Format=$1.  	Length=$1.      Label='Fim Warr Supp Cd'
    	FimContrPrc             Format=7.2                      Label='Fim Contr Prc'

		FimTgpPrcCd1			Format=$1.		Length=$1.		Label='Fim Tgp Prc Cd S1'
		FimTgpPrcCd2			Format=$1.		Length=$1.		Label='Fim Tgp Prc Cd R2'
		FimTgpPrcCd3			Format=$1.		Length=$1.		Label='Fim Tgp Prc Cd W3'
		FimTgpPrcCd4			Format=$1.		Length=$1.		Label='Fim Tgp Prc Cd C4'
		FimTgpPrcCd5			Format=$1.		Length=$1.		Label='Fim Tgp Prc Cd E5'

		FimDltReasCd1			Format=$1.		Length=$1.		Label='Fim Dlt Reas Cd S1'
		FimDltReasCd2			Format=$1.		Length=$1.		Label='Fim Dlt Reas Cd R2'
		FimDltReasCd3			Format=$1.		Length=$1.		Label='Fim Dlt Reas Cd W3'
		FimDltReasCd4			Format=$1.		Length=$1.		Label='Fim Dlt Reas Cd C4'
		FimDltReasCd5			Format=$1.		Length=$1.		Label='Fim Dlt Reas Cd E5'

		FimFutDltReasCd1		Format=$1.		Length=$1.		Label='Fim Fut Dlt Reas Cd S1'
		FimFutDltReasCd2		Format=$1.		Length=$1.		Label='Fim Fut Dlt Reas Cd R2'
		FimFutDltReasCd3		Format=$1.		Length=$1.		Label='Fim Fut Dlt Reas Cd W3'
		FimFutDltReasCd4		Format=$1.		Length=$1.		Label='Fim Fut Dlt Reas Cd C4'
		FimFutDltReasCd5		Format=$1.		Length=$1.		Label='Fim Fut Dlt Reas Cd E5'
    	        		     
    	FimRtlUnitsMeasDim     	Format=$2.   	Length=$2.      Label='Fim Rtl Units Meas Dim'
    	FimRtlLgthDim           Format=10.3                     Label='Fim Rtl Lgth Dim'
    	FimRtlWidthDim          Format=10.3                     Label='Fim Rtl Width Dim'
    	FimRtlDepthDim          Format=10.3                     Label='Fim Rtl Depth Dim'
    	FimRtlPkgDesc           Format=$3. 		Length=$3.      Label='Fim Rtl Pkg Desc'
    	FimRtlShrtDesc          Format=$20. 	Length=$20.     Label='Fim Rtl Shrt Desc'
    	FimRtlFillerPatNo       Format=2.                       Label='Fim Rtl Filler Pat No'
    	FimRtlPegRightDim       Format=Best5.                   Label='Fim Rtl Peg Right Dim'
    	FimRtlPegDownDim        Format=Best5.                   Label='Fim Rtl Peg Down Dim'
    	FimRtlHangCd            Format=$1. 		Length=$1.      Label='Fim Rtl Hang Cd'
    	FricMajorDptNo          Format=Z2.						Label='Fric Major Dpt No'
    	FricMinorDptNo          Format=Z3.                      Label='Fric Minor Dpt No'
    	FricMajorCategNo        Format=Z2.                      Label='Fric Major Categ No'
		FricGrpCategNo			Format=Z2.						Label='Fric Group Categ No'
    	FricMinorCategNo        Format=Z3.                      Label='Fric Minor Categ No'

    	FimRtlMovmntQty1        Format=Best5.                   Label='Fim Rtl Movmnt Qty1'
    	FimRtlMovmntQty2        Format=Best5.                   Label='Fim Rtl Movmnt Qty2'
    	FimRtlMovmntQty3        Format=Best5.                   Label='Fim Rtl Movmnt Qty3'
    	FimRtlMovmntQty4        Format=Best5.                   Label='Fim Rtl Movmnt Qty4'
    	FimRtlMovmntQty5        Format=Best5.                   Label='Fim Rtl Movmnt Qty5'
    	FimRtlMovmntQty6        Format=Best5.                   Label='Fim Rtl Movmnt Qty6'
    	FimRtlMovmntQty7        Format=Best5.                   Label='Fim Rtl Movmnt Qty7'
    	FimRtlMovmntQty8        Format=Best5.                   Label='Fim Rtl Movmnt Qty8'
    	FimRtlMovmntQty9        Format=Best5.                   Label='Fim Rtl Movmnt Qty9'
    	FimRtlMovmntQty10       Format=Best5.                   Label='Fim Rtl Movmnt Qty10'
    	FimRtlMovmntQty11       Format=Best5.                   Label='Fim Rtl Movmnt Qty11'
    	FimRtlMovmntQty12       Format=Best5.                   Label='Fim Rtl Movmnt Qty12'

    	FimRtlPlanogramNo1      Format=4.                       Label='Fim Rtl Planogram No1'
    	FimRtlPlanogramNo2      Format=4.                       Label='Fim Rtl Planogram No2'
    	FimRtlPlanogramNo3      Format=4.                       Label='Fim Rtl Planogram No3'
    	FimRtlPlanogramCd1      Format=$15.  	Length=$15.     Label='Fim Rtl Planogram Cd1'
    	FimRtlPlanogramCd2      Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd2'
    	FimRtlPlanogramCd3      Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd3'
    	FimRtlPlanogramCd4      Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd4'
    	FimRtlPlanogramCd5      Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd5'
    	FimRtlPlanogramCd6      Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd6'
    	FimRtlPlanogramCd7      Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd7'
    	FimRtlPlanogramCd8      Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd8'
    	FimRtlPlanogramCd9      Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd9'
    	FimRtlPlanogramCd10     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd10'
    	FimRtlPlanogramCd11     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd11'
    	FimRtlPlanogramCd12     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd12'
    	FimRtlPlanogramCd13     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd13'
    	FimRtlPlanogramCd14     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd14'
    	FimRtlPlanogramCd15     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd15'
    	FimRtlPlanogramCd16     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd16'
    	FimRtlPlanogramCd17     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd17'
    	FimRtlPlanogramCd18     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd18'
    	FimRtlPlanogramCd19     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd19'
    	FimRtlPlanogramCd20     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd20'
    	FimRtlPlanogramCd21     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd21'
    	FimRtlPlanogramCd22     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd22'
    	FimRtlPlanogramCd23     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd23'
    	FimRtlPlanogramCd24     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd24'
    	FimRtlPlanogramCd25     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd25'
    	FimRtlPlanogramCd26     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd26'
    	FimRtlPlanogramCd27     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd27'
    	FimRtlPlanogramCd28     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd28'
    	FimRtlPlanogramCd29     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd29'
    	FimRtlPlanogramCd30     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd30'
    	FimRtlPlanogramCd31     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd31'
    	FimRtlPlanogramCd32     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd32'
    	FimRtlPlanogramCd33     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd33'
    	FimRtlPlanogramCd34     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd34'
    	FimRtlPlanogramCd35     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd35'
    	FimRtlPlanogramCd36     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd36'
    	FimRtlPlanogramCd37     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd37'
    	FimRtlPlanogramCd38     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd38'
    	FimRtlPlanogramCd39     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd39'
    	FimRtlPlanogramCd40     Format=$15.     Length=$15.     Label='Fim Rtl Planogram Cd40'

    	FdsTgpGrpNo             Format=Z2.                      Label='Fds Tgp Grp No'
    	FdsTgpFamilyNo          Format=Z6.                      Label='Fds Tgp Family No'
    	FimTgpSeqNo             Format=Z4.                      Label='Fim Tgp Seq No'
	    FimBrandCd            	Format=$1.   	Length=$1.      Label='Fim Brand Cd'
    	FimTgpItemNo            Format=Z7.                      Label='Fim Tgp Item No'
    	
		FimBuyStrDate1          Format=Date9.                   Label='Fim Buy Str Date S1'
    	FimBuyStrDate2          Format=Date9.                   Label='Fim Buy Str Date R2'
    	FimBuyStrDate3          Format=Date9.                   Label='Fim Buy Str Date W3'
    	FimBuyStrDate4          Format=Date9.                   Label='Fim Buy Str Date C4'
    	FimBuyStrDate5          Format=Date9.                   Label='Fim Buy Str Date E5'

		FimBuyStpDate1          Format=Date9.                   Label='Fim Buy Stp Date S1'
    	FimBuyStpDate2          Format=Date9.                   Label='Fim Buy Stp Date R2'
    	FimBuyStpDate3          Format=Date9.                   Label='Fim Buy Stp Date W3'
    	FimBuyStpDate4          Format=Date9.                   Label='Fim Buy Stp Date C4'
    	FimBuyStpDate5          Format=Date9.                   Label='Fim Buy Stp Date E5'
    	                   
    	FdsDptSectNo            	Format=ss. 						Label='Fds Dpt Sect No'                             
    	FdsDptSectModGrpSeqId   	Format=$14. 	Length=$14.		Label='Fds Dpt Sect Mod Grp Seq Id'
    	FWFMeas                  	Format=$15.		Length=$15.     Label='FWF Meas'
    	UPC                     	Format=$16. 	Length=$16.     Label='UPC'                            
    	    	
    	FimCoopLabelCd          	Format=$1.      Length=$1.      Label='Fim Coop Label Cd'
    	
    	FimRgnId                	Format=$5.      Length=$5.      Label='Fim Rgn Id'
    	FimRgnStkStat           	Format=$5.      Length=$5.      Label='Fim Rgn Stk Stat'
    	
    	FimRgnAltPrtyPkChgCd    	Format=$5.      Length=$5.      Label='Fim Rgn Alt Prty Pk Chg Cd'
    	FimRgnTgpPrcCd          	Format=$5.      Length=$5.      Label='Fim Rgn Tgp Prc Cd'
		FimRgnMovmntClassCd			Format=$5.		Length=$5.		Label='Fim Rgn Movmnt Class Cd'

		FimSubNo					Format=Z7.						Label='Fim Sub No'
		FvmNo						Format=Z5.						Label='Fvm No'
		FimMinOrddQty				Format=7.						Label='Fim Min Ordd Qty'
		FimStkStatDate				Format=Date9.					Label='Fim Stk Stat Date'
		FimDltDate					Format=Date9.					Label='Fim Dlt Date'
		FimBuyStrDate				Format=Date9.					Label='Fim Buy Str Date'
		FimBuyStpDate				Format=Date9.					Label='Fim Buy Stp Date'
    	 
    	FricMajMinDptNo         	Format=Z5.                      Label='Fric Maj Min Dpt No'
    	FricMajMinDptMajCatNo   	Format=Z7.                      Label='Fric Maj Min Dpt Maj Cat No'
		FricMajMinDptMajCatGrpNo	Format=Z9.						Label='Fric Maj Min Dpt Maj Cat Grp No'
    	FricMajMinDptMajMinCatNo 	Format=Z10.                   	Label='Fric Maj Min Dpt Maj Min Cat No'
    	;

  	Keep
    	FimNo
    	FimRecStat
    	FimStat
    	FimStatDate

    	FdsDptNo
    	FdsSectNo
    	FdsGrpNo
    	FimSeqNo
    	FimDesc
    	FimStkNo
    	FimFclPkQty
    	FimWholeNo
    	FimFractDesc
    	FimTypMeasCd
    	FimMeasCd
    	FimCaseWholeNo
    	FimCaseFractDesc
    	FimCaseTypMeasCd
    	FimUnitWght
    	FimWghtScaleCd
    	FimCubeLgthDim
    	FimCubeWidthDim
    	FimCubeHighDim
    	FimCubeCd
    	FimCubeMeasCd
    	FimCubeVolDim
    	
    	FimMfrLgthDim
    	FimMfrWidthDim
    	FimMfrHighDim
    	FimMfrCd
    	FimMfrQty
    	FimMfrShpgUnitCd

    	FimUnitLgthDim
    	FimUnitWidthDim
    	FimUnitHighDim
    	FimUnitCd

    	FimSpeedToMktCd
    	FimSpeedToMktQty
    	FimSpeedToMktDate

    	FimOrddUnitsCd
    	FimRtlUnitQty
    	FimRtlUnitCd
    	FimCaseRtlUnitsQty

    	FimLowCostCd
    	FimBoCd
    	FimRdrCd
    	FimWarrCd
    	FimWarrAmt

    	FimServFeeNo
    	FimFrtAllowCd
    	FimLksSwlsCd
    	FimPartAdvCd
    	FimTireCd
    	FimRebateCd
    	FimRebatePcnt

    	FimMstItemCd
    	FimZonePrcCd

    	FdsSectModNo
    	FstBuyerCd

    	FimDirCd
		FimTrayDesc	        	

		FimDptScaleItemCd 
		FimGlutenFreeCd
		FimFairTrdCd
		FimOrganicCd
		FimHalalCertCd
		FimKosherCertCd

    	FimScc14No1         - FimScc14No5
    	FimUpcNo1           - FimUpcNo5
    	FimCaseUpcNo1       - FimCaseUpcNo5
    	FimCompetitorUpcNo1 - FimCompetitorUpcNo4
    	FimEdiCaseUpcNo1    - FimEdiCaseUpcNo5
    	FimItemRstrCd1      - FimItemRstrCd33

    	FwsRgnId1			- FwsRgnId5
		FimStkStat1			- FimStkStat5

		FimStkStatDate
    	FimStkStatDate1     - FimStkStatDate5
		
		FvmNo
    	FvmNo1              - FvmNo5
		FimBuyerCd1			- FimBuyerCd5
		FimMovmntClassCd1	- FimMovmntClassCd5

		FimDltDate
    	FimDltDate1         - FimDltDate5

		FimSubNo
    	FimSubNo1			- FimSubNo5 

		FimMinOrddQty
		FimMinOrddQty1		- FimMinOrddQty5
		
    	FimReclmCd
    	FimCatlgCd
    	FimHdwePartCd
    	FimDptStoreStkCd
    	FimNFoodStkCd
    	FimHomeCentreStkCd
    	FimFarmSuplStkCd
    	FimServStnStkCd
    	FimRtlSeasnlCd

    	FimPenaltyCd
    	FimRtlWarrCd
    	FimWarrSuppCd
    	FimContrPrc

		FimTgpPrcCd1		- FimTgpPrcCd5
		FimDltReasCd1		- FimDltReasCd5
		FimFutDltReasCd1	- FimFutDltReasCd5

    	FimRtlUnitsMeasDim
    	FimRtlLgthDim
    	FimRtlWidthDim
    	FimRtlDepthDim
    	FimRtlPkgDesc
    	FimRtlShrtDesc
    	FimRtlFillerPatNo
    	FimRtlPegRightDim
    	FimRtlPegDownDim
    	FimRtlHangCd

    	FricMajorDptNo
    	FricMinorDptNo
    	FricMinorDptNo
    	FricMajorCategNo
		FricGrpCategNo
    	FricMinorCategNo

		FimLblTypCd

    	FimRtlMovmntQty1   	- FimRtlMovmntQty12
   	 	FimRtlPlanogramNo1 	- FimRtlPlanogramNo3
    	FimRtlPlanogramCd1 	- FimRtlPlanogramCd40

    	FdsTgpGrpNo
    	FdsTgpFamilyNo
    	FimTgpSeqNo
    	FimBrandCd
    	FimTgpItemNo

		FimBuyStrDate
    	FimBuyStrDate1 		- FimBuyStrDate5

		FimBuyStpDate
		FimBuyStpDate1 		- FimBuyStpDate5

    	FdsDptSectNo
    	FdsDptSectModGrpSeqId
    	UPC
		CaseUPC
    	FWFMeas

    	FimCoopLabelCd
    	
    	FimRgnId
    	FimRgnStkStat
    	FimRgnMovmntClassCd 
    	FimRgnAltPrtyPkChgCd
    	FimRgnTgpPrcCd

    	FricMajMinDptNo
    	FricMajMinDptMajCatNo
		FricMajMinDptMajCatGrpNo
    	FricMajMinDptMajMinCatNo
    	;

 	%Include RDC(ORS275PC) ;

	%CNVTDATE(FimStatDate		) ;

	%CNVTDATE(FimSpeedToMktDate	) ;

	%CNVTDATE(FimStkStatDate1	) ;
  	%CNVTDATE(FimStkStatDate2	) ;
  	%CNVTDATE(FimStkStatDate3	) ;
  	%CNVTDATE(FimStkStatDate4	) ;
  	%CNVTDATE(FimStkStatDate5	) ;

  	%CNVTDATE(FimDltDate1		) ;
  	%CNVTDATE(FimDltDate2		) ;
  	%CNVTDATE(FimDltDate3		) ;
  	%CNVTDATE(FimDltDate4		) ;
  	%CNVTDATE(FimDltDate5		) ;

  	%CNVTDATE(FimBuyStrDate1	) ;
  	%CNVTDATE(FimBuyStrDate2	) ;
  	%CNVTDATE(FimBuyStrDate3	) ;
  	%CNVTDATE(FimBuyStrDate4	) ;
  	%CNVTDATE(FimBuyStrDate5	) ;

	%CNVTDATE(FimBuyStpDate1	) ;
  	%CNVTDATE(FimBuyStpDate2	) ;
  	%CNVTDATE(FimBuyStpDate3	) ;
  	%CNVTDATE(FimBuyStpDate4	) ;
  	%CNVTDATE(FimBuyStpDate5	) ;

	* Catch any items that went to stat 5,9 today ;
	If FimStat in ('5','9') and FimStatDate = Date() Then FimStatDate = Date() - 1 ;

  	If TB{1} = ' ' and TB{2} = ' ' and TB{3} = ' ' and TB{4} = ' ' and TB{5} = ' ' Then Do ;
		*Put 
			'1.Item Deleted: No Region ' 
			/	
			+	1	FimNo			Z7. 
			+	1	FimDesc 		$35.
			;
		Delete ;
	End ;

	If FimRecStat = 'I' Then Do ;
		Put 
			'2.Item Deleted: Incomplete ' 
			/	
			+	1	FimNo			Z7. 
			+	1	FimDesc 		$35.
			;
		Delete ;
	End ;

/*	If TB{5} = 'E' and TN{5} in (2,6) Then Do ;
		*Put 
			'3.Item Deleted: TGP Kamloops ' 
			/		
			+	1	FimNo			Z7.  
			+	1	FimDesc			$35. 
			+	1	TB{5}			$1. 
			+	1	TN{5}			$1. 	
			;
		Delete ; 
	End ;

	If 		GW{1} in (' ','T') 
		and GW{2} in (' ','T') 
		and GW{3} in (' ','T') 
		and GW{4} in (' ','T') 
		and GW{5} in (' ','T') 
		Then Do ;
		* Put 
			'4.Item Deleted: TGP Only Pricing ' 
			/		
			+	1	FimNo			Z7.  
			+	1	FimDesc			$35. 
			+	1	GW{1}			$1. 
			+	1	GW{2}			$1. 
			+	1	GW{3}			$1. 
			+	1	GW{4}			$1. 
			+	1	GW{5}			$1. 	
			;
		* Delete ; 
	End ;

	If FimStat = '9' And (Date() - FimStatDate > 700) Then Do ;
		Put 
			'5.Item Delete: Stat 9 over 700 days old' 
			/
			+	1	FimNo			z7. 
			+	1	FimDesc			$35. 
			+	1	FimStat			$1.
			+	1	FimStatDate		Date9.
			;
		Delete ;
	End ;

	If FimStat = '9' And (Date() - FimStatDate > 60) And FimRtlPlanogramCd1 = ' ' Then Do ;
		Put 
			'6.Item Delete: Stat 9 over 60 days old and not on any Plan' 
			/
			+	1	FimNo			z7. 
			+	1	FimDesc			$35. 
			+	1	FimStat			$1.
			+	1	FimStatDate		Date9.
			;
		Delete ;
	End ;
*/
  	FdsDptSectNo = FdsDptNo * 100 + FdsSectNo ;

  	Substr(FdsDptSectModGrpSeqId,01,3) = Put (FdsDptNo     ,z3.) ;
  	Substr(FdsDptSectModGrpSeqId,04,2) = Put (FdsSectNo    ,z2.) ;
  	Substr(FdsDptSectModGrpSeqId,06,2) = Put (FdsSectModNo ,z2.) ;
  	Substr(FdsDptSectModGrpSeqId,08,3) = Put (FdsGrpNo     ,z3.) ;
  	Substr(FdsDptSectModGrpSeqId,11,4) = Put (FimSeqNo     ,z4.) ;

* screen names refer to Minor Dpt - variables are planogram no ;
  	*FricMinorDptNo 				= FricPlanogramNo ; *Var name changed Mar 1 2012 ;
  	FricMajMinDptNo           	= FricMajorDptNo         * 1000 + FricMinorDptNo ;
  	FricMajMinDptMajCatNo     	= FricMajMinDptNo        *  100 + FricMajorCategNo ;
	FricMajMinDptMajCatGrpNo	= FricMajMinDptMajCatNo  *  100 + FricGrpCategNo ;
  	FricMajMinDptMajMinCatNo  	= FricMajMinDptMajCatNo  * 1000 + FricMinorCategNo ;

* UPC Logic ;
  	UPC = Left (Put (FimUpcNo1 , 16.)) ;

  	Do i = 2 To 5 ;
    	If KD{i} > 0 Then UPC = Left (Put (KD{i} , 16.)) ;
    	If KD{i} = 0 Then Leave ; 
  	End ;
	If UPC = ' ' Then UPC = '0000000000' ;
	*NEW BZ SEP 2008 - add a leading zero to match the hand held scanners ;
	If Length(UPC) = 10 then UPC = '0' || UPC ;
* End UPC logic ;

* Case UPC Logic 		*Added for Courtney 15-Jun-2018 LL ;
  	CaseUPC = Left (Put (FimCaseUpcNo1 , 16.)) ;

  	Do i = 2 To 5 ;
    	If KD{i} > 0 Then CaseUPC = Left (Put (KD{i} , 16.)) ;
    	If KD{i} = 0 Then Leave ; 
  	End ;
	If CaseUPC = ' ' Then CaseUPC = '0000000000' ;
	*NEW BZ SEP 2008 - add a leading zero to match the hand held scanners ;
	If Length(CaseUPC) = 10 then CaseUPC = '0' || UPC ;
* End UPC logic ;

  	FWFMeas = Compress (Put(FimFclPkQty,5.)) || '/' || Compress (Put(FimWholeNo,5.) || FimFractDesc || FimTypMeasCd ) ;

  	FimCoopLabelCd = 'N' ;
  	Select (FdsDptNo) ;
    	When (030) Do ;
      		If BF{08} = 'Y' or BF{16} = 'Y' Then FimCoopLabelCd = 'Y' ;
    	End ;

    	Otherwise Do ;
      		If BF{04} = 'Y' or BF{05} = 'Y' Then FimCoopLabelCd = 'Y' ;
    	End ;
  	End ;

	If FimDesc = ' ' Then FimDesc = Put(FimNo , z7.) || '*** Blank Desc on IMF but added by Ord040 *** ' ;

  	FimRgnId             	= '-----' ;
  	FimRgnStkStat        	= '-----' ;
	FimRgnMovmntClassCd	 	= '-----' ;
  	FimRgnAltPrtyPkChgCd 	= '-----' ;
  	FimRgnTgpPrcCd       	= '-----' ;

	FimSubNo 		= 0 ;
	FvmNo			= 0 ;
	FimMinOrddQty	= 0 ;

	FimDltDate		= '31Dec2099'd ;
	FimStkStatDate	= '31Dec1900'd ;
	FimBuyStrDate	= . 		   ;
	FimBuyStpDate	= '31Dec2099'd ;

  	Do i = 1 to Dim(TB) ;
    	If Verify(TB{i},'CERSW')> 0 or Verify(TD{i},'AEGJMPRS') > 0 Then Do ;
        	TB{i} = ' ' ;	* Fws Rgn Id 				;
        	TD{i} = ' ' ;	* Fim Stk Stat 				;
        	ZJ{i} = ' ' ;	* Fim Alt Prty Pk Chg Cd	; 
        	GW{i} = ' ' ;	* Fim Tgp Prc Cd			;
			TZ{i} = ' ' ; 	* Fim Movmnt Class Cd		;
        	A7{i} = ' ' ;	* Fim Buyer Cd				;
    	End ;

    	If Verify(TB{i},'CERSW') = 0 Then Do ;
      		If Verify(TD{i},'AEGJMPRS') = 0 Then Do ;
        		Substr (FimRgnId           	,i,1) = TB{i} ;
        		Substr (FimRgnStkStat      	,i,1) = TD{i} ;
				Substr (FimRgnTgpPrcCd 		,i,1) = GW{i} ;
				Substr (FimRgnMovmntClassCd	,i,1) = TZ{i} ;

        		If ZJ{i} = 'Y' Then	Substr (FimRgnAltPrtyPkChgCd ,i,1) = ZJ{i} ;

				If UN{i} > 0 Then FimSubNo 			= UN{i} ;
				If TI{i} > 0 Then FvmNo				= TI{i} ;
				If HX{i} > 0 Then FimMinOrddQty 	= HX{i} ;

				If TE{i} > 0 Then FimStkStatDate	= TE{i} ;
				If WO{i} > 0 Then FimDltDate		= WO{i} ;
				If DQ{i} > 0 Then FimBuyStrDate 	= DQ{i} ;
				If DR{i} > 0 Then FimBuyStpDate		= DR{i} ;
      		End ;
    	End ;
  	End ;  	
Run ;

%PDATASET(Save,Ors275a) ;

Proc Sort Data=Ors275a Out=Save.Ors275a NoDupKey ;
  	By FimNo ;
Run ;

Data Ors275PCSub (Keep=FimNo FimSubNo) ;	
	Set Save.Ors275a (Keep=FimNo FimSubNo FimRgnAltPrtyPkChgCd ) ;
	Where Index(FimRgnAltPrtyPkChgCd,'Y') > 0 and FimSubNo > 0 ; 
Run ;

Proc Sort Data=Ors275PCSub Noduplicates ;
  	By FimSubNo ;
Run ;

Data Ors275PCSub2 ;
	Set Ors275PCSub ;
	FimNoPCX = FimNo ;
	FimNo = FimSubNo ;
	Keep
		FimNo
		FimNoPCX
		;
Run ;

Proc Sort Data=Ors275PCSub2 ;
	By FimNo ;
Run ;

Data _null_ ;
	Set Ors275PCSub2 ;
	By FimNo ;
	File Print ;
	If First.FimNo and Last.FimNo Then Delete ;
	Put _all_ ;
Run ;

Proc Sort Data=Ors275PCSub2 NoDupKey ;
	By FimNo ;
Run ;

Proc Contents Data=Save.Ors275a Position ;
Run ;
		
Proc Format Library=SAVE ;
  	Value $OR_pkg
   		'BOX' = 'Box'
   		'CAN' = 'Can'
   		'JAR' = 'Jar'
   		'ROL' = 'Roll'
   		'BOT' = 'Bottle'
   		'HAI' = 'Hairpin'
   		'CLO' = 'Clothing'
   		'LOO' = 'Loose'
   		'HOL' = 'Holed'
    	;

  	Picture Category Low-High = '99-999-99-999';
Run ;

Data Ori274 ;
  	Set ORI274.Ors274 ;
  	Where FvmNo > 0 ;
  	By FvmNo FwsRgnId ;
  	Length FmtName $ 8 Label $ 40 ;
  	Keep FmtName Start Label ;

  	If First.FvmNo Then Do ;
    	FmtName = 'FvmName' ;
    	Start 	= FvmNo ;
    	Label 	= FvmName ;
    	Output ;
  	End ;
Run ;

Proc Sort Data=Ori274 ;
  	By FmtName Start ;
Run ;

Proc Format Library=SAVE Cntlin=Ori274 ;
Run ;

%PDATASET(WORK,Ori274) ;

*Data Ors101 (Keep=FimNo FimRegSrpPrcX FimRegRccPrcX FimTgpRegRccPrcX) ;
Data Ors276 (Keep=FimNo FimSrpPrcX FimRccPrcX FimTgpRccPrcX) ;
  	Merge
  		Save.Ors275a 	(in=i1 Keep=FimNo FdsDptNo FimFclPkQty FimRtlUnitQty)
		ORI276.Ors276 	(in=i2 Keep=FimNo FimTgpRccPrc FimSrpPrc FimRccPrc FimPkRtlPrc)
    	;
  	By FimNo ;
  	If i1 and i2 ;  
  	Retain 
    	FimSrpPrcX  
    	FimRccPrcX  
    	FimTgpRccPrcX
    	0 ;

  	If First.FimNo Then Do ;
    	FimSrpPrcX 		= 0.00 ;
    	FimRccPrcX 		= 0.00 ;
    	FimTgpRccPrcX 	= 0.00 ;
  	End ;

  	If FimSrpPrcX 	= 0 And FimPkRtlPrc 	> 0 and FimRtlUnitQty > 0 	Then FimSrpPrcX 		= FimPkRtlPrc / FimRtlUnitQty ;
  	If FimRccPrcX 	= 0 And FimRccPrc 		> 0 						Then FimRccPrcX 		= FimRccPrc ;
  	If FimTgpRccPrcX = 0 And FimTgpRccPrc 	> 0 						Then FimTgpRccPrcX 		= FimTgpRccPrc ;

  	If Last.FimNo Then Output ;
Run ;

* Get DHF Demand History ;
Proc Means Data=ORI802.Ors107
    	(Keep=FimNo FdsDptNo 
          	RegShpdTyQty    PromShpdTyQty    SopShpdTyQty
    		) NoPrint;
  	By FimNo ;
  	Var		RegShpdTyQty    PromShpdTyQty    SopShpdTyQty
			;
  	Output Out=Ors107(Drop=_type_ _freq_) Sum= ;
Run ;

*  READ IN Imf Constant ;
Data ORS275b ;
  	Merge
		Save.Ors275a 	(in=i1)
		Ors275PCSub2	(in=i2)
		;

	By FimNo ;
	If i1 ;

  	File PRTERR Mod ;

  	Attrib
    	FimNo                       		Format=Z7.    		Label='Item #'
    	FimTgpItemNo                		Format=Z7.    		Label='TGP Item #'
    	FdsDptNo                    		Format=Z3.    		Label='Fds Dpt #'

    	FimStkStatDate1 - FimStkStatDate5  	Format=Date9.
    	FimDltDate1     - FimDltDate5      	Format=Date9.
    	FimBuyStrDate1  - FimBuyStrDate5   	Format=Date9.
		FimStatDate						   	Format=Date9.

    	UPC               	Length=$16
    	ID                	Length=$7
    	Name              	Length=$35
    	Key               	Length=$20
    	AbbrevName        	Length=$20
    	Size                               	Format=Best10.
    	UOM               	Length=$5
    	Manufacturer      	Length=$30
    	Category          	Length=$30
    	Supplier          	Length=$30
    	PackageStyle      	Length=$20
    	Price                              	Format=Best10.
    	CaseCost                           	Format=Best10.
    	Brand             	Length=$30
    	SubCategory       	Length=$30

    	Desc1             	Length=$20
    	Desc2             	Length=$12
    	Desc3             	Length=$10
		Desc4 		    	Length=$5
		Desc5				Length=$20
    	Desc9			 	Length=$1

    	Desc11-Desc14     	Length=$1
    	Desc15-Desc18     	Length=$12
    	Desc19-Desc22     	Length=$12

    	Desc23-Desc26     	Length=$18
		Desc27				Length=$7
		Desc28				Length=$7
		Desc29				Length=$1
		Desc30				Length=$10
    	Desc31			  	Length=$15
		Desc32				Length=$1
		Desc33				Length=$18
		Desc34     			Length=$13
    	Desc35            	Length=$30
    	Desc36-Desc40     	Length=$1

    	Desc41				Length=$100
		Desc42-Desc49     	Length=$25
    	Desc50     			Length=$1

    	SRWCE             	Length=$5.

    	Value1-Value50                     	Format=Best10.
    	Flag1 -Flag10                      	Format=Z1.

		Date1-Date3							Format=WordDate12.

		DateCreated							Format=WordDate12.
		DateModified						Format=WordDate12.
		DatePending							Format=WordDate12.
		DateEffective						Format=WordDate12.
		DateFinished						Format=WordDate12.

		DBDateEffectiveFrom					Format=WordDate12.
		DBDateEffectiveTo					Format=WordDate12.
  		;

  	Array Desc       	{*}       	Desc1  				- Desc50 				;
  	Array Value      	{*}       	Value1 				- Value50 				;
  	Array Flag       	{*}       	Flag1  				- Flag10 				;

  	Array MQ       		{*} $ 15 	FimRtlPlanogramCd1 	- FimRtlPlanogramCd40 	;
  	Array KD       		{*} 		FimUPCNo1       	- FimUPCNo5 			; 

	Array TI			{*}			FvmNo1				- FvmNo5				;
  	Array DQ       		{*} 		FimBuyStrDate1  	- FimBuyStrDate5 		;
  	Array TE       		{*} 		FimStkStatDate1 	- FimStkStatDate5 		;
  	Array WO       		{*} 		FimDltDate1     	- FimDltDate5 			;
  	Array BF 			{*} $1    	FimItemRstrCd1		- FimItemRstrCd33  		;   

  	If FimRdrCd = ' ' Then FimRdrCd = '-' ;

  	SRWCE  = FimRgnStkStat ;

  	Do i = 1 to 5 ;
    	If Substr(FimRgnId,i,1) in (' ') Then Do ;
      		Substr (SRWCE  				,i,1) = '-' ; * stk status ;
      		Substr (FimRgnMovmntClassCd ,i,1) = '-' ; * movement class ;
      		Substr (FimRgnAltPrtyPkChgCd,i,1) = '-' ; * pack change ;
      		Substr (FimRgnTgpPrcCd		,i,1) = '-' ; * TGP price ;
    	End ;

    	Select (Substr(SRWCE,i,1)) ;
      		When ('A') ;
			When ('E') Substr (SRWCE,i,1) = 'A' ;
			When ('G') ; 
      		When ('J') ;         
      		When ('M') ;        
      		When ('P') ;
			When ('R') ;
      		When ('S') Substr (SRWCE,i,1) = 'P' ;
      		Otherwise  Substr (SRWCE,i,1) = '-' ;
    	End ;

* Movement Class based on Stk Status ;
    	If Substr(FimRgnMovmntClassCd	,i,1) 	= ' ' Then Substr(FimRgnMovmntClassCd		,i,1) = '-' ;
    	If Substr(SRWCE,i,1) 					= '-' Then Substr(FimRgnMovmntClassCd		,i,1) = '-' ;

* Pack Change ;
    	If Substr(FimRgnAltPrtyPkChgCd	,i,1) 	= ' ' Then Substr(FimRgnAltPrtyPkChgCd	,i,1) = '-' ;

  	End ;

  * ONLY TAKE ITEM FOR STN & WPG PLAN If IT IS IN BOTH REGIONS ;
  * EXCEPT If IT IS A REDIRECT ;

  	If 		Substr (SRWCE , 1 , 1) =  'A'  
     	And Substr (SRWCE , 3 , 1) In ('-','M','P','R') 
     	And Substr (SRWCE , 4 , 1) In ('-','M','P','R')  
     	And Substr (SRWCE , 5 , 1) In ('-','M','P','R')
    	Then Sonly = 'Y' ;

  	If 		Substr (SRWCE , 1 , 1) =  'A'  
     	And Substr (SRWCE , 3 , 1) =  'A'  
     	And Substr (SRWCE , 4 , 1) In ('-','M','P','R')  
     	And Substr (SRWCE , 5 , 1) In ('-','M','P','R')
    	Then SWonly = 'Y' ;

  	If 		Substr (SRWCE , 4 , 1) =  'A'  
     	And Substr (SRWCE , 5 , 1) In ('-','M','P','R')  
     	And Substr (SRWCE , 1 , 1) In ('-','M','P','R')  
     	And Substr (SRWCE , 3 , 1) In ('-','M','P','R')
    	Then Conly = 'Y' ;

  	If 		Substr (SRWCE , 5 , 1) =  'A'  
     	And Substr (SRWCE , 1 , 1) In ('-','M','P','R')  
     	And Substr (SRWCE , 3 , 1) In ('-','M','P','R')  
     	And Substr (SRWCE , 4 , 1) In ('-','M','P','R')
    	Then Eonly = 'Y' ;

  	If 		Substr (SRWCE , 4 , 1) =  'A'  
     	And Substr (SRWCE , 5 , 1) =  'A'  
     	And Substr (SRWCE , 1 , 1) In ('-','M','P','R')  
     	And Substr (SRWCE , 3 , 1) In ('-','M','P','R')
    	Then CEonly = 'Y' ;

  	If 		Substr (SRWCE , 4 , 1) =  'A'  
     	And Substr (SRWCE , 3 , 1) =  'A'  
     	And Substr (SRWCE , 1 , 1) In ('-','M','P','R')  
     	And Substr (SRWCE , 5 , 1) In ('-','M','P','R')
    	Then CWonly = 'Y' ;

  	If 		Substr (SRWCE , 4 , 1) =  'A'  
     	And Substr (SRWCE , 1 , 1) =  'A'  
     	And Substr (SRWCE , 3 , 1) In ('-','M','P','R')  
     	And Substr (SRWCE , 5 , 1) In ('-','M','P','R')
    	Then CSonly = 'Y' ;

* 0 ----------- ;

* 1 ----------- ;
  	ID  = Left (Put (FimNo , 7.) ) ;

* 2 ----------- ;
  	Name = FimDesc ;
  
* 3 ----------- ;
  	Key = FimStkNo ;
 
* 4 5 6 ------- ;
  	Width  = FimRtlWidthDim ;
  	Height = FimRtlLgthDim ;
  	Depth  = FimRtlDepthDim ;

  	If FimRtlUnitsMeasDim = 'CM' Then Do ;
    	Width  = Width  / 2.54 ;
    	Height = Height / 2.54 ;
    	Depth  = Depth  / 2.54 ;
  	End ;

  	If Width  = 0 Then Width  = 1 ;
  	If Height = 0 Then Height = 1 ;
  	If Depth  = 0 Then Depth  = 1 ;

* 9 ----------- ;
  	AbbrevName = FimRtlShrtDesc ;
  
* 10 ---------- ;
	FimFractNo = 0 ;
	If Verify(Trim(FimFractDesc),'0123456789.') = 0 Then FimFractNo = Input ( FimFractDesc , 5.) ;
  	If FimFractNo = . Then FimFractNo = 0 ;

  	Size = FimWholeNo + FimFractNo ;
  	If Size = 0 Then Size = 1 ;

* 11 ---------- ;
  	UOM = FimTypMeasCd ;
  	If UOM = ' ' Then UOM = 'EA' ;

* 12 ---------- ;
  	Manufacturer = Put ( FvmNo , FvmName.) ;
  	If FimCoopLabelCd = 'Y' Then Manufacturer = 'Coop Label' ;

* 13 ---------- ;
	Category  = Put (FricMajMinDptMajMinCatNo , Z10.) || ' ' || Put (FricMajMinDptMajMinCatNo , FricMinC.) ;

* 14 ---------- ;
  	Supplier = Put (FvmNo,z5.) ;
  	If FimCoopLabelCd = 'Y' Then Supplier = '00001' ;

* 20 21 ------- Peghole X / Y ;
  	PegholeX = FimRtlPegRightDim ;
  	PegholeY = FimRtlPegDownDim ;

	If FimRtlUnitsMeasDim = 'CM' Then Do ;
    	PegholeX  = PegholeX  / 2.54 ;
    	PegholeY  = PegholeY  / 2.54 ;
  	End ;

* 29 ---------- ;
  	PackageStyle = Put ( FimRtlPkgDesc , $or_pkg.) ;

  	If FimRtlHangCd in ('Y') and FimRtlPkgDesc in ('Jar','Can','Rol','Loo','Bot')
      	Then PackageStyle = 'Holed' ;

  	If FimRtlHangCd in ('O') and ~(FimRtlPkgDesc in ('Box','Clo'))
     	Then PackageStyle = 'Box' ;

* 33 ---------- ;
  	Price = 1.0 ;

* 34 ---------- ;
  	CaseCost = 1.0 ;

* 54 55 56 ---- ;
  	CaseWidth  = FimCubeWidthDim ;
  	CaseHeight = FimCubeHighDim ;
  	CaseDepth  = FimCubeLgthDim ;

	If FimCubeCd = 'M' Then Do ;
    	CaseWidth  = CaseWidth  / 2.54 ;
    	CaseHeight = CaseHeight / 2.54 ;
    	CaseDepth  = CaseDepth  / 2.54 ;
  	End ;

* 60 ---------- ;
  	CaseTotalNumber = FimFclPkQty ;

* ------------- DESCs ;
* 158 -> 207 -- Desc 1 to 50 ;
  	Do i = 1 To 50 ;
    	Desc{i} = '-' ;
  	End ;

* D19 -> D22 -- Future Delete Date - MOVED BEFORE DESC1 ;
  	If WO{1} > 0 Then Desc19 = Put (FimDltDate1 , Date9. ) ;
  	If WO{3} > 0 Then Desc20 = Put (FimDltDate3 , Date9. ) ;
  	If WO{4} > 0 Then Desc21 = Put (FimDltDate4 , Date9. ) ;
  	If WO{5} > 0 Then Desc22 = Put (FimDltDate5 , Date9. ) ;

  	If Desc19 ~= '-' Then Substr(SRWCE,1,1) = 'F' ;
  	If Desc20 ~= '-' Then Substr(SRWCE,3,1) = 'F' ;
  	If Desc21 ~= '-' Then Substr(SRWCE,4,1) = 'F' ;
  	If Desc22 ~= '-' Then Substr(SRWCE,5,1) = 'F' ;

* D1 ---------- Item Update Date ;
	Desc1 = '-----' ;

	Select ;
		When (FimStat 	In ('9')	 				) Desc1 = 'XXXXX' 	;
		When (FimStat 	In ('5')	 				) Desc1 = 'M' 	;
		When (SRWCE 	= 'A-AAA'					) Desc1 = 'A' 		;
  		When (SWonly  	= 'Y'						) Desc1 = 'SW' 		;
  		When (CEonly  	= 'Y'						) Desc1 = 'CE' 		;
  		When (CWonly  	= 'Y'						) Desc1 = 'CW' 		;
  		When (CSonly  	= 'Y'						) Desc1 = 'CS' 		;
  		When (Conly   	= 'Y' And FimRdrCd = '-'	) Desc1 = 'C' 		;
  		When (Conly   	= 'Y' And FimRdrCd = 'Y'	) Desc1 = 'CR' 		;
  		When (Eonly  	= 'Y' 						) Desc1 = 'E' 		;
		When (Sonly   	= 'Y' 						) Desc1 = 'S' 		;
  		Otherwise					 				  Desc1 = 'SRWCE=' || SRWCE ;
	End ;

* D2 ---------- Item Date Available  ;
	Desc2 = 'NOW' ;

	If FimBuyStrDate ~= . then do ;
		If (FimBuyStrDate + 14) > Today() then Desc2 = Put (FimBuyStrDate + 14 , Date9.) ;
	End ;

	If FimStat In ('9') then do ;
		Desc2 = 'DELETE' ;
	End ; 
  	
* D3 ---------- Section Module Group ;
  	Desc3  = Put (FdsSectNo , z2.) || '-' || Put (FdsSectModNo , z2.) || '-' || Put (FdsGrpNo , z3.) ;
  	
* D4 ---------- Movement class ;
  	Desc4  = FimRgnMovmntClassCd ;
  	
* D5 ---------- Case UPC ;
  	Desc5  = CaseUPC ; 
  	
* D6 ---------- ;
  	Desc6  = '-' ;
  	
* D7 ---------- ;
  	Desc7  = '-' ;
  
* D8 ---------- TGP Price (not used LL 18-Apr-2018 ;
  	Desc8  = '-' ;
  	
* D9 ---------- SpdMktHist (ie. Is it / was it a SpdToMkt item) ;
	Desc9  = 'N' ;
	If (FimSpeedToMktCd in ('Y', 'R') ) or (FimSpeedToMktDate > 0) then Desc9 = 'Y' ;

* D10 --------- ;
	Desc10 = '-' ;

* D11 -> D14 -- Stk Status ;
  	Desc11 = Substr(SRWCE,1,1) ;
  	Desc12 = Substr(SRWCE,3,1) ;
  	Desc13 = Substr(SRWCE,4,1) ;
  	Desc14 = Substr(SRWCE,5,1) ;

* D15 -> D18 -- Stock Status Date ;
  	If TE{1} > 0 Then Desc15 = Put (FimStkStatDate1 , Date9.) ;
  	If TE{3} > 0 Then Desc16 = Put (FimStkStatDate3 , Date9.) ;
  	If TE{4} > 0 Then Desc17 = Put (FimStkStatDate4 , Date9.) ;
  	If TE{5} > 0 Then Desc18 = Put (FimStkStatDate5 , Date9.) ;

* D23 -> D26 -- New Items and Buy Start Date ;
	* DQ BuyStrDate		;
	* TE FimStkStatDate ;

	If DQ{1} > 0 Then Desc23 = Put (FimBuyStrDate1 , Date9.) ;
  	If DQ{3} > 0 Then Desc24 = Put (FimBuyStrDate3 , Date9.) ;
  	If DQ{4} > 0 Then Desc25 = Put (FimBuyStrDate4 , Date9.) ;
  	If DQ{5} > 0 Then Desc26 = Put (FimBuyStrDate5 , Date9.) ;
  	
* D27 -> D28 -- Pack Change and Date - filled in later ;
	Desc27 = '-' ;
	Desc28 = '-' ;

	If Index(FimRgnAltPrtyPkChgCd,'Y') > 0 and FimSubNo > 0 Then Do ;
		Desc27 = 'PC-TO  ' ;
		Desc28 = Put(FimSubNo , Z7.) ;
	End ; 

	Else If i2 Then Do ;
		Desc27 = 'PC-FROM' ;
		Desc28 = Put(FimNoPCX , Z7.) ;
	End ;  	

* D29 --  ;
  	Desc29 = '-' ;

* D30 --  ;
  	Desc30 = FimTrayDesc ;

* D31 --  ;
  	Desc31 = FWFMeas ;
  	
* D32 -- FimLblTypCd ; 
	Desc32 = FimLblTypCd ;
	If Desc32 = ' ' Then Desc32 = '-' ;
	
* D33 -- DptSectModGrpSeq DSMGS ; 
	Desc33 = '000-00-00-000-0000' ;
	Substr (Desc33,01,3) = Put(FdsDptNo		,Z3.) ;
	Substr (Desc33,05,2) = Put(FdsSectNo	,Z2.) ;
	Substr (Desc33,08,2) = Put(FdsSectModNo	,Z2.) ;
	Substr (Desc33,11,3) = Put(FdsGrpNo		,Z3.) ;
	Substr (Desc33,15,4) = Put(FimSeqNo		,Z4.) ;
	
	
* D34 -- MajMinDptMajMinCat ; 
	Desc34 = '00-000-00-000' ;
	Substr (Desc34,01,2) = Put(FricMajorDptNo	,Z2.) ;
	Substr (Desc34,04,3) = Put(FricMinorDptNo	,Z3.) ;
	Substr (Desc34,08,2) = Put(FricMajorCategNo	,Z2.) ;
	Substr (Desc34,11,3) = Put(FricMinorCategNo	,Z3.) ;

	
* D35 --------- ;
  *******************************************************************
  * NOTE - this variable is the original manufacturer info as seen  *
  * on the IMF - Manufacturer variable is changed for Coop Label    *
  * so this variable shows the actual vendor for the PL products    *
  *   ie. Normal products - manufacturer = vendor                   *
  *       PL products -     manufacturer = 'Coop Label'             *
  *                         vendor = actual manufacturer            *
  ******************************************************************* ;

  	Vendor = Put ( FvmNo , FvmName.) ;
  	
  	Desc35 = Vendor ;

* D38 -- FimDirCd ; 

	Desc38 = FimDirCd ;

	
* D41 -- IMFPlans - pack as many possible into 100 char ; 
	Desc41 = '-' ;
	iPlanX = 1 ;
	Do iPlan = 1 to Dim(MQ) ;
		If MQ{iPlan} = ' ' Then Leave ;

		iPlanL = Length(Trim(MQ{iPlan})) ;

		If iPlanX + iPlanL > 100 Then Leave ;

		Substr (Desc41 , iPlanX , iPlanL ) = MQ{iPlan} ;
		iPlanX = iPlanX + iPlanL + 1 ;	
	End ;	

* D42 -- OTC Restriction flags by Province ; 

	Desc42 = '--------' ;

	Select ;
		When (BF{17} = 'Y' 				) OTC1 = 'MB' 	;
			Otherwise					  OTC1 = '-'	;
		End ; 
	Select ; 
		When (BF{03} = 'Y'				) OTC2 = 'SK' 	;
			Otherwise					  OTC2 = '-'	;
		End ; 
	Select ; 
  		When (BF{18} = 'Y'				) OTC3 = 'AB' 	;
			Otherwise					  OTC3 = '-'	;
		End ; 
	Select ; 
  		When (BF{07} = 'Y'				) OTC4 = 'BC' 	;
			Otherwise					  OTC4 = '-'	;
		End ;

	Desc42 = catx('-', of OTC1-OTC4) ;

* ------------------------- ;
* Hierarchy ;

	Desc49	= 'ALL' ;

	Desc48	= Put (FdsDptNo , z3.) || ' ' || Put(FdsDptNo,DD.) ;
	FdsDptSectModNo = (FdsDptNo * 100 + FdsSectNo) * 1000 + FdsSectModNo 	;
	FdsDptSectGrpNo	= (FdsDptNo * 100 + FdsSectNo) * 1000 + FdsGrpNo		;

	Select (FdsDptNo) ;
		When (030) Do ;
			Desc47	= Put (FricMajorDptNo 	, z2.) || ' ' || Put (FricMajorDptNo 			, FricMajD.) ;
			Desc46	= Put (FricMinorDptNo 	, z3.) || ' ' || Put (FricMajMinDptNo 			, FricMinD.) ;
			Desc45  = Put (FricMajorCategNo , z2.) || ' ' || Put (FricMajMinDptMajCatNo 	, FricMajC.) ; 
			Desc44  = Put (FricGrpCategNo 	, z2.) || ' ' || Put (FricMajMinDptMajCatGrpNo 	, FricGrp.) ; 
			Desc43 	= Put (FricMinorCategNo , z3.) || ' ' || Put (FricMajMinDptMajMinCatNo 	, FricMinC.) ;  
		End ;

		Otherwise Do ;
			Desc47	= Put (FdsDptSectNo 	, XSS.	) ;
			Desc46 	= Put (FdsDptSectModNo 	, XMM.	) ;
			Desc45  = Put (FdsDptSectGrpNo 	, XGG.	) ;
			Desc44 	= Put (FimSeqNo 		, z4.	) ;
			Desc43 	= ' ' ; 
		End ;
	End ;
	
* D50 -> Used for manual keying of brand groups in Food ;
*     -> It is populated with a dash above but it is not written
	     to the ProductUpdate SQL table in Ord043 ;

* ------------- VALUEs ;
* 208 -> 257 -- Value 1 to 50 ;
  	Do i = 1 To 50 ;
    	Value{i} 	= 0 ;
   	End ;

* V1 ---------- ;
  	Value1       = FdsDptNo ;  

* V2 ---------- ;
  	Value2       = FdsSectNo ;  

* V3 ---------- ;
  	Value3       = FdsSectModNo ;  	

* V4 ---------- ;
  	Value4       = FdsGrpNo ;
  
* V5 ---------- ;
  	Value5       = FimSeqNo ; 

/*VALUES 6-25 now being used for cluster POS rollups ; 

* V6 ---------- ;
  	Value6       = 0 ;  

* V7 ---------- Previously FimRtlPlanogramNo1;
  	Value7       =  0 ;  

* V8 ---------- Previously FimTgpItemNo ;
  	Value8       = 0 ;  

* V9 ---------- ;
  	Value9       = 0 ;  

* V10 --------- ;
  	Value10      = 0 ;  

* V11 --------- ;
  	Value11      = 0 ;  

* V12 --------- ;
  	Value12      = 0 ;  

* V13 --------- ;
  	Value13      = 0 ;  

* V14 --------- ;
  	Value14      = 0 ;  

* V15 --------- ;
  	Value15      = 0 ;  

* V16 --------- ;
  	Value16      = 0 ; 

* V17 --------- ;

* V22 --------- ;
  	Value22      = 0 ;
  
* V23 --------- ;
  	Value23      = 0 ; 

* V24 --------- ;
  	Value24      = 0 ;  

* V25---------- Previously FimRtlPlanogramNo2 ;
  	Value25      = 0 ;  


* V26---------- Previously FimRtlPlanogramNo3 ;
  	Value26      = 0 ;
*/

* V27 ---------- Previously FimRtlPlanogramNo1;
  	Value27       =  FimRtlPlanogramNo1 ;  

* V28---------- TgpRCC - filled in below ;
  	Value28      = 	1.0 ;

* V29---------- ;
  	Value29      = 	FimMinOrddQty ;
 
* V30---------- ;
	Select (FimStat) ;
  		When ('0') 	Value30 = 0 ;
		When ('5') 	Value30 = 5 ;
		When ('9')	Value30 = 9 ;
		Otherwise 	Value30 = 0 ;
	End ;   

* V31 --------- DCRegShp - filled in below ;
  	Value31      = 0 ;  

* V32 --------- DCPromShp - filled in below ;
  	Value32      = 0 ;  

* V33 --------- DCSopShp - filled in below ;
  	Value33      = 0 ;  

* V34 --------- DCTotShp - filled in below ;
  	Value34      = 0 ;  

* V35 --------- DCPromPct - filled in below ;
  	Value35      = 0 ;

* ------------- FLAGs ;
* 258 -> 267 -- Flag 1 to 10 ;
  	Do i = 1 To 10 ;
    	Flag{i} = 0 ;   
  	End ;

* F1 ---------- Gluten Free Code ;  
	If FimGlutenFreeCd = 'Y' Then Flag1 = 1 ;

* F2 ---------- Organic Code ;  
	If FimOrganicCd = 'Y' Then Flag2 = 1 ;

* F3 ---------- Fair Trade Code ;  
	If FimFairTrdCd = 'Y' Then Flag3 = 1 ;

* F4 ---------- Halal Code ;  
	If FimHalalCertCd = 'Y' Then Flag4 = 1 ;

* F5 ---------- Kosher Code ;  
	If FimKosherCertCd = 'Y' Then Flag5 = 1 ;

* F6 ---------- Cdn Made Code ;  
  	*Manually maintained in CKB ; 

* F7 ---------- VPED ;  
  	If (FimLowCostCd ~= ' ' and FimLowCostCd ~= 'N') Then Flag7  = 1 ;

* F8 ---------- ; 
  	If FimCoopLabelCd = 'Y' Then Flag8  = 1 ;


* F9 ---------- ;  
  	If FimSpeedToMktCd = 'Y' Then Flag9 = 1 ;

* F10 ---------- Redirect Code ;  
  	If FimRdrCd = 'Y' Then Flag10  = 1 ;


* Dates --------- ;
	Date1 = FimStatDate ;
	Date2 = Date() ;
	Date3 = Date() ;

* Product Dates --------- ;
	DateCreated		= Date() ;
	DateModified	= Date() ;
	DatePending		= Date() ;
	DateEffective	= Date() ;
	DateFinished	= Date() ;

	DBDateEffectiveFrom	= Date() ;
	DBDateEffectiveTo	= Date() ;

* 276 --------- ;
  	*Brand = Put( (FdsDptNo * 100 + FdsSectNo)*1000 + FdsSectModNo ,XMM30.) ;
	If FimStat In ('9') then Brand = 'ZZ DELETED ITEMS' ;
		Else Brand   = Put (FricMajMinDptMajCatNo 	, FricMajC.) ; 

* 277 --------- ; 
  	SubCategory = Put (FricMajMinDptMajMinCatNo,FricMinC.) ;
	If SubCategory = ' ' Then SubCategory = '-' ;

*DBStatus ; 
	Select ; 
		When (FimStat = '9') 	DBStatus 	= 4		;	*Deleted items in IMFS version to historic status in CKB 	21-AUG-2018 LL ; 
		Otherwise				DBStatus 	= 1		;	*Forcing active items in IMFS to be active in CKB 			21-AUG-2018 LL ; 
	End ;
  	
Run ;

%PDATASET (Save,Ors275b) ;

Data Save.Ors275b ;
  	Merge
    	Ors275b   	(in=i1)
    	Ors107 		(in=i3)
    	Ors276 		(in=i4)
    	;
  	By FimNo ;

  	Array TE       	{*} FimStkStatDate1 - FimStkStatDate5 ;
  	Array DQ       	{*} FimBuyStrDate1  - FimBuyStrDate5 ;
	Array Desc    	{*}	Desc1  			- Desc50 ;


  	File PRTERR Mod ;

  	If i1 ;
  	
  	If i3 Then Do ;
* v31 -> v34 -- Sales ;
    	Value31 = RegShpdTyQty ;
    	Value32 = PromShpdTyQty ;
    	Value33 = SopShpdTyQty ;
    	Value34 = RegShpdTyQty + PromShpdTyQty + SopShpdTyQty ;
    	If Value34 > 0 Then Value35 =  Round (PromShpdTyQty / Value34 * 100 ) ; 
  	End ;

  	If i4 Then Do ;
* CaseCost Price ;
    *	Value11  = 0 ;
    	Value28  = FimTgpRccPrcX ;
    	Price    = FimSrpPrcX ;
    	CaseCost = FimRccPrcX ;
  	End ;

* Product Dates ;

	Select ; 
		When (FimStat = '5') 	DateCreated 	= FimStatDate 	;
		When (FimStat = '9') 	DateCreated 	= FimStatDate 	;
		Otherwise				DateCreated 	= FimStatDate 	;
	End ;

	Select ;
		When (FimStat = '5') 	DateModified 	= Date()		;
		When (FimStat = '9') 	DateModified 	= Date() 		;
		Otherwise				DateModified 	= Date()		;
	End ;

								DateEffective	= Date()		;

	Select ; 
		When (FimStat = '5') 	DateFinished 	= '31Dec2099'd 	;
		When (FimStat = '9') 	DateFinished 	= '31Dec2099'd	;
		Otherwise				DateFinished 	= '31Dec2099'd	;
	End ;

	Select ; 
		When (FimStat = '9') 	DBDateEffectiveFrom 	= .				;
		Otherwise				DBDateEffectiveFrom 	= FimStatDate	;
	End ;

	Select ; 
		When (FimStat = '9') 	DBDateEffectiveTo 	= FimStatDate	;
		Otherwise				DBDateEffectiveTo 	= .				;
	End ;

	
Run ;

*ENDSAS ;
