* Created:(ORS212AA)PC                 02APR09:10:43:20 ;
 Array XB {*}         FimMstItemNo1                         -
                      FimMstItemNo40                        ;

 Array CV {*}         FimStrgItemNo1                        -
                      FimStrgItemNo191                      ;

 Array CW {*}         FimStrgQty1                           -
                      FimStrgQty191                         ;

 Array CY {*} $001    FimStrgReqdCd1                        -
                      FimStrgReqdCd191                      ;

 Array CZ {*}         FimStrgPrcPcnt1                       -
                      FimStrgPrcPcnt191                     ;


* End of Array: (ORS212AA) ;

* Created : (ORS212AA)                      02APR09:10:43:20 ;
 Input
    @    1    FimNo                              S370FF7.
    @    8    FimRecStat                         $EBCDIC1.
    @    9    FimStat                            $EBCDIC1.
    @   10   (FimMstItemNo1            -
              FimMstItemNo40           )        (S370FF7. )

    @  290    FimDesc                            $EBCDIC35.
    @  325   (FimStrgItemNo1           -
              FimStrgItemNo191         )        (S370FF7. )

    @ 1662   (FimStrgQty1              -
              FimStrgQty191            )        (S370FPD3. )

    @ 2235   (FimStrgReqdCd1           -
              FimStrgReqdCd191         )        ($EBCDIC1. )

    @ 2426   (FimStrgPrcPcnt1          -
              FimStrgPrcPcnt191        )        (S370FPD3.2 )

    ;
* End of Input: (ORS212AA) ;

