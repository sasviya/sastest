* Created:(ORS211AA)PC                 28NOV12:11:20:39 ;

* End of Array: (ORS211AA) ; 

* Created : (ORS211AA)                      28NOV12:11:20:39 ;
 Input
    @    1    FcmNo                              S370FZD6.
    @    7    FcmLocNo                           S370FZD3.
    @   10    FcmStat                            $EBCDIC1.
    @   11    FcmStatDate                        S370FZD8.
    @   19    FcmName                            $EBCDIC35.
    @   54    FcmAddr1Na                         $EBCDIC35.
    @   89    FcmAddr2Na                         $EBCDIC35.
    @  124    FcmCityNa                          $EBCDIC35.
    @  159    FcmProvNa                          $EBCDIC2.
    @  161    FcmPostcdNa                        $EBCDIC6.
    @  167    FcmCntryNa                         $EBCDIC10.
    @  177    FcmZipcdNa                         $EBCDIC10.
    @  187    FcmCustCd                          $EBCDIC1.
    @  188    FcmAbrvName                        $EBCDIC15.
    @  203    FwsRgnId                           $EBCDIC1.
    @  204    FcmElectrlNo                       S370FZD2.
    @  206    FcmEmailId                         $EBCDIC60.
    @  266    FcmInvcEmailId                     $EBCDIC60.
    @  326    FcmAltEmailId                      $EBCDIC60.
    @  386    FcmPhNo                            S370FZD10.
    @  396    FcmBankName                        $EBCDIC35.
    @  431    FcmBankAddr1Na                     $EBCDIC35.
    @  466    FcmBankAddr2Na                     $EBCDIC35.
    @  501    FcmBankCityNa                      $EBCDIC35.
    @  536    FcmBankProvNa                      $EBCDIC2.
    @  538    FcmBankPostcdNa                    $EBCDIC6.
    @  544    FcmBankCntryNa                     $EBCDIC10.
    @  554    FcmBankZipcdNa                     $EBCDIC10.
    @  564    FcmRecCd                           $EBCDIC1.
    @  565    FcmFaxNo                           S370FZD10.
    @  575    FcmLocCd                           $EBCDIC1.
    @  576    FcmLocName                         $EBCDIC12.
    @  588    FcmFrtNo                           S370FZD1.
    @  589    FcmFrtRate                         S370FPD3.2
    @  592    FcmStoreDescCd                     $EBCDIC2.
    @  594    FcmConvnceStoreCd                  $EBCDIC1.
    @  595    FcmCommisTerrNo                    S370FZD3.
    @  598    FcmPetrSlsCd                       $EBCDIC2.
    @  600    FcmHdweCd                          $EBCDIC1.
    @  601    FcmFoodCd                          $EBCDIC1.
    @  602    FcmCsCd                            $EBCDIC1.
    @  603    FcmFeedCd                          $EBCDIC1.
    @  604    FcmPetrCd                          $EBCDIC1.
    @  605    FcmGrocFaRate                      S370FPD4.5
    @  609    FcmHdweFaRate                      S370FPD4.5
    @  613    FcmGrocSfRate                      S370FPD3.3
    @  616    FcmHdweSfRate                      S370FPD3.3
    @  619    FcmFfSfRate                        S370FPD3.3
    @  622    FcmMeatSfRate                      S370FPD3.3
    @  625    FcmBulkMeatSfRate                  S370FPD3.3
    @  628    FcmAvgGrocFrtRate                  S370FPD3.2
    @  631    FcmAvgFfFrtRate                    S370FPD3.2
    @  634    FcmAvgHdweFrtRate                  S370FPD3.2
    @  637    FcmAvgGrocSfRate                   S370FPD3.2
    @  640    FcmAvgFfSfRate                     S370FPD3.2
    ;
* End of Input: (ORS211AA) ; 

* Created:(ORS211AA)PC                 28NOV12:11:20:39 ;

* End of Array: (ORS211AA) ; 

