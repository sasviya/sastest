%JobDate4(Ori211x1,Ori274aa,CMF xls Dump,SC) ;
Title 'Ori211x1 - CMF-LOC-INFO' ;
Options NoCenter Mprint Symbolgen Source2 ;
options NoCenter ;

* Libname LIBRARY '\\Usserv02\common\Orm000OrFmt' 		ACCESS=READONLY ;
Libname ORI211  '/sasdata/supplychain/sastest/ori211' 				ACCESS=READONLY ;


Data Ors211 ;
	Set ORI211.Ors211 ;
	Where FcmStat = 'A' and FcmDptNo1 ~= 0 ;
Run ;

ods _all_ close; 
ods listing;
ods csv file='/sasfiles/output/ori211/Ori211x1.csv' ;
proc print data=Ors211 ;
	Id FcmNo FcmLocNo ;
Run ;
Ods csv Close ;

Data _null_ ;
Run ;

*ENDSAS ;
