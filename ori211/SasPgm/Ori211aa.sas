%JobDate4(Ori211aa,Ori274aa,CMF-LOC-INFO,SC) ;
Title 'Ori211aa - CMF-LOC-INFO' ;
Options NoCenter Mprint Symbolgen Macrogen Source2 ;

Libname 	LIBRARY 	'/sasdata/supplychain/sastest/orm000'			Access=READONLY ;
FileName 	RDC     	'/sasdata/supplychain/sastest/sasrdc' ; 

Libname 	ORI211		'/sasdata/supplychain/sastest/ori211' ;
FileName 	ORS211 		'/sasfiles/ftp/ori211/Ors211aa.txt' LRecl=1000 BlkSize=26000 Recfm=f ;	*New FTP fileserver location ; 
FileName 	ORS219 		'/sasfiles/ftp/ori211/Ors219aa.txt' LRecl=1000 BlkSize=26000 Recfm=f ;	*New FTP fileserver location ; 

/* Libname     FORMATS 	cas caslib=formats ; */

%Macro FindFix (a) ;
  &a = Left(Translate ( &a , ' ' , ',' , ' ' , '"' , ' ' , "'") ) ;
%Mend FindFix ;

%Macro FindFixB (a) ;
  &a = Left(Tranwrd ( &a , 'FEDERATED'		, '') ) ;
  &a = Left(Tranwrd ( &a , 'CO-OPERATIVE'	, '') ) ;
  &a = Left(Tranwrd ( &a , 'COOPERATIVE' 	, '') ) ;
  &a = Left(Tranwrd ( &a , 'CO-OP-'      	, '') ) ;
  &a = Left(Tranwrd ( &a , 'CO-OP'       	, '') ) ;
  &a = Left(Tranwrd ( &a , 'COOP'        	, '') ) ;
  &a = Left(Tranwrd ( &a , 'CO OP'       	, '') ) ;
  &a = Left(Tranwrd ( &a , ' ASSN'       	, '') ) ;
  &a = Left(Tranwrd ( &a , ' ASSOC'      	, '') ) ;
  &a = Left(Tranwrd ( &a , 'LTD'         	, '') ) ;
  &a = Left(Tranwrd ( &a , 'LIMITED'     	, '') ) ;
  &a = Left(Tranwrd ( &a , '.'           	, '') ) ;
  &a = Left(Tranwrd ( &a , '   '         	, ' ') ) ;
  &a = Left(Tranwrd ( &a , '  '          	, ' ') ) ;
%Mend FindFixB ;

Proc Format ;
	%comterx ;
Run ;

Proc Format ;
	%cbpx ;
Run ;

* Not used - look in macro library for CBPX ;
 
*  Read in CMF dpt records from ORS219 ;
Data Ors219 ;
  	Infile ORS219 ;
  	Attrib
    	FcmNo                    Format=Z6.     Label='Fcm No'
    	FcmLocNo                 Format=Z3.     Label='Fcm Loc No'
    	FcmDptNo                 Format=Z3.     Label='Fcm Loc No'
    	FcmEosRgnCd              Length=$1.     Label='Fcm Eos Rgn Cd'
    	FcmCstoreCd              Length=$1.     Label='Fcm Cstore Cd'
    	FcmDptSqFtDim            Format=6.      Label='Fcm Dpt Sq Ft Dim'
    	FcmRtlGlDptNo            Format=z2.     Label='Fcm Rtl Gl Dpt No'
    	;
  	%Include RDC(ors219pc) ;
Run ;
Filename ORS219 Clear ;

Proc Sort Data = Ors219 ;
  	By FcmNo FcmLocNo FcmDptNo ;
Run ;

Data Ors219 ;
  	Set Ors219 ;
  	Array GA {*}         FcmDptNo1  	- FcmDptNo19                            ;
  	Array IY {*}         FcmDptSqFtDim1 - FcmDptSqFtDim19                       ;
  	Array ZG {*}         FcmRtlGlDptNo1 - FcmRtlGlDptNo19                       ;
  	Array ER {*}  $1.    FcmEosRgnCd1   - FcmEosRgnCd19                         ;
  	Array CS {*}  $1.    FcmCstoreCd1   - FcmCstoreCd19                         ;

  	By FcmNo FcmLocNo ;

  	Attrib
    	FcmDptNo1                Format=Z3.     Label='Fcm Dpt No 1'
    	FcmDptNo2                Format=Z3.     Label='Fcm Dpt No 2'
    	FcmDptNo3                Format=Z3.     Label='Fcm Dpt No 3'
    	FcmDptNo4                Format=Z3.     Label='Fcm Dpt No 4'
    	FcmDptNo5                Format=Z3.     Label='Fcm Dpt No 5'
    	FcmDptNo6                Format=Z3.     Label='Fcm Dpt No 6'
    	FcmDptNo7                Format=Z3.     Label='Fcm Dpt No 7'
    	FcmDptNo8                Format=Z3.     Label='Fcm Dpt No 8'
    	FcmDptNo9                Format=Z3.     Label='Fcm Dpt No 9'
    	FcmDptNo10               Format=Z3.     Label='Fcm Dpt No 10'
    	FcmDptNo11               Format=Z3.     Label='Fcm Dpt No 11'
    	FcmDptNo12               Format=Z3.     Label='Fcm Dpt No 12'
    	FcmDptNo13               Format=Z3.     Label='Fcm Dpt No 13'
    	FcmDptNo14               Format=Z3.     Label='Fcm Dpt No 14'
    	FcmDptNo15               Format=Z3.     Label='Fcm Dpt No 15'
    	FcmDptNo16               Format=Z3.     Label='Fcm Dpt No 16'
    	FcmDptNo17               Format=Z3.     Label='Fcm Dpt No 17'
    	FcmDptNo18               Format=Z3.     Label='Fcm Dpt No 18'
    	FcmDptNo19               Format=Z3.     Label='Fcm Dpt No 19'

    	FcmDptSqFtDim1           Format=6.      Label='Fcm Dpt SqFt Dim 1'
    	FcmDptSqFtDim2           Format=6.      Label='Fcm Dpt SqFt Dim 2'
    	FcmDptSqFtDim3           Format=6.      Label='Fcm Dpt SqFt Dim 3'
    	FcmDptSqFtDim4           Format=6.      Label='Fcm Dpt SqFt Dim 4'
    	FcmDptSqFtDim5           Format=6.      Label='Fcm Dpt SqFt Dim 5'
    	FcmDptSqFtDim6           Format=6.      Label='Fcm Dpt SqFt Dim 6'
    	FcmDptSqFtDim7           Format=6.      Label='Fcm Dpt SqFt Dim 7'
    	FcmDptSqFtDim8           Format=6.      Label='Fcm Dpt SqFt Dim 8'
    	FcmDptSqFtDim9           Format=6.      Label='Fcm Dpt SqFt Dim 9'
    	FcmDptSqFtDim10          Format=6.      Label='Fcm Dpt SqFt Dim 10'
    	FcmDptSqFtDim11          Format=6.      Label='Fcm Dpt SqFt Dim 11'
    	FcmDptSqFtDim12          Format=6.      Label='Fcm Dpt SqFt Dim 12'
    	FcmDptSqFtDim13          Format=6.      Label='Fcm Dpt SqFt Dim 13'
    	FcmDptSqFtDim14          Format=6.      Label='Fcm Dpt SqFt Dim 14'
    	FcmDptSqFtDim15          Format=6.      Label='Fcm Dpt SqFt Dim 15'
    	FcmDptSqFtDim16          Format=6.      Label='Fcm Dpt SqFt Dim 16'
    	FcmDptSqFtDim17          Format=6.      Label='Fcm Dpt SqFt Dim 17'
    	FcmDptSqFtDim18          Format=6.      Label='Fcm Dpt SqFt Dim 18'
    	FcmDptSqFtDim19          Format=6.      Label='Fcm Dpt SqFt Dim 19'

    	FcmRtlGlDptNo1           Format=Z2.     Label='Fcm Rtl GLDpt No 1'
    	FcmRtlGlDptNo2           Format=Z2.     Label='Fcm Rtl GLDpt No 2'
    	FcmRtlGlDptNo3           Format=Z2.     Label='Fcm Rtl GLDpt No 3'
    	FcmRtlGlDptNo4           Format=Z2.     Label='Fcm Rtl GLDpt No 4'
    	FcmRtlGlDptNo5           Format=Z2.     Label='Fcm Rtl GLDpt No 5'
    	FcmRtlGlDptNo6           Format=Z2.     Label='Fcm Rtl GLDpt No 6'
    	FcmRtlGlDptNo7           Format=Z2.     Label='Fcm Rtl GLDpt No 7'
    	FcmRtlGlDptNo8           Format=Z2.     Label='Fcm Rtl GLDpt No 8'
    	FcmRtlGlDptNo9           Format=Z2.     Label='Fcm Rtl GLDpt No 9'
    	FcmRtlGlDptNo10          Format=Z2.     Label='Fcm Rtl GLDpt No 10'
    	FcmRtlGlDptNo11          Format=Z2.     Label='Fcm Rtl GLDpt No 11'
    	FcmRtlGlDptNo12          Format=Z2.     Label='Fcm Rtl GLDpt No 12'
    	FcmRtlGlDptNo13          Format=Z2.     Label='Fcm Rtl GLDpt No 13'
    	FcmRtlGlDptNo14          Format=Z2.     Label='Fcm Rtl GLDpt No 14'
    	FcmRtlGlDptNo15          Format=Z2.     Label='Fcm Rtl GLDpt No 15'
    	FcmRtlGlDptNo16          Format=Z2.     Label='Fcm Rtl GLDpt No 16'
    	FcmRtlGlDptNo17          Format=Z2.     Label='Fcm Rtl GLDpt No 17'
    	FcmRtlGlDptNo18          Format=Z2.     Label='Fcm Rtl GLDpt No 18'
    	FcmRtlGlDptNo19          Format=Z2.     Label='Fcm Rtl GLDpt No 19'

    	FcmEosRgnCd1             Length=$1.     Label='Fcm Eos Rgn Cd 1'
    	FcmEosRgnCd2             Length=$1.     Label='Fcm Eos Rgn Cd 2'
    	FcmEosRgnCd3             Length=$1.     Label='Fcm Eos Rgn Cd 3'
    	FcmEosRgnCd4             Length=$1.     Label='Fcm Eos Rgn Cd 4'
    	FcmEosRgnCd5             Length=$1.     Label='Fcm Eos Rgn Cd 5'
    	FcmEosRgnCd6             Length=$1.     Label='Fcm Eos Rgn Cd 6'
    	FcmEosRgnCd7             Length=$1.     Label='Fcm Eos Rgn Cd 7'
    	FcmEosRgnCd8             Length=$1.     Label='Fcm Eos Rgn Cd 8'
    	FcmEosRgnCd9             Length=$1.     Label='Fcm Eos Rgn Cd 9'
    	FcmEosRgnCd10            Length=$1.     Label='Fcm Eos Rgn Cd 10'
    	FcmEosRgnCd11            Length=$1.     Label='Fcm Eos Rgn Cd 11'
    	FcmEosRgnCd12            Length=$1.     Label='Fcm Eos Rgn Cd 12'
    	FcmEosRgnCd13            Length=$1.     Label='Fcm Eos Rgn Cd 13'
    	FcmEosRgnCd14            Length=$1.     Label='Fcm Eos Rgn Cd 14'
    	FcmEosRgnCd15            Length=$1.     Label='Fcm Eos Rgn Cd 15'
    	FcmEosRgnCd16            Length=$1.     Label='Fcm Eos Rgn Cd 16'
    	FcmEosRgnCd17            Length=$1.     Label='Fcm Eos Rgn Cd 17'
    	FcmEosRgnCd18            Length=$1.     Label='Fcm Eos Rgn Cd 18'
    	FcmEosRgnCd19            Length=$1.     Label='Fcm Eos Rgn Cd 19'
    	
    	FcmCstoreCd1             Length=$1.     Label='Fcm Cstore Cd 1'
    	FcmCstoreCd2             Length=$1.     Label='Fcm Cstore Cd 2'
    	FcmCstoreCd3             Length=$1.     Label='Fcm Cstore Cd 3'
    	FcmCstoreCd4             Length=$1.     Label='Fcm Cstore Cd 4'
    	FcmCstoreCd5             Length=$1.     Label='Fcm Cstore Cd 5'
    	FcmCstoreCd6             Length=$1.     Label='Fcm Cstore Cd 6'
    	FcmCstoreCd7             Length=$1.     Label='Fcm Cstore Cd 7'
    	FcmCstoreCd8             Length=$1.     Label='Fcm Cstore Cd 8'
    	FcmCstoreCd9             Length=$1.     Label='Fcm Cstore Cd 9'
    	FcmCstoreCd10            Length=$1.     Label='Fcm Cstore Cd 10'
    	FcmCstoreCd11            Length=$1.     Label='Fcm Cstore Cd 11'
    	FcmCstoreCd12            Length=$1.     Label='Fcm Cstore Cd 12'
    	FcmCstoreCd13            Length=$1.     Label='Fcm Cstore Cd 13'
    	FcmCstoreCd14            Length=$1.     Label='Fcm Cstore Cd 14'
    	FcmCstoreCd15            Length=$1.     Label='Fcm Cstore Cd 15'
    	FcmCstoreCd16            Length=$1.     Label='Fcm Cstore Cd 16'
    	FcmCstoreCd17            Length=$1.     Label='Fcm Cstore Cd 17'
    	FcmCstoreCd18            Length=$1.     Label='Fcm Cstore Cd 18'
    	FcmCstoreCd19            Length=$1.     Label='Fcm Cstore Cd 19'
    	;

  	Retain
    	FcmDptNo1      - FcmDptNo19
    	FcmDptSqFtDim1 - FcmDptSqFtDim19
    	FcmRtlGlDptNo1 - FcmRtlGlDptNo19
    	FcmEosRgnCd1   - FcmEosRgnCd19
    	FcmCstoreCd1   - FcmCstoreCd19
    	;

  	Keep
    	FcmNo
    	FcmLocNo
    	FcmEosRgnCd
    	FcmCstoreCd
    	FcmDptNo1      - FcmDptNo19
    	FcmDptSqFtDim1 - FcmDptSqFtDim19
    	FcmRtlGlDptNo1 - FcmRtlGlDptNo19
    	FcmEosRgnCd1   - FcmEosRgnCd19
    	FcmCstoreCd1   - FcmCstoreCd19
    	;

  	If First.FcmLocNo Then Do ;
    	Do i = 1 to Dim(GA) ;
      		GA{i} = 0 ;
      		IY{i} = 0 ;
      		ZG{i} = 0 ;
      		ER{i} = ' ' ;
      		CS{i} = ' ' ;
    	End ;
    	LocCnt = 0 ;
  	End ;

  	LocCnt + 1 ;

  	If LocCnt <= Dim(GA) Then Do ;
    	GA{LocCnt} = FcmDptNo      ;
    	IY{LocCnt} = FcmDptSqFtDim ;
    	ZG{LocCnt} = FcmRtlGlDptNo ;
    	ER{LocCnt} = FcmEosRgnCd   ;
    	CS{LocCnt} = FcmCstoreCd   ;
  	End ;

  	Else Do ;
    	Put 'Array Overflow ' _all_ ;
  	End ;

  	If Last.FcmLocNo Then Output ;
Run ;
                                             
*  Read in CMF  records from ORS211 ;

Data Ors211 ;
  	Infile ORS211 ;

  	Attrib
    	FcmNo				Format=Z6.						Label='Fcm No'
    	FcmLocNo        	Format=Z3.     					Label='Fcm Loc No'
    	FcmStat         	Format=$1.		Length=$1.  	Label='Fcm Stat'
    	FcmStatDate     	Format=Date9.  					Label='Fcm Stat Date'
    	FcmName         	Format=$35.     Length=$35. 	Label='Fcm Name'
    	FcmNameX        	Format=$35.     Length=$35. 	Label='Fcm Name X'
    	FcmAddr1Na      	Format=$35.     Length=$35.    	Label='Fcm Addr1 Na'
    	FcmAddr2Na      	Format=$35.     Length=$35.    	Label='Fcm Addr2 Na'                            
    	FcmCityNa       	Format=$35.     Length=$35.    	Label='Fcm City Na'                   
    	FcmProvNa       	Format=$2.      Length=$2.     	Label='Fcm Prov Na'
    	FcmPostcdNa     	Format=$6.      Length=$6.     	Label='Fcm Postcd Na'
    	FcmCntryNa      	Format=$10.     Length=$10.    	Label='Fcm Cntry Na'
    	FcmZipcdNa      	Format=$10.     Length=$10.    	Label='Fcm Zipcd Na'
    	FcmCustCd       	Format=$1.      Length=$1.     	Label='Fcm Cust Cd'
    	FcmAbrvName     	Format=$15.     Length=$15.    	Label='Fcm Abvr Name'
    	FwsRgnId        	Format=$1.      Length=$1.     	Label='Fws Rgn Id'
    	FcmElectrlNo    	Format=Z2.     					Label='Fcm Electrl No'
    	FcmEmailId      	Format=$60.     Length=$60.    	Label='Fcm Email Id'
		FcmInvcEmailId		Format=$60.		Length=$60.		Label='Fcm Invc Email Id'
		FcmAltEmailId      	Format=$60.     Length=$60.    	Label='Fcm Alt Email Id'
    	FcmPhNo         	Format=Z10.    					Label='Fcm Ph No'
    	FcmRecCd        	Format=$1.      Length=$1.     	Label='Fcm Rec Cd'
    	FcmFaxNo        	Format=Z10.    					Label='Fcm Fax No'
		FcmLocCd			Format=$1.		Length=$1.		label='Fcm Loc Cd'
    	FcmLocName      	Format=$12.     Length=$12.    	Label='Fcm Loc Name'
    	FcmLocNameX     	Format=$12.     Length=$12.    	Label='Fcm Loc Name X'
    	FcmFrtNo        	Format=Z1.     					Label='Fcm Frt No'
    	FcmFrtRate      	Format=6.2     					Label='Fcm Frt Rate'
		FcmStoreDescCd		Format=$2.		Length=$2.		Label='Fcm Store Desc Cd'
		FcmConvnceStoreCd	Format=$1.		Length=$1.		Label='Fcm Convnce Store Cd'
    	FcmCommisTerrNo 	Format=Z3.     					Label='Fcm Commis Terr No'
    	OvrCommisTerrNo 	Format=Z3.     					Label='Ovr Commis Terr No'
    	FcmPetrSlsCd    	Format=$2.      Length=$2.     	Label='Fcm Petr Sls Cd'
    	FcmHdweCd       	Format=$1.      Length=$1.     	Label='Fcm Hdwe Cd'
    	FcmFoodCd       	Format=$1.      Length=$1.     	Label='Fcm Food Cd'
    	FcmCsCd         	Format=$1.      Length=$1.     	Label='Fcm Cs Cd'
    	FcmFeedCd       	Format=$1.      Length=$1.     	Label='Fcm Feed Cd'
    	FcmPetrCd       	Format=$1.      Length=$1.     	Label='Fcm Petr Cd'
    	FcmGrocFaRate   	Format=8.5     					Label='Fcm Groc FA Rate'
    	FcmHdweFaRate   	Format=8.5     					Label='Fcm Hdwe FA Rate'
    	FcmGrocSfRate   	Format=6.3     					Label='Fcm Groc SF Rate'
    	FcmHdweSfRate   	Format=6.3     					Label='Fcm Hdwe SF Rate'
    	FcmFfSfRate     	Format=6.3     					Label='Fcm FF SF Rate'
    	FcmMeatSfRate   	Format=6.3     					Label='Fcm Meat SF Rate'
    	FcmBulkMeatSfRate 	Format=6.3                    	Label='Fcm Bulk Meat SF Rate'
    	FcmAvgGrocFrtRate   Format=6.2                      Label='Fcm Avg Groc Frt Rate'
    	FcmAvgFfFrtRate     Format=6.2                      Label='Fcm Avg FF Frt Rate'
    	FcmAvgHdweFrtRate   Format=6.2                      Label='Fcm Avg Hdwe Frt Rate'
    	FcmAvgGrocSfRate    Format=6.2                      Label='Fcm Avg Groc Sf Rate'
    	FcmAvgFfSfRate      Format=6.2                      Label='Fcm Avg FF Sf Rate'
    	FcmNoLocNo          Format=Z9.     					Label='Fcm No Loc No'
    	FcmCommisTerr1No    Format=Z2.     					Label='Fcm Commis Terr1 No'
    	FcmCommisTerr2No    Format=Z1.     					Label='Fcm Commis Terr2 No'
    	MarketId            Format=$1.     	Length=$1.      Label='Market Id'
    	ProvinceId          Format=$2.     	Length=$2.      Label='Province Id'
    	;

  	%Include RDC(ors211pc) ;

  	%CNVTDATE(FcmStatDate) ;

  	FcmNoLocNo = FcmNo * 1000 + FcmLocNo ;
  	FcmCommisTerr1No = Int (FcmCommisTerrNo / 10) ;
  	FcmCommisTerr2No = Mod (FcmCommisTerrNo , 10) ;

  	Length TC $ 80 ;
  	Drop TC ;

* Use the Commission Territory to get the province for petroleum sls ;
  	TC = Put (FcmCommisTerrNo , ComTerX.) ;
  	MarketId = '-' ;
  	ProvinceId = '--' ;
  	If Substr (TC , 4 , 1) = '.' then do ;
    	MarketId   = Substr (TC , 8 , 1) ;
    	ProvinceId = Substr (TC , 5 , 2) ;
  	End ;

  	OvrCommisTerrNo = . ;

  	If FcmCommisTerrNo >= 160 Then Do ;
    	If FcmPetrSlsCd = 'TL' Then Do ;
      		TC = Put (FcmNo , CBP. ) ;
      		If Substr (TC , 7 , 1 ) = '.' Then Do ;
        		OvrCommisTerrNo = Input ( Substr ( TC , 8 , 3 ) , 3. ) ;
        		TC = Put (OvrCommisTerrNo , ComTerX.) ;
        		MarketId = '-' ;
        		ProvinceId = '--' ;
        		If Substr (TC , 4 , 1) = '.' Then Do ;
          			ProvinceId = Substr (TC , 5 , 2) ;
        		End ;
      		End ;
    	End ;

    	If FcmPetrSlsCd in ('CC','TK') Then Do ;
      		Select (FcmProvNa) ;
        		When ('BC') OvrCommisTerrNo = 032 ;
        		When ('AB') OvrCommisTerrNo = 062 ;
        		When ('SK') OvrCommisTerrNo = 122 ;
        		When ('MB') OvrCommisTerrNo = 152 ;
        		When ('MA') OvrCommisTerrNo = 152 ;
        		Otherwise ;
      		End ;

      		If OvrCommisTerrNo ~= . Then Do ;
        		TC = Put (OvrCommisTerrNo , ComTerX.) ;
        		MarketId = '-' ;
        		ProvinceId = '--' ;
        		If Substr (TC , 4 , 1) = '.' Then Do ;
          			ProvinceId = Substr (TC , 5 , 2) ;
        		End ;
      		End ;
    	End ;
  	End ;

	If FcmNo in (
		1691	/* Medicine Hat 				*/
		0102	/* Arrowwood					*/
		2013	/* Pincher Creek				*/
		2788	/* South Central ie Vauxhall	*/
		6039	/* Linden						*/
		) Then ProvinceId='AB' ;

	If FcmNo in (
		6043	/* Meacham		 				*/
		) Then ProvinceId='SK' ;


* Fix up variable values, strip out redundant info ;

  	FcmNameX    = FcmName ;
  	FcmLocNameX = FcmLocName ;

  	%FindFix(FcmNameX)     ;
  	%FindFix(FcmLocNameX)  ;

  	%FindFixB(FcmNameX)    ;
  	%FindFixB(FcmLocNameX) ;

  	%FindFix(FcmAddr1Na)   ;
  	%FindFix(FcmAddr2Na)   ;
  	%FindFix(FcmProvNa)    ;
  	%FindFix(FcmPostCdNa)  ;
  	%FindFix(FcmName)      ;
  	%FindFix(FcmLocName)   ;

Run ;
Filename ORS211 Clear ;

Proc Sort Data=Ors211 ;
  	By FcmNo FcmLocNo ;
Run ;

Data Ors211 ;
  	Merge
    	Ors211 (in=i1)
    	Ors219 (in=i2)
    	;
  	By FcmNo FcmLocNo ;
  	If i1 ;
  	Drop i ;
  	Array GA {*} FcmDptNo1      - FcmDptNo19      ;
  	Array IY {*} FcmDptSqFtDim1 - FcmDptSqFtDim19 ;                
  	Array ZG {*} FcmRtlGlDptNo1 - FcmRtlGlDptNo19 ;               
  	Do i = 1 to Dim(GA) ;
    	If GA{i} = . Then GA{i} = 0 ;
    	If IY{i} = . Then IY{i} = 0 ;
    	If ZG{i} = . Then ZG{i} = 0 ;
  	End ;
Run ;

Proc Sort Data=Ors211 Out=ori211.ors211 (Label='Customer Master File') ;
  	By FcmNoLocNo ;
Run ;


proc fmtc2itm catalog=(ori211.formats) 
print locale itemstore="/sasdata/supplychain/sastestori211/formatItemStore";
run;

cas casauto host="FSR1VLSASVIYA.res.ad.crs" port=5570 ; 
cas casauto addfmtlib fmtlibname="ORI211"    
   path="/sasdata/supplychain/sastest/ori211/formatItemStore"
   replacefmtlib;
   
cas casauto promotefmtlib fmtlibname=ORI211 replace;

cas casauto savefmtlib fmtlibname=ORI211 
	caslib=formats table=ORI211 replace ; 
	
cas casauto listformat fmtlibname="ORI211"     
   members;

Proc Casutil incaslib="Formats" outcaslib="Formats"; 
	
	Droptable casdata="ORI211" ;
	
	Load casdata="ORI211.sashdat" 
		importoptions=(filetype="hdat") 
		casout = "ORI211" replace ;  
	
	Promote casdata="ORI211" ; 
Quit ; 
	  
cas _all_ terminate ;

Libname ORI211 Clear ; 

Data _null_ ;
Run ;
