/* %JobDateX(Orm000aa,Ori274aa) ; */
Title 'Orm000aa - OR Formats' ;
Options NoCenter Mprint Symbolgen Macrogen Source2 ;

Libname 	LIBRARY 	'/sasdata/supplychain/sastest/orm000'	;
FileName 	RDC     	'/sasdata/supplychain/sastest/sasrdc' 	; 

%macro StripChar (VarX ) ;
  &VarX = TRANSLATE (&VarX , ' ' , ',' , ' ' , '"' , ' ' , "'" ) ;
%mend StripChar ;

Libname		ORI211	'/sasdata/supplychain/sastest/ori211'	Access=Readonly ;
Data Ors211 ;
  	Set ORI211.Ors211 ;
  	Length
   		FmtName $ 12
    	Label   $ 40
    	;
  	Format
    	FcmNo      Z6.
    	FcmNoLocNo Z9.
    	;

  	Keep
    	FmtName
    	FcmNo
    	FcmLocNo
    	FcmNoLocNo
    	FwsRgnId
    	Start
    	Label
    	;

  	If FcmLocNo = 000 Then Do ;
    	FmtName = 'FcmNo' ;
    	Start   = FcmNo ;
    	Label   = Put (FcmNo , Z4.) || '.' || FwsRgnId || '.' || FcmName ;
    	Output ;
  	End ;

  	FcmNoLocNo 	= FcmNo * 1000 + FcmLocNo ;
  	FmtName 	= 'FcmLocNo' ;
  	Start   	= FcmNoLocNo ;
  	If FcmLocName = ' ' Then 	Label = Put (FcmNoLocNo , z9.) || '.' || FcmName ;
  	Else                		Label = Put (FcmNoLocNo , z9.) || '.' || FcmLocName ;

  	Output ;
Run ;
Libname ORI211 Clear ;

Proc Sort Data=Ors211 NoDupKey ;
  	By FmtName Start ;
Run ;

Proc Format Library=LIBRARY Cntlin=Ors211 ;
Run ;

* Create Vendor Name format ;
Libname		ORI274	'/sasdata/ori274'	Access=Readonly ;

Data Ors274 ;
  	Set ORI274.Ors274 ;
  	By FvmNo FwsRgnId ;
  	If First.FvmNo ;
  	Length Start $ 5 label $ 50 ;
  	Start = Put (FvmNo , z5.) ;
  	Fmtname = '$VNM' ;
  	Label = Left (FvmName) ;
Run ;

Proc Format Cntlin=Ors274 Library=LIBRARY ;
Run ;

Data Ors274 ;
  	Set ORI274.Ors274 ;
  	By FvmNo FwsRgnId ;
  	If First.FvmNo ;
  	Length label $ 50 ;
  	Start = FvmNo ;
  	Fmtname = 'VendNa' ;
  	Label = Left (FvmName) ;
run ;

Proc Format Cntlin=Ors274 Library=LIBRARY ;
Run ;

Libname ORI274 Clear ;

*  READ IN smgs FILE for Tgp Group and Family ;
Libname		ORI268	'/sasdata/ori268'	Access=Readonly ;

Data
  	FdsTgpGrp     (Keep= FdsDptNo FdsTgpGrpNo FdsTgpGrpName)
  	FdsTgpFamily  (Keep= FdsDptNo FdsTgpFamilyNo FdsTgpFamilyName)
  	;
  	Set ORI268.Ors268 ;

  	FdsTgpFamilyName = FdsTgpFamilyName1 ;

  	%StripChar (FdsTgpGrpName)
  	%StripChar (FdsTgpFamilyName) ;

  	If FdsTgpGrpNo > 0 and FdsTgpGrpName ~= ' ' 		then Output FdsTgpGrp ;
  	If FdsTgpFamilyName ~= ' ' and FdsTgpFamilyNo > 0 	then Output FdsTgpFamily ;
Run ;
Libname ORI268 Clear ;

Proc Sort Data=FdsTgpGrp ;
  	By FdsTgpGrpNo ;
Run ;

Data FdsTgpGrp ;
  	Set FdsTgpGrp ;
  	By FdsTgpGrpNo ;
  	If first.FdsTgpGrpNo ;
  	FmtName = 'TGRP' ;
  	Start = FdsTgpGrpNo ;
  	Length label $ 50 ;
  	Label = put (FdsTgpGrpNo , z2.) || '.' || FdsTgpGrpName ;
Run ;

Proc Format Cntlin=FdsTgpGrp Library=LIBRARY ;
Run ;

Proc Sort Data=FdsTgpFamily ;
  	By FdsTgpFamilyNo ;
Run ;

Data FdsTgpFamily ;
  	Set FdsTgpFamily ;
  	By FdsTgpFamilyNo ;
  	If First.FdsTgpFamilyNo ;
  	Fmtname = 'TFAM' ;
  	Start = FdsTgpFamilyNo ;
  	Length Label $ 50 ;
  	Label = put (FdsTgpFamilyNo , z6.) || '.' || FdsTgpFamilyName ;
Run ;

Proc Format Cntlin=FdsTgpFamily Library=LIBRARY ;
Run ;

*  READ IN smgs FILE ;
Libname		ORI253	'/sasdata/ori253'	Access=Readonly ;

Data
  	FdsDpt        (Keep= FdsDptNo FdsDptName)
  	FdsSect       (Keep= FdsDptNo FdsSectNo FdsSectName)
  	FdsSectMod    (Keep= FdsDptNo FdsSectNo FdsSectModNo FdsModName)
  	FdsGrp        (Keep= FdsDptNo FdsSectNo FdsSectModNo FdsGrpNo FdsGrpName FdsMassUpdtCd )
  	;

  	Set ORI253.Ors253 ;

  	%StripChar (FdsDptName) ;
  	%StripChar (FdsSectName) ;
  	%StripChar (FdsModName) ;
  	%StripChar (FdsGrpName) ;

  	If FdsRecCd = '1' and FdsSectNo    = 0 then delete ;
  	If FdsRecCd = '2' and FdsSectModNo = 0 then delete ;
  	If FdsRecCd = '3' and FdsGrpNo     = 0 then delete ;

  	Select (FdsRecCd) ;
    	When ('0') Output FdsDpt ;
    	When ('1') Output FdsSect ;
   	 	When ('2') Output FdsSectMod ;
    	When ('3') Output FdsGrp ;
    	Otherwise Delete ;
  	End ;
Run ;
Libname ORI253 Clear ;

Proc Sort Data=FdsDpt ;
  	By FdsDptNo ;
Run ;

Proc Sort Data=FdsSect ;
  	By FdsDptNo FdsSectNo ;
Run ;

Data FdsSect ;
  	Set FdsSect ;
  	By FdsDptNo FdsSectNo ;
  	If first.FdsSectNo ;
  	FmtName = 'XSS' ;
  	Start = FdsDptNo * 100 + FdsSectNo ;
  	Length label $ 50 ;
  	Label = put (FdsSectNo , z2.) || '.' || FdsSectName ;
run ;

Proc Format Cntlin=FdsSect Library=LIBRARY ;
Run ;

Proc Sort Data=FdsSectMod ;
  	By FdsDptNo FdsSectNo FdsSectModNo ;
Run ;

Data FdsSectMod ;
  	Set FdsSectMod ;
  	By FdsDptNo FdsSectNo FdsSectModNo ;
  	If First.FdsSectModNo ;
  	FmtName = 'XMM' ;
  	Start = (FdsDptNo * 100 + FdsSectNo ) * 1000 + FdsSectModNo ;
  	Length Label $ 50 ;
  	Label = Put (FdsSectModNo , z2.) || '.' || FdsModName ;
Run ;

Proc Format Cntlin=FdsSectMod Library=LIBRARY ;
Run ;

Proc Sort Data=FdsGrp ;
  	By FdsDptNo FdsSectNo FdsGrpNo ;
Run ;

Data FdsGrp ;
  	Set FdsGrp ;
  	By FdsDptNo FdsSectNo FdsGrpNo ;
  	If first.FdsGrpNo ;
  	FmtName = 'XGG' ;
  	Start = (FdsDptNo * 100 + FdsSectNo ) * 1000 + FdsGrpNo ;
  	Length Label $ 50 ;
  	Label = Put (FdsGrpNo , z3.) || '.' || FdsMassUpdtCd || '.' || FdsGrpName ;
Run ;

Proc Format Cntlin=FdsGrp Library=LIBRARY ;
Run ;

Proc Format Library=LIBRARY ;
  	Value CalMon
    	01 = 'Jan'
    	02 = 'Feb'
    	03 = 'Mar'
    	04 = 'Apr'
    	05 = 'May'
    	06 = 'Jun'
    	07 = 'Jul'
    	08 = 'Aug'
    	09 = 'Sep'
   		10 = 'Oct'
   		11 = 'Nov'
   		12 = 'Dec'
    	;

  	Value $StkStat
    	'A' = 'A.Active'
    	'E' = 'E.Supp Out'
    	'G' = 'G.Seas Delete'
    	'J' = 'J.Out New Pack'
    	'M' = 'M.Delete'
    	'P' = 'P.Promotion'
    	'S' = 'S.Sop'
    	'R' = 'R.Delete Deplete'
    	;

  	Value $RPS
    	'R' = 'Reg'
    	'P' = 'Pro'
    	'S' = 'Sop'
    	'T' = 'Tot'
    	' ' = 'Tot'
        ;

  	Value $RESP
    	'AAA' = 'AAA Total'
    	'FC'  = 'FCL Crop'
    	'FE'  = 'FCL Feed'
    	'FT'  = 'FCL Tire'
    	'FS'  = 'FCL Stat'
    	'FF'  = 'FCL Food'
    	'FH'  = 'FCL G.M.'
    	'FX'  = 'FCL XXXX'
    	'TGP' = 'TGP'
    	'TPP' = 'TPP'
     	;

*  Manager Email addresses ;
  	Value $MEmail

   		'$' = 'Ken.Bartsch@fcl.crs'
   		'W' = 'Daniel.Caldwell@fcl.crs' 
   		
   		'Y',
   		'(' = 'Kimball.Lischynski@fcl.crs' 

   		'J',
   		'@',

   		'Q',
   		'R',
   		'S',
   		'T',
   		'U',
		'V',
   		'4' = 'Randy.Seifert@fcl.crs'

   		'7' = 'David.Thomson@fcl.crs'

   		'#' ,
   		'E' ,
   		'H' ,
   		'O' ,
   		'L' ,
   		'X' ,
		'6' ,
   		'9' = 'lindsay.young@fcl.crs'

		'0' ,
		'2' ,
		'G' ,
		'N' ,
		'Z' = 'Doug.Robertson@fcl.crs'

		'3',
   		'A',
   		'B',
   		'C',
   		'D',
   		'F',
   		'I',
   		'K',
   		'M',
   		'P', 
   		'1',
   		'+' = 'Patrick.Ashcroft@fcl.crs'			

   		'5' = '?'

    	Other = '?'
    	;

*  Buyer Email addresses ;
  	Value $BEmail

   		'$' = 'John.Hildebrand@fcl.crs'
   		'W' = 'Nicole.Smith-Windsor@fcl.crs'
   		
   		'Y',
   		'(' = 'Shairy.Nayyar@fcl.crs'

   		'@' = 'Lauren.Rivey@fcl.crs'
   		'J' = 'Julius.Fernandes@fcl.crs'	 

   		'Q' , 
   		'R' , 
   		'S' , 
   		'T' , 
   		'U' , 
		'V' ,
   		'4' = 'Kari.Schira@fcl.crs'    

   		'7' = 'Tim.VanPinxteren@fcl.crs'

		'#' ,
		'E' ,
		'H' = 'Brian.Hampton@fcl.crs' 
		'X' = 'Miso.Acimovic@fcl.crs' 
   		'9' = 'David.Wingerak@fcl.crs'  
		'L' ,
   		'6' = 'Bonny.Sabo@fcl.crs'

		'3' = 'Jellah.Mondigo@fcl.crs'
   		'A' = 'Colin.Pidperyhora@fcl.crs'
   		'B' = 'Michael.Yantz@fcl.crs'
   		'C' = 'Jason.Goodwin@fcl.crs'
   		'D' = 'Johnnie.Fong@fcl.crs'
   		'F' = 'Hudson.Isaak@fcl.crs'
   		'I' = 'Byron.Mann@fcl.crs'	    
   		'K' = 'Erin.Scrobe@fcl.crs'
   		'M' = 'Jeremy.Kweens@fcl.crs'			
   		'P' = 'Mikael.Drabyk@fcl.crs' 
   		'1' = 'Dawn.Davey@fcl.crs'
   		'+' = 'Dawn.Davey@fcl.crs'

   		'5' = '?'

    	Other = '?'
    	;

*  Replenisher Email addresses ;
  	Value $REmail
   		'$' = 'Jellah.Mondigo@fcl.crs'
   		'W' = 'Tracy.Wickstrom@fcl.crs'
   		
   		'Y',
   		'(' = 'Deonne.Hrynyk@fcl.crs'			

   		'Q' = 'Joel.Tomtene@fcl.crs'
   		'R' = 'Bobby.Lee@fcl.crs'
   		'S' = 'Kristan.Shwydiuk@fcl.crs'
   		'T' = 'Jessica.Thompson-Taylor@fcl.crs'
   		'U' = 'Jerry.Sawchuk@fcl.crs'
		'V' = 'Monette.Vocalan@fcl.crs'
   		'4' = 'Connie.McConnell@fcl.crs'

   		'J' = 'Julius.Fernandes@fcl.crs'
   		'@' = 'Lauren.Rivey@fcl.crs'

   		'7' = 'Shane.Hrycan@fcl.crs'           		
   		
   		'G' = 'lavo.gara@fcl.crs'				
   		'N' = 'Aatif.Taher@fcl.crs'
   		'Z' = 'lori.gizoski@fcl.crs'
   		'0' = 'Doug.Robertson@fcl.crs'
   		'2' = 'Doug.Robertson@fcl.crs'

   		'#' = 'Shawn.Coates@fcl.crs'
   		'H' = 'Dave.Flynn@fcl.crs'
   		'E' = 'Brenda.Martin@fcl.crs'
   		'L' = 'Sherryann.Veronelly@fcl.crs'
   		'X' = 'Irene.Hipfner@fcl.crs'
		'9' = 'Eleana.Sahmkow@fcl.crs'

		'3' = 'Jellah.Mondigo@fcl.crs'	
   		'A' = 'Brian.Schindel@fcl.crs'
   		'B' = 'Marife.Carlos@fcl.crs'  
   		'C' = 'Aleya.Ruiz-lumictin@fcl.crs'     
   		'D' = 'Brian.Schindel@fcl.crs'    
   		'F' = 'Aleya.Ruiz-lumictin@fcl.crs'     
   		'I' = 'Marife.Carlos@fcl.crs'   
   		'K' = 'Jellah.Mondigo@fcl.crs'     
   		'M' = 'Marife.Carlos@fcl.crs'    
   		'P' = 'Jellah.Mondigo@fcl.crs'    
   		'1' = 'Brian.Schindel@fcl.crs'
   		'+' = 'Brian.Schindel@fcl.crs'

   		'5' = '?'

    	Other = '?'
    	;

  	Value $NBUY
    	'&' ='&Total'
    	'*' ='*ALL BUYERS'
    	'?' ='TRANSFERS CROSS DOCK'

    	'$'='$.S Penner   SPDCR3185'
    	'W'='W.T WickstromTWDCR3318'
    	'Y'='Y.S Nayyar   SNDCR4775'
    	'('='(.S Nayyar   SNDFE4775'

    	'J'='J.J FernandesJFDFO4572'
    	'@'='@.L Rivey    LRDFO4113'

    	'Q'='Q.J Tomtene  RSDFO3253'
    	'R'='R.B Lee      BLDFO3254'
    	'S'='S.K Shwydiuk KSDFO3252'
    	'T'='T.J Thompson JTDFO4590'
    	'U'='U.J Sawchuk  JTDFO3253'
		'V'='V.M Vocalan  MVDFO3366'
    	'4'='4.C McConnellCMDFO3251'

    	'7'='7.D Thomson  DTDFO3033'

    	'G'='G.L Gara     LGDFO7009'
    	'N'='N.A Taher    ATDFO7046'
    	'Z'='Z.L Gizoski  LGDFO7115'
    	'0'='0.D RobertsonDRDFO7009'
    	'2'='2.                    '

    	'#'='#.S Coates   SCDFO7910'
    	'E'='E.B Martin   BMDFO7903'
    	'H'='H.D Flynn    DFDFO7911'
    	'L'='L.S VeronellySVDFO7902'
    	'X'='X.I Hipfner  IHDFO7915'
		'6'='6.B Sabo     BSDFO7101'
    	'9'='9.E Sahmkow  ESDFO7026'

		'3'='3.J Mondigo    DHD4577'
    	'A'='A.C PidperyhoraDHD3267'
    	'B'='B.M Yantz      DHD3265'
    	'C'='C.J Goodwin    DHD3263'
    	'D'='D.J Fong       DHD3273'
    	'F'='F.H Isaak    	DHD4558'
    	'I'='I.B Mann       DHD3023'
    	'K'='K.E Scrobe     DHD3264'
    	'M'='M.J Kweens     DHD5106'		
    	'P'='P.M Drabyk     DHD3266' 
    	'1'='1.D Davey      DFE3180'
    	'+'='+.D Davey      DCR3180'

    	'5'='5.                    '

    	'8'='8.ONE TIME BUY        '
     	;

  	Value $NBUYS
    	'&' ='&Total'
    	'*' ='*ALL BUYERS'
    	'?' ='TRANSFERS CROSS DOCK'

		'3'='3.J Mondigo    '
    	'A'='A.C Pidperyhora'
    	'B'='B.M Yantz      '
    	'C'='C.J Goodwin    '
    	'D'='D.J Fong       '
    	'F'='F.H Isaak	    '
    	'I'='I.B Mann       '
    	'K'='K.E Scrobe     '
    	'M'='M.J Kweens     '			
    	'P'='P.M Drabyk     '
    	'1'='1.D Davey      '
    	'+'='+.D Davey      '

    	'5'='5.             '

    	'G'='G.L Gara     LG'
    	'N'='N.A Taher    AT'
    	'Z'='Z.L Gizoski  LG'
    	'0'='0.D RobertsonDR'

    	'2'='2.             '
    	

    	'#'='#.S Coates   SC'
    	'H'='H.D Flynn    DF'
    	'L'='L.S VeronellySV'
    	'E'='E.B Martin   BM'
    	'X'='X.I Hipfner  IH'
		'6'='6.B Sabo     BS'
		'9'='9.E Sahmkow  ES'

    	'Q'='Q.J Tomtene  JT'
    	'R'='R.B Lee      BL'
    	'S'='S.K Shwydiuk KS'
    	'T'='T.J ThompsonJTT'
    	'U'='U.J Sawchuk  JS'
		'V'='V.M Vocalan  MV'
    	'4'='4.C McConnellCM'

    	'J'='J.Julius FernJF'
    	'@'='@.B Kuemper  BK'
    	
    	'7'='7.D Thomson  DT'
    	
		'$'='$.S Penner   SP'
    	'W'='W.T WickstromTW'
    	'Y'='Y.S Nayyar   SN'
    	'('='(.S Nayyar   SN'
    	
    	'8'='8.ONE TIME BU--'
     	;

  	Value $RBUY
    	'*'='FX '
    	'?'='FX '

    	'$'='FC '
    	'W'='FC '
    	'Y'='FC '
    	'('='FC '

     	'J'='FF '
    	'@'='FF '

    	'Q'='FF '
    	'R'='FF '
    	'S'='FF '
    	'T'='FF '
    	'U'='FF '
		'V'='FF '
    	'4'='FF '

    	'7'='FF '	

    	'G'='TGF'
    	'N'='TGF'
    	'Z'='TGF'
    	'0'='TGF'
    	'2'='TGP'

    	'#'='TPP'
    	'E'='TPP'
    	'H'='TPP'
    	'L'='TPP'
    	'X'='TPP'
		'6'='TPP'
    	'9'='TPP'
	
    	'3'='FH '
    	'A'='FH '
    	'B'='FH '
    	'C'='FH '
    	'D'='FH '
    	'F'='FH '
    	'I'='FH '
    	'M'='FH '
    	'P'='FH '
    	'K'='FH '
    	'1'='FH '
    	'+'='FH '

    	'5'='FT '

    	'8'='FF '

    	other = 'FX '
        ;

  	Value $DBUY

   		'$'= '00'
   		'W'= '00'
   		'Y'= '00'
   		'('= '20'

   		'J'= '30'
   		'@'= '30'

   		'Q'= '30'
   		'R'= '30'
   		'S'= '30'
   		'T'= '30'
   		'U'= '30'
   		'V'= '30'
   		'4'= '30'

   		'7'= '30'

   		'X'= '60'

   		'2'= '60'

   		'9'= '60'

		'3'= '40'
   		'A'= '40'
   		'B'= '40'
   		'C'= '40'
   		'D'= '40'
   		'F'= '40'
   		'H'= '40'
   		'K'= '40'
   		'L'= '40'
   		'M'= '40'
   		'P'= '40'
   		'1'= '20'
   		'+'= '00'

   		'5'= '50'

    	OTHER = 99
   		;

  	Value $RR
    	'C' = 'Calgary '
    	'E' = 'Edmonton '
    	'R' = 'Regina '
    	'S' = 'Saskatoon '
    	'W' = 'Winnipeg '
    	'V' = 'Vancouver '
    	'T' = 'TGP Edm '
    	'A' = 'Total'
    	'Z' = 'Total'
   		;

  	Value $PP
    	'A' = ' Alta '
    	'B' = ' B.C. '
    	'M' = ' Man  '
    	'O' = ' Ont  '
    	'S' = ' Sask '
    	;

  	Value DD
    	000 = 'Crop'
    	010 = 'Stat'
    	020 = 'Feed'
    	030 = 'Food'
    	040 = 'Home'
    	050 = 'Tire'
    	060 = 'Prod'
    	;

  	Value DEPT
    	000 = 'Crop Supplies'
    	010 = 'Stationery'
    	020 = 'Feed'
    	030 = 'Food'
    	040 = 'Home&Building'
    	050 = 'Tires'
    	060 = 'Produce'
    	;

  	Value DEPTn
    	000 = '000.Crop Supplies'
    	010 = '010.Stationery'
    	020 = '020.Feed'
    	030 = '030.Food'
    	040 = '040.Home & Bldg'
    	050 = '050.Tires'
    	060 = '060.Produce'
    	;

  	Value SS
    	0000='00.Total'
    	0010='10.Herbicides                 '
    	0020='20.Seed  '
    	0030='30.Twine '
    	0040='40.Aeration Equipment'
    	0041='41.Harvest Items'
    	0043='43.Tillage Equipment'
    	0045='45.Hydraulics / Access'
    	0061='61.Sprayer Parts'
    	0065='65.Ag Equipment'
    	0095='95.GIND '
    	0096='96.INV ASSIST '
    	0099='99.Total'

		1001-1099='1001.Stationery'

    	2000='00.Total'
    	2003='03.Biologicals      S-01-01'
    	2006='06.Insecticides     S-02'
    	2007='07.Parasiticeds     S-03'
    	2008='08.Pharmaceutical   S-04'
    	2009='09.Identification   S-05'
    	2010='10.Veterinary Instr S-06'
    	2012='12.Sanitation Prod  S-07-01 '
    	2020='20.Livestock Equipment '
    	2021='21.Specialty Feeds'
    	2030='30.Horse Tack'
    	2040='40.Pet Foods'
    	2099='99.Total'

    	3000='00.Total'
    	3001='01.Canned Fruits                 '
    	3002='02.Juices                        '
    	3003='03.Vegetables                    '
    	3004='04.Ethnic Foods Organic Foods   '
    	3005='05.                              '
    	3006='06.Canned Fish                   '
    	3007='07.Canned Meats-Chili Con Carne  '
    	3008='08.Jams Jellies Marmalades       '
    	3009='09.Pnut Butter/Honey/Syrups    '

    	3010='10.Soups Canned Dry            '
    	3011='11.Cold Cereals                  '
    	3012='12.Hot Cereals Pop Tarts Cereal B'
    	3013='13.Cereal Grains Dried Vegetables'
    	3014='14.Pasta Pizza Related Sauce'
    	3015='15.Ground Coffee,Coffee Filters '
    	3016='16.Instant Coffee                '
    	3017='17.Tea                           '
    	3018='18.Soft Drinks-Waters-Crystals   '
    	3019='19.Chocolate Beverages/Syrup/Coco'

    	3020='20.Milk Milk Substitutes         '
    	3021='21.Baby Foods                    '
    	3022='22.Baking Supplies Misc          '
    	3023='23.Lard Margerine Shortenings    '
    	3024='24.Cooking Oils Sprays         '
    	3025='25.Salt Spices                   '
    	3026='26.Sugar                         '
    	3027='27.Evaporated Glaze Fruits       '
    	3028='28.Flour Pancake Flour         '
    	3029='29.Prepared Flour Mixes          '

    	3030='30.Desserts Pie Filling        '
    	3031='31.Cheese Halvah Pillsbury       '
    	3032='32.Olives Pickles              '
    	3033='33.Salad Dressings Vinegar       '
    	3034='34.Sauces                        '
    	3035='35.Non Foods                     '
    	3036='36.Pet Food Supplies           '
    	3037='37.Household Supplies            '
    	3038='38.Paper Supplies                '
    	3039='39.Preserving Supplies           '

    	3040='40.Soaps Detergents            '
    	3041='41.Cleaners                      '
    	3042='42.Laundry Supplies              '
    	3043='43.Waxes Polishes              '
    	3044='44.Biscuits Snacks             '
    	3045='45.Confectionery                 '
    	3046='46.Cigars Cigarettes Tobaccos    '
    	3047='47.                              '
    	3048='48.Store Supplies                '
    	3049='49.Whse Refrigrated Deli Pro'

    	3050='50.Food Service Grocery        '
    	3051='51.Food Service Frozen         '
    	3053='53.                              '
    	3054='54.Frozen Bulk Meat              '
    	3055='55.Frozen Food '
    	3056='56.One Time Buys'
    	3057='57.Meat Directs       '
    	3058='58.Ice Cream                       '
    	3059='59.Country Morning Harmonie Chick'

    	3060='60.Frozen Food Meat Items          '
    	3061='61.Elmans Foods                    '
    	3062='62.P Coded Items Zacher/Sawchuk    '
    	3063='63.P Coded Items Ziolkowski '
    	3064='64.Dry Grocery Spec Buyer T      '
    	3065='65.Dry Grocery Spec Buyer T      '
    	3066='66.Dairy Planogram Items'
    	3067='67.P Code Items Len Laprairie'
    	3068='68.P Code Popoff/Willems'
    	3069='69.Analgesics Cold Preparations'

    	3070='70.Stomach Prep         '
    	3071='71.Baby Products'
    	3072='72.Skin Care '
    	3073='73.Dental Products '
    	3074='74.Deoderants Antiperspirants'
    	3075='75.Feminine Hygiene '
    	3076='76.First Aid Foot Care '
    	3077='77.Hair Care '
    	3078='78.Mens Toiletries '
    	3079='79.Sundries Vit Meal Replmts'

    	3080='80.Photo Supplies '
    	3081='81.OTC / Pharm Restricted'
    	3082='82.Cosmetics'
    	3083='83.Bakery Direct'
    	3084='84.         '
    	3085='85.Produce '
    	3086='86.Eurosolf              '
    	3099='99.Total'

    	4000='00.Total'
    	4001='01.Tools '
    	4002='02.Lawn Garden '
    	4003='03.Heavy Hardware '
    	4004='04.Builders Hardware '
    	4005='05.Farm Hardware '
    	4006='06.Paints / Paint Supplies'
    	4007='07.Housewares '
    	4008='08.Sport / Leisure '
    	4009='09.Home / Office Products'

    	4010='10.Automotive '
    	4011='11.Electrical '
    	4012='12.Plumbing / Heating '
    	4013='13.Tires / Tubes '
    	4014='14.Electronics '
    	4015='15.Cross Dock Items '
    	4016='16.Small Appliances '
    	4017='17.Power Equipment '
    	4018='18.Caulking / Adhesives'
    	4019='19.Building Prod / Home Furn'

    	4020='20.Deleted  '
    	4021='21.Soft Goods       '
    	4022='22.Toys '
    	4023='23.DROS'
    	4024='24.Pet Supplies'
		4027='27.Building Materials Direct'
    	4040='40.One time buys / Returns'
		4089='89.Stationery'
    	4090='90.Display Accessories'
    	4099='99.Total'

    	5000='00.Total '
    	5013='13.Tires '
    	5099='99.Total '

    	6010='10.pSoups Canned / Dry            '
    	6018='18.pSoft Drinks-Waters-Crystals-  '
    	6025='25.pSalt Spices                   '
    	6034='34.pSauces                        '
    	6044='44.pBiscuits / Snacks             '
    	6048='48.pStore Supplies **             '
    	6050='60.pFood Service - Grocery        '
    	6062='62.pP Coded Items Sawchuk/Zacher    '
    	6063='63.pP Coded Items Peters            '
    	6064='64.pDry Grocery Spec / Buyer T      '
    	6065='65.pDry Grocery Spec / Buyer T      '
    	6066='66.pDairy Planogram Items'
    	6068='68.pOne Time Buy'
    	6069='69.pAnalgesics / Cold Preparations'
    	6085='85.pProduce'
    	6099='99.Total'

    	9999='All Sections '
    	;

  	Value BUYER
    	0001-0009 = 'Y'
    	0010      = 'W'
    	0011      = 'Y'
    	0012      = 'W'
    	0013-0019 = 'Y'
    	0020      = '$'
    	0021      = 'Y'
    	0022      = 'Y'
    	0023-0029 = 'Y'
    	0030      = '$'
    	0040-0099 = 'Y'

    	1001-1099 = '3'

    	2001-2099 = '1'

    	3001      = 'S'
    	3002      = 'U'
    	3003      = 'S'
    	3004      = 'J'
    	3005      = 'J'
    	3006      = 'S'
    	3007      = 'S'
    	3008      = 'U'
    	3009      = 'U'
    	3010      = 'U'

    	3011      = 'S'
    	3012      = 'S'
    	3013      = 'U'
    	3014      = 'U'
    	3015      = 'U'
    	3016      = 'U'
    	3017      = 'U'
    	3018      = 'J'
    	3019      = 'U'
    	3020      = 'U'

    	3021      = 'S'
    	3022      = '4'
    	3023      = 'U'
    	3024      = 'U'
    	3025      = 'U'
    	3026      = '4'
    	3027      = '4'
    	3028      = '4'
    	3029      = '4'
    	3030      = '4'

    	3031      = 'U'
    	3032      = 'U'
    	3033      = 'U'
    	3034      = 'S'
    	3035      = 'U'
    	3036      = 'S'
    	3037      = 'S'
    	3038      = 'S'
    	3039      = '4'
    	3040      = 'S'

    	3041      = 'S'
    	3042      = 'S'
    	3043      = 'S'
    	3044      = '4'
    	3045      = '4'
    	3046      = '4'
    	3047      = '4'
    	3048      = '4'
    	3049      = '7'
    	3050      = '4'

    	3051      = '4'
    	3052      = '8'
    	3053      = '8'
    	3054      = '7'
    	3055      = '4'
    	3056      = '4'
    	3057      = '7'
    	3058      = '4'
    	3059      = '7'
    	3060      = '4'

    	3061      = 'T'
    	3062      = 'U'
    	3063      = 'U'
    	3064      = 'T'
    	3065      = 'R'
    	3066      = '8'
    	3067      = '8'
    	3068      = 'U'
    	3069      = 'R'
    	3070      = 'R'

    	3071      = 'S'
    	3072      = 'R'
    	3073      = 'R'
    	3074      = 'R'
    	3075      = 'S'
    	3076      = 'R'
    	3077      = 'R'
    	3078      = 'R'
    	3079      = 'R'
    	3080      = '4'

    	3081      = 'R'
    	3082      = 'R'
    	3083      = '8'
    	3084      = '8'
    	3085      = 'U'
    	3086-3099 = '8'

    	4001      = 'B'
    	4002      = 'C'
    	4003      = 'I'
    	4004      = 'I'
    	4005      = 'B'
    	4006      = 'K'
    	4007      = 'D'
    	4008      = 'F'
    	4009      = 'B'		
    	4010      = 'P'

    	4011      = 'K'
    	4012      = 'P'
    	4013      = '5'
    	4014      = 'F'
    	4015      = 'I'
    	4016      = 'D'
    	4017      = 'C'
    	4018      = 'K'
    	4019      = 'A'
    	4020      = 'I'

    	4021      = 'C'		
    	4022      = 'F'
       	4024      = 'D'
    	4025-4026 = 'I'
		4027	  = 'A'
		4028-4039 = 'I'
    	4040      = 'P'
    	4041-4088 = 'I'
		4089	  = '3'
    	4090      = 'M'		
    	4091-4099 = 'I'

    	5013      = '5'

    	6001-6099 = 'H'

    	OTHER     = '8'
    	;

  	Picture THOU
    	LOW--0      ='0009.0' (MULT=.01 PREFIX='-' )
    	0  -1000000 ='0009.0' (MULT=.01)
    	1000000-HIGH='000009' (MULT=.001) 
		;

  	Picture THOUB
    	LOW--0      ='0009.0' 	(MULT=.01 PREFIX='-' )
    	0  -10000000='00009.0'	(MULT=.01)
    	10000000-HIGH='000009' 	(MULT=.001)
    	;


  	Value $SST
    	'A'= 'A-Active'
    	'E'= 'E-Supp Out'
    	'G'= 'G-Seas Delete'
    	'J'= 'J-Out New Pack'
    	'P'= 'P-Promotion'
    	'M'= 'M-Delete'
    	'R'= 'R-Delete Deplete'
    	'S'= 'S-Sop'
    	'9'= 'Total'
    	;

  	Value TRMTYPE
    	0='0.NONE'
    	1='1.DOLS'
    	2='2.CASE'
    	3='3.PNDS'
    	4='4.UNTS'
    	5='5.GALS'
    	6='6.CBFT'
    	;

    * FORMATS FOR RAINBOW ITEMS ;
  	Value $MESSAG
    	'C'= 'CLEAR OUT AS INSTRUCTED'
    	'D'= 'ITEM ON DELETE LIST'
    	'L'= 'LONG BUY'
    	'P'= 'PROMOTE'
    	'S'= 'STOCK ROTATION'
    	'W'= 'WRITE DOWN AND CLEAR'
    	'X'= 'ERROR ITEM IS NOT RAINBOW'
    	'Z'= 'DO NOTHING'
    	'_'= '_________________________'
    	;

  	Value $N_OUT
    	Y		='NOUT'
    	OTHER	='    '
    	;

* OR_MTH LINKS TO THE LUBE OIL SCHEDULING DATASET ORO100A# ;

  	Value OR_MTH
    	00 = 'STOCK'
    	01 = 'Nov'
    	02 = 'Dec'
    	03 = 'Jan'
    	04 = 'Feb'
    	05 = 'Mar'
    	06 = 'Apr'
    	07 = 'May'
    	08 = 'Jun'
    	09 = 'Jul'
   		10 = 'Aug'
   		11 = 'Sep'
   		12 = 'Oct'
    	;


  	Value OR_COLOR
     	00 = '0.CLEAR'
     	01 = '1.BLUE'
     	02 = '2.GREEN'
     	03 = '3.AQUA'
     	04 = '4.RED'
     	05 = '5.PURPLE'
     	06 = '6.BROWN'
     	07 = '7.BLACK'
     	08 = '8.BLACK'
     	09 = '1.BLUE'
    	10 = '2.GREEN'
    	11 = '3.AQUA'
    	12 = '4.RED'
    	13 = '5.PURPLE'
    	14 = '6.BROWN'
    	15 = '8.BLACK'
     	;

  	Value OR_SHAPE
    	1 = '1.BOX'
    	2 = '2.CAN'
    	3 = '3.JAR'
    	4 = '4.BOT'
    	5 = '5.BAG'
    	6 = '6.ENV'
    	7 = '7.ASP'
    	;

  	Value $OR_RSHP
    	'   ' = '1'
    	'BOX' = '1'
    	'CAN' = '2'
    	'JAR' = '3'
    	'BOT' = '4'
    	'BAG' = '5'
    	'ENV' = '6'
    	'ASP' = '7'
     	;

  	Value OR_CAP
    	1 = '1.NO CAP'
    	2 = '2.MIN CAP'
    	3 = '3.MED CAP'
    	4 = '4.MAX CAP'
    	5 = '5.CAN STACK'
    	;

  	Value $RE_CAP
    	'NO ' = 'NO CAP'
    	'MIN' = 'MIN CAP'
    	'MED' = 'MED CAP'
    	'MAX' = 'MAX CAP'
    	'CAN' = 'CAN STACK'
     	;

  	Value $OR_HANG
    	'Y' = 'ALWAYS'
    	'O' = 'OPTIONAL'
    	'N' = 'NEVER'
     	;

  	Value $OR_MRCH
    	'U' = 'UNITS'
    	'T' = 'TRAYS'
    	'C' = 'CASES'
     	;

  	Value OR_ORIEN
     	1 = '1.NO'
     	2 = '2.YES'
     	;

  	Value OR_LEGAL
     	1 = 'ILLEGAL'
     	2 = 'LEGAL'
     	;

  	Value OR_FILL
     	01 = '01.EMPTY'
     	02 = '02.LEFT SLANT'
     	03 = '03.RIGHT SLANT'
     	04 = '04.CROSS HATCH'
     	05 = '05.EMPTY'
     	06 = '06.DOT'
     	07 = '07.CROSS'
     	08 = '08.DIAMOND'
     	09 = '09.RIGHT HASH'
    	10 = '10.LEFT HASH'
    	11 = '11.TRIANGLE'
    	12 = '12.HORZ WAVE'
    	13 = '13.VERT WAVE'
     	;

  	Value OR_VEND
    	0001 = 'COOP LABEL'
    	;

	
	

  VALUE $DOC_CAT
    'CASM' = 'CASM.SHELF MGMT'
    'DILP' = 'DILP.DISTRIBUTION LP'
    'FEED' = 'FEED.FEED LP'
    'GBAR' = 'GBAR.GAS BAR'
    'GRPH' = 'GRPH.PET SLS GRAPHS'
    'IM'   = 'IM.INVENTORY MGMT'
    'LUBE' = 'LUBE.LUBE OIL'
    'OR'   = 'OR.OR USE'
    'NEI'  = 'NEI.FINANCIAL MODEL'
    'PET'  = 'PET.PETROLEUM MERCH'
    'PC'   = 'PC.PRICE CHECK'
    'RETB' = 'RETB.RETAIL BUDGET'
    'RFLP' = 'RFLP.REFINERY LP'
    'RTLP' = 'RTLP.RETAIL PGMS'
  	;
	

  	Value $AB
    	'A' = 'Act'
    	'B' = 'Bud'
    	;

  	Value $Prom_Cd
    	'AA' = 'AA.Food Super Saver'
    	'BB' = 'BB.Food Cash Saver'
    	'CC' = 'CC.Food Regular Advertised'

    	'DA' = 'DA.Direct to store Delivered SS'
    	'DB' = 'DB.Direct to Store Delivered CS'
    	'DC' = 'DC.Direct to Store Delivered '

    	'EA' = 'EA.Frozen Food Super Saver'
    	'FB' = 'FB.Frozen Food Cash Saver'
    	'GC' = 'GC.Frozen Food Regula Advertised'

    	'HA' = 'HA.Fresh Dairy Super Saver'
    	'HB' = 'HB.Fresh Dairy Cash Saver'
    	'HC' = 'HC.Fresh Dairy Features'

    	'HD' = 'HD.Fresh Dairy Super Saver Cal'
    	'HE' = 'HE.Fresh Dairy Cash Saver Cal'
    	'HF' = 'HF.Fresh Dairy Features Cal'

    	'IA' = 'IA.HBA Super Saver'
    	'JB' = 'JB.HDA Cash Saver'
    	'KC' = 'KC.HBA Regular'

    	'MA' = 'MA.Drug Super Saver'
    	'NB' = 'NB.Drug Cash Saver'
    	'OC' = 'OC.Drug Regular'

    	'A' = 'A.SS/SIZZLER'
    	'B' = 'B.CS/SUPER BUY'
    	'C' = 'C.REG ADV/VALUE BUYS'
    	'D' = 'D.PKG MEATS INSTORES'
    	'E' = 'E.FROZ FOOD SS'
    	'F' = 'F.FROZ FOOD CS'
    	'G' = 'G.FROZ FOOD REG ADV'
    	'H' = 'H.DELI DEPT INSTORES'
    	'I' = 'I.HBA SS'
    	'J' = 'J.HBA CS'
    	'K' = 'K.HBA REG ADV'
    	'L' = 'L.HBA NON-ADV '
    	'M' = 'M.DRUG SS'
    	'N' = 'N.DRUG CS'
    	'O' = 'O.DRUG REG ADV '
    	'P' = 'P.DELI SS'

    	'Q ' = 'Q .Food Special Packs'
    	'R ' = 'R .HBA Special Packs'
    	'S ' = 'S .Country Morning Items'
    	'T ' = 'T .Bonus Items'
    	'U ' = 'U .Bonus Buys Feature'

    	'V'  = 'V .Fresh Bakery Reg Adv'
    	'VA' = 'VA.Fresh Bakery Super Saver'
    	'VB' = 'VB.Fresh Bakery Cash Saver'
    	'VC' = 'VC.Fresh Bakery Regular Advertised'


    	'W ' = 'W .Meat Super Saver'
    	'X ' = 'X .Fresh Meat / Fish'
    	'Y ' = 'Y .Deli Dept Advertised'
    	'Z ' = 'Z .Packaged Meat Advertised'
    	;

  	Value $DEPTBUY
    	'00$' = 'DCR3185'
    	'00W' = 'DCR3035'
    	'00Y' = 'DCR4775'
    	'20(' = 'DFE4775'

    	'30Q' = 'DFO3967'
    	'30R' = 'DFO3254'
    	'30S' = 'DFO3252'
    	'30T' = 'DFO3250'
    	'30U' = 'DFO3253'
    	'30V' = 'DFO3366'
    	'304' = 'DFO3251'

    	'30G' = 'DFO7026'
    	'30L' = 'DFO7902'
    	'30N' = 'DFO7005'
    	'30X' = 'DFO7128'
    	'30Z' = 'DFO7024'
    	'300' = 'DFO7046'
    	'302' = 'DFO7003'
    	'306' = 'DFO7006'

    	'307' = 'DFO3033'

    	'309' = 'DFO7903'
    	'30#' = 'DFO7128'
    	'30E' = 'DFO7128'
    	'30O' = 'DFO7905'

		'403' = 'DHD4577'
    	'40A' = 'DHD4577'    
    	'40B' = 'DHD4577'    
    	'40C' = 'DHD3270'    
    	'40D' = 'DHD3270'    
    	'40F' = 'DHD4577'   
    	'40I' = 'DHD4577'    
    	'40K' = 'DHD3270'    
    	'40M' = 'DHD4577'    
    	'40P' = 'DHD3270' 
    	'201' = 'DFE3180'
    	'20+' = 'DCR3180'

    	'405' = 'DCR3002'
     	OTHER  = 'DOR3087'
        ;

  	Value $DRPBUY
  		'3' ='Jellah Mondigo     '
    	'A' ='Brian Schindel     '
    	'B' ='Marife Carlos      '
    	'C' ='Aleya Ruiz Lumictin'
    	'D' ='Brian Schindel     '
    	'F' ='Aleya Ruiz Lumictin'
    	'I' ='Marife Carlos      '
    	'K' ='Jellah Mondigo     '
    	'M' ='Marife Carlos      '
    	'P' ='Jellah Mondigo     '
    	'1' ='Brian Schindel     '
    	'+' ='Brian Schindel     '

    	'Q' ='Joel Tomtene   	 '
    	'R' ='Bobby Lee          '
    	'S' ='Kristan Shwydiuk   '
    	'T' ='Jessica Thompson-Ta'
    	'U' ='Jerry Sawchuk      '
    	'V' ='Monette Vocalan    '
    	'4' ='Connie McConnell   '

    	'7' ='Tim VanPinxteren   '

    	'Y' ='Shairy Nayyar      '
    	'$' ='Jellah Mondigo     '
		'W' ='Tracy Wickstrom    '

    	;
Run ;

Proc Format Library=LIBRARY ;
  	Value DptNo
    	000 = '000.Crop'
    	010 = '010.Stat'
    	020 = '020.Feed'
    	030 = '030.Food'
    	040 = '040.Genm'
    	050 = '050.Tire'
    	060 = '060.Prod'
    	;

  	Value $RgnId
    	'C' = 'Cal'
    	'E' = 'Edm'
    	'R' = 'Reg'
    	'S' = 'Stn'
    	'W' = 'Wpg'
    	;

  	Value $BuyerCd
    	'$' = '$.Jellah Mondigo    -JM'
    	'W' = 'W.Tracy Wickstrom   -TW'
    	'Y' = 'Y.Shairy Nayyar     -SN'
    	'(' = '(.Shairy Nayyar     -SN'

    	'J' = 'J.Julius Fernandes  -JF'
    	'@' = '@.Lauren Rivey      -LR'

    	'Q' = 'Q.Joel Tomtene      -JT'
    	'R' = 'R.Bobby Lee	       -BL'
    	'S' = 'S.Kristan Shwydiuk  -KS'
    	'T' = 'T.Jessica Thompson -JTT'
    	'U' = 'U.Jerry Sawchuk     -JS'
		'V' = 'V.Monette Vocalan   -MV'
    	'4' = '4.Connie McConnell  -CM'
    
    	'7' = '7.Tim VanPinxteren  -TV'

    	'G' = 'G.Lavo Gara         -LG'
    	'N' = 'N.Aatif Taher       -AT'
    	'Z' = 'Z.Lori Gizoski      -LG'
    	'2' = '2.                     '

    	'#' = '#.Shawn Coates      -SC'
    	'E' = 'E.Brenda Martin     -BM'
    	'H' = 'H.Dave Flynn        -DF'
    	'L' = 'L.Sherryann Veronell-SV'
    	'X' = 'X.Irene Hipfner     -IH'
		'6' = '6.Bonny Sabo        -BS'
    	'9' = '9.Eleana Sahmkow    -ES'

    	'8' = '8.One Time Buy      -XX'

		'3' = '3.Jellah Mondigo       '
    	'A' = 'A.Colin Pidperyhora    '
    	'B' = 'B.Mike Yantz           '
    	'C' = 'C.Jason Goodwin        '
    	'D' = 'D.Johnnie Fong         '
    	'F' = 'F.Hudson Isaak         '
    	'I' = 'I.Byron Mann           '
    	'K' = 'K.E Scrobe             '
    	'M' = 'M.Jeremy Kweens        '		
    	'P' = 'P.Mikael Drabyk        '
    	'1' = '1.Dawn Davey           '
    	'+' = '+.Dawn Davey           '
    	
    	'5' = '5.                     '
    	;

  	Value $ReplCd
    	'$' = '$.Jellah Mondigo      '
    	'W' = 'W.Tracy Wickstrom     '
    	'Y' = 'Y.Deonne Hrynyk       '
    	'(' = '(.Deonne Hrynyk       '

    	'G' = 'G.Lavo Gara           '
    	'N' = 'N.Aatif Taher         '
    	'Z' = 'Z.Lori Gizoski        '
    	'2' = '2.                    '

    	'#' = '#.Shawn Coates		 '
    	'H' = 'H.Dave Flynn          '
    	'L' = 'L.SherryAnn Veronelly '
    	'E' = 'E.Brenda Martin       '
    	'X' = 'X.Irene Hipfner       '
		'6' = '6.Bonny Sabo          '
		'9' = '9.Eleana Sahmkow      '

    	'J' = 'J.Julius Fernandes    '
    	'@' = '@.Lauren Rivey        '

    	'Q' = 'Q.Joel Tomtene   	 '
    	'R' = 'R.Bobby Lee           '
    	'S' = 'S.Kristan Shwydiuk    '
    	'T' = 'T.Jessica Thompson-Tay'
    	'U' = 'U.Jerry Sawchuk       '
    	'V' = 'V.Monette Vocalan  	 '
    	'4' = '4.Connie McConnell    '
    	'8' = '8.One Time Buy        '

    	'7' = '7.Tim Vanpinxteren    '

		'3' = '3.Jellah Mondigo      '
    	'A' = 'A.Brian Schindel   -CC'
    	'B' = 'B.Marife Carlos    -MY'
    	'C' = 'C.Aleya Lumictin   -JG'
    	'D' = 'D.Brian Schindel   -JF'
    	'F' = 'F.Aleya Lumictin   -HI'
    	'I' = 'I.Marife Carlos    -BM'
    	'K' = 'K.Jellah Mondigo   -ES'
    	'M' = 'M.Marife Carlos    -JK'
    	'P' = 'P.Jellah Mondigo   -MD'
    	'1' = '1.Brian Schindel   -DD'
    	'+' = '+.Brian Schindel   -DD'

    	'5' = '5.                    '
    	;

  	Value $Company
    	'$' = 'FCL'
    	'W' = 'FCL'
    	'Y' = 'FCL'
		'(' = 'FCL' 	

    	'Q' = 'FCL'
    	'R' = 'FCL'
    	'S' = 'FCL'
    	'T' = 'FCL'
    	'U' = 'FCL'
		'V' = 'FCL'
    	'4' = 'FCL'

    	'7' = 'FCM'

    	'8' = 'FCL'

    	'G' = 'TGP'
    	'N' = 'TGP'
    	'Z' = 'TGP'
    	'0' = 'TGP'
    	'2' = 'TGP'

    	'E' = 'TPP'
    	'H' = 'TPP'
    	'L' = 'TPP'
    	'X' = 'TPP'
		'6' = 'TPP'
    	'9' = 'TPP'
    	'#' = 'TPP'

    	'J' = 'FCD'
    	'@' = 'FCD'

		'3' = 'FCL'
    	'A' = 'FCL'
    	'B' = 'FCL'
    	'C' = 'FCL'
    	'D' = 'FCL'
    	'F' = 'FCL'
    	'I' = 'FCL'
    	'K' = 'FCL'
    	'M' = 'FCL'
    	'P' = 'FCL'
    	'1' = 'FCL'
    	'+' = 'FCL'

    	'5' = 'FCT'

    	OTHER = 'FCL'
    	;

  	Value $CyclRev
    	'U1' = 'U1m....'
    	'U2' = 'U2.t...'
    	'U3' = 'U3..w..'
    	'U4' = 'U4...t.'
    	'U5' = 'U5....f'

    	'R1' = 'R1m..t.'
    	'R2' = 'R2.t.t.'
    	'R3' = 'R3m.w.f'
    	'R4' = 'R4m.w..'
    	'R5' = 'R5mtwtf'

    	'Y1' = 'Y1.....'
    	'Y3' = 'Y3..w.f'
    	'Y4' = 'Y4.t.t.'
    	'Y5' = 'Y5.t..f'

    	'X1' = 'X1?????'
    	;

  	Value UnitShp
    	0 = 'No Terms'
    	1 = '1.Dollars'
    	2 = '2.Cases'
    	3 = '3.Pounds'
    	4 = '4.Units'
    	5 = '5.Gallons'
    	6 = '6.Cubic Ft'
    	;

  	Value $OrdrCd
   		'I' = 'Indep'
   		'J' = 'Joint'
    	;

  	Value PricePat
    	Low   -< -11.5 = 'D8'
    	-11.5 -< -10.5 = 'D7'
    	-10.5 -< -09.5 = 'D6'
    	-09.5 -< -08.5 = 'D5'
    	-08.5 -< -07.5 = 'D4'
    	-07.5 -< -06.5 = 'D3'
    	-06.5 -< -05.5 = 'D2'
    	-05.5 -< -04.5 = 'D1'
    	-04.5 -< -03.5 = 'D1'
    	-03.5 -< -02.5 = 'D1'
    	-02.5 -< -01.5 = 'D0'
    	-01.5 -<  00.0 = 'D0'
     	00.0 -   00.0 = '--'
     	00.0 -<  01.5 = 'U0'
     	01.5 -<  02.5 = 'U0'
     	02.5 -<  03.5 = 'U1'
     	03.5 -<  04.5 = 'U1'
     	04.5 -<  05.5 = 'U1'
     	05.5 -<  06.5 = 'U2'
     	06.5 -<  07.5 = 'U3'
     	07.5 -<  08.5 = 'U4'
     	08.5 -<  09.5 = 'U5'
     	09.5 -<  10.5 = 'U6'
     	10.5 -<  11.5 = 'U7'
     	11.5 -   High = 'U8'
     	;
Run ;


proc fmtc2itm catalog=(library.formats) encoding=latin1
print locale itemstore="/sasdata/supplychain/sastest/orm000/formatItemStore";
run;

cas casauto addfmtlib fmtlibname="Orm000"    
   path="/sasdata/supplychain/sastest/orm000/formatItemStore"
   replacefmtlib;
   
cas casauto promotefmtlib fmtlibname=Orm000 replace;
   
cas casauto listformat fmtlibname="Orm000"     
   members;
   
   
Libname LIBRARY Clear ;

Data _null_ ;
Run ;


