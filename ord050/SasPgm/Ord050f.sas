/*
	This is a program I don't want to commit yet :(
	2020-JUL-16 - MC: Created this SAS program for all formats and variable cell positionings for the output Excel sheet.
	I will cause a merge conflict!
*/

/* Put Statement for creating Total rows based on Plan Code flags */
%Macro CreatePlanFormula(VarName) ;
	Cats("=SUMIF($A$3:$A$", Counter, ',"Y",', Put("&VarName.", $CellPos.), '3:', Put("&VarName.", $CellPos.) , Counter, ")") ;
%Mend ;


/* Put Statement for creating Total rows based on Divider/Shelf Code flags */
%Macro CreateSWFormula(FlagCellLoc, VarName) ;
	Cats("=SUMIF($&FlagCellLoc.$3:$&FlagCellLoc.$", Counter, ',"Y",', Put("&VarName.", $CellPos.), '3:', Put("&VarName.", $CellPos.) , Counter, ")") ;
%Mend ;

/* Put Statement for creating totals by plan group in Summary Tab */
%Macro CreateSummarySumFormula(VarName) ;
	Cats('=', SubPlanGroup, '!', Put("&VarName.", $CellPos.), Counter) ;
%Mend ;

/* Put Statement for creating Totals row in the Summary Tab */
%Macro CreateSummaryTotalFormula(VarName) ;
	Cats("=SUM(", Put("&VarName.", $PlanTotalSummary.), Counter, ")") ;
%Mend ;

/* Proc Report Labels */
%Macro OutLabels ;
			Shelf16 = '16"'
			Shelf18 = '18"'
			Shelf20 = '20"'
			Shelf22 = '22"'
			Shelf24 = '24"'
			Shelf27 = '27"'

			ShelfDiv16 = '16"'
			ShelfDiv18 = '18"'
			ShelfDiv20 = '20"'
			ShelfDiv22 = '22"'
			ShelfDiv24 = '24"'
			ShelfDiv27 = '27"'

			WireBasket16 = '16"'
			WireBasket18 = '18"'
			WireBasket20 = '20"'
			WireBasket22 = '22"'
			WireBasket24 = '24"'

			WireDiv16 = '16"'
			WireDiv18 = '18"'
			WireDiv20 = '20"'
			WireDiv22 = '22"'
			WireDiv24 = '24"'

			Aerosol16 = '16"'
			Aerosol20 = '20"'

			Caulking16   = '16"'
			Caulking20   = '20"'

			VendorFixture  = "Vendor Fixture"
			ExtenderPanel  = "Extender Panel"
			ClipStrip      = "Clip Strip"
			WireGrid       = "Wire Grid"

			RockMaplePanel = "Maple Panel"
			RockMapleShelf = "Maple Shelf"
			BeamShelf      = "Beam Shelf"
			WireBasket     = "Wire Baskets"    
			WireShelf      = "Wire Shelf"
			SolidShelf     = "Solid Shelf"
			Bar            = "Bar"
			PegBoard       = "Pegboard"
			SlatWall       = "Slat Wall"
			GravityFeed    = "Gravity Feed"
			ShelfBase      = "Shelf Base"

			Fences   = "Fences"

			Hook3  = '3"'
		    Hook4  = '4"'
		    Hook6  = '6"'
		    Hook8  = '8"'
		    Hook9  = '9"'
		    Hook12 = '12"'

		    PegHook2 = '2"'
		    PegHook3 = '3"'
		    PegHook4 = '4"'
		    PegHook6 = '6"'
		    PegHook9 = '9"'
		    PegHook12 = '12"'
		    PegHook15 = '15"'
		    PegHook16 = '16"'
		    PegHook18 = '18"'

			PegHookHD12 = '12"'
			PegHookHD16 = '16"'

		    PegHook = "Peg Hook"

			BH16 = '16" Broom Hooks'

		    Bdsma   = "Abrasive Hook"
		    Hoe = "Hoe"
		    SWG = '6" Wire Grid Scanner Hook'
		    Cm  = "Cake Mate"
		    Hangers  = "Hangers"
		    DHandle  = "D-Handle"
		    Ch       = "Crossbar Hook"
		    Peg12Scb = '12" Crossbar Hooks'
		    Bdmda    = '5"-7" Abrasive Hook'
		    Pd       = "Pegboard Divider"

		    Shovel = "Shovel"
		    Hammer = "Hammer"

		    Clip   = "Clipstrip"
		    Rack   = "Total SKU's for Special Rack"
		    Jh     = "J-Hook for Rope"
		    Dh10s  = '10" Heavy Duty Double Hook'

			UnknownPeg = "Not Listed in Peg Library"
%Mend ;

/* Proc Report Columns */
%Macro ReportColOrder ;
	("Shelves" 			 Shelf16 	Shelf18    Shelf20 	  Shelf22 	 Shelf24 	Shelf27)
	("Shelf Dividers" ShelfDiv16 ShelfDiv18 ShelfDiv20 ShelfDiv22 ShelfDiv24 ShelfDiv27)
	
	("Wire Baskets"  WireBasket16 WireBasket18 WireBasket20 WireBasket22 WireBasket24)
	("Wire Dividers" 	WireDiv16 	 WireDiv18    WireDiv20	   WireDiv22 	WireDiv24)
	
	("Aerosol"  Aerosol16   Aerosol20)
	("Caulking" Caulking16 Caulking20)

	("Peg Hooks" PegHook PegHook2 PegHook3 PegHook4 PegHook6 PegHook9 PegHook12 PegHook15 PegHook16 PegHook18)
	
	("Scanner Hooks" Hook3 Hook4 Hook6 Hook8 Hook9 Hook12)
	("Heavy Duty Hooks"  PegHookHD12 PegHookHD16) 
	
	("PEGS" BH16 Bdsma Hoe SWG Cm Hangers DHandle Ch Peg12Scb Bdmda Pd Shovel Hammer Clip Rack Jh Dh10s UnknownPeg)
				
	Fences
	BeamShelf RockMapleShelf SolidShelf WireShelf

	ExtenderPanel RockMaplePanel

	WireBasket ClipStrip WireGrid    
  
	Bar PegBoard SlatWall GravityFeed ShelfBase    
 
	VendorFixture
%Mend ;


Proc Format ;
	Value Bool
		0 = " "
		1 = "Yes"
		;

	Value FixtureType
		0 = "Shelf"
		1 = "Chest"
		2 = "Bin"
		3 = "Polygonal Shelf"
		4 = "Rod"
		5 = "Lateral Rod"
		6 = "Bar"
		7 = "Pegboard"
		8 = "Multi-row Pegboard"
		9 = "Curved Rod"
		10 = "Obstruction"
		11 = "Sign"
		12 = "Gravity Feed"
		13 = "Subplanogram Space"
		;

	Picture ddmmmyy (Default=10)
    	Low - High = '%D-%b-%y' (DataType=date)
  		;
	


	/* Format that determines cell position for each variable in the output Excel Sheet For Planogram Group Section and Summary By Plans */
	Value $CellPos
		"ActRCC"          = "F"
		"ActSRP"          = "G"

		"Margin"		  = "H"

		"Turns"           = "I"
		"Sales"           = "J"
  
		"Shelf16"         = "AA"
		"Shelf18"         = "AB"
		"Shelf20"         = "AC"
		"Shelf22"         = "AD"
        "Shelf24"         = "AE"
		"Shelf27"         = "AF"
		"ShelfDiv16"      = "AG"
		"ShelfDiv18"      = "AH"
		"ShelfDiv20"      = "AI"
		"ShelfDiv22"      = "AJ"
        "ShelfDiv24"      = "AK"
		"ShelfDiv27"      = "AL"
		"WireBasket16"    = "AM"
		"WireBasket18"    = "AN"
		"WireBasket20"    = "AO"
		"WireBasket22"    = "AP"
        "WireBasket24"    = "AQ"
		"WireDiv16"       = "AR"
		"WireDiv18"       = "AS"
		"WireDiv20"       = "AT"
		"WireDiv22"       = "AU"
        "WireDiv24"       = "AV"
        "Aerosol16"       = "AW"
        "Aerosol20"       = "AX"
        "Caulking16"      = "AY"
        "Caulking20"      = "AZ"
		"PegHook"  	      = "BA"
		"PegHook2" 	      = "BB"
		"PegHook3" 	      = "BC"
		"PegHook4" 	      = "BD"
		"PegHook6" 	      = "BE"
		"PegHook9" 	      = "BF"
		"PegHook12"       = "BG"
		"PegHook15"       = "BH"
		"PegHook16"       = "BI"
		"PegHook18"       = "BJ"
		"Hook3"           = "BK"
		"Hook4"           = "BL"
		"Hook6"           = "BM"
		"Hook8"           = "BN"
		"Hook9"           = "BO"
		"Hook12"          = "BP"
		"PegHookHD12"     = "BQ"
		"PegHookHD16"     = "BR"
		"BH16"            = "BS"
		"Bdsma"           = "BT"
		"Hoe"	          = "BU"
		"SWG"	          = "BV"
		"Cm" 	          = "BW"
		"Hangers"         = "BX"
		"DHandle"         = "BY"
		"Ch"              = "BZ"
		"Peg12Scb"        = "CA"
		"Bdmda"           = "CB"
		"Pd"   	          = "CC"
		"Shovel"          = "CD"
		"Hammer"          = "CE"
		"Clip"            = "CF"
		"Rack"            = "CG"
		"Jh"              = "CH"
		"Dh10s"           = "CI"
		"UnknownPeg"      = "CJ"
		"Fences"		  = "CK"
		"BeamShelf"		  = "CL"
		"RockMapleShelf"  = "CM"
		"SolidShelf"	  = "CN"
		"WireShelf"		  = "CO"
		"ExtenderPanel"	  = "CP"
		"RockMaplePanel"  = "CQ"
		"WireBasket"	  = "CR"
		"ClipStrip"		  = "CS"
		"WireGrid"		  = "CT"
		"Bar"			  = "CU"
		"PegBoard"		  = "CV"
		"SlatWall"		  = "CW"
		"GravityFeed"	  = "CX"
		"ShelfBase"		  = "CY"
		"VendorFixture"	  = "CZ"
		;

	/* Summary Section Column Order By Variable - Totals Row*/
	Value $PlanTotalSummary 
		"ActRCC"          = "B3:B"
		"ActSRP"          = "C3:C"
		"Margin"		  = "D3:D"
		"Turns"           = "E3:E"
		"Sales"           = "F3:F"
		"Shelf16" 	 	  = "G3:G"
		"Shelf18" 	 	  = "H3:H"
		"Shelf20" 	 	  = "I3:I"
		"Shelf22" 	 	  = "J3:J"
		"Shelf24" 	 	  = "K3:K"
		"Shelf27" 	 	  = "L3:L"
		"ShelfDiv16" 	  = "M3:M"
		"ShelfDiv18" 	  = "N3:N"
		"ShelfDiv20" 	  = "O3:O"
		"ShelfDiv22" 	  = "P3:P"
		"ShelfDiv24" 	  = "Q3:Q"
		"ShelfDiv27" 	  = "R3:R"
		"WireBasket16" 	  = "S3:S"
		"WireBasket18" 	  = "T3:T"
		"WireBasket20" 	  = "U3:U"
		"WireBasket22" 	  = "V3:V"
		"WireBasket24" 	  = "W3:W"
		"WireDiv16" 	  = "X3:X"
		"WireDiv18" 	  = "Y3:Y"
		"WireDiv20" 	  = "Z3:Z"
		"WireDiv22" 	  = "AA3:AA"
		"WireDiv24" 	  = "AB3:AB"
		"Aerosol16"		  = "AC3:AC"
		"Aerosol20"		  = "AD3:AD"
		"Caulking16"	  = "AE3:AE"
		"Caulking20"	  = "AF3:AF"
		"PegHook"  		  = "AG3:AG"
		"PegHook2" 		  = "AH3:AH"
		"PegHook3" 		  = "AI3:AI"
		"PegHook4" 		  = "AJ3:AJ"
		"PegHook6" 		  = "AK3:AK"
		"PegHook9" 		  = "AL3:AL"
		"PegHook12" 	  = "AM3:AM"
		"PegHook15" 	  = "AN3:AN"
		"PegHook16" 	  = "AO3:AO"
		"PegHook18" 	  = "AP3:AP"
		"Hook3"  	      = "AQ3:AQ"
		"Hook4"  	      = "AR3:AR"
		"Hook6"  	      = "AS3:AS"
		"Hook8"  	      = "AT3:AT"
		"Hook9"  	      = "AU3:AU"
		"Hook12" 	      = "AV3:AV"
		"PegHookHD12"	  = "AW3:AW"
		"PegHookHD16"	  = "AX3:AX"
		"BH16" 		      = "AY3:AY"
		"Bdsma"      	  = "AZ3:AZ"
		"Hoe"	     	  = "BA3:BA"
		"SWG"	     	  = "BB3:BB"
		"Cm" 	     	  = "BC3:BC"
		"Hangers"    	  = "BD3:BD"
		"DHandle"    	  = "BE3:BE"
		"Ch"         	  = "BF3:BF"
		"Peg12Scb"   	  = "BG3:BG"
		"Bdmda"      	  = "BH3:BH"
		"Pd"   	     	  = "BI3:BI"
		"Shovel"     	  = "BJ3:BJ"
		"Hammer"     	  = "BK3:BK"
		"Clip"       	  = "BL3:BL"
		"Rack"       	  = "BM3:BM"
		"Jh"         	  = "BN3:BN"
		"Dh10s"      	  = "BO3:BO"
		"UnknownPeg" 	  = "BP3:BP"
		"Fences"		  = "BQ3:BQ"
		"BeamShelf"		  = "BR3:BR"
		"RockMapleShelf"  = "BS3:BS"
		"SolidShelf"	  = "BT3:BT"
		"WireShelf"		  = "BU3:BU"
		"ExtenderPanel"	  = "BV3:BV"
		"RockMaplePanel"  = "BW3:BW"
		"WireBasket"	  = "BX3:BX"
		"ClipStrip"		  = "BY3:BY"
		"WireGrid"		  = "BZ3:BZ"
		"Bar"			  = "CA3:CA"
		"PegBoard"		  = "CB3:CB"
		"SlatWall"		  = "CC3:CC"
		"GravityFeed"	  = "CD3:CD"
		"ShelfBase"		  = "CE3:CE"
		"VendorFixture"	  = "CF3:CF"
		;

Run ;