%JobDate4(Ord050,Ord050,CASM Workbook,SC) ;
/* I made a change in test!! */

/*
	2020-JUL-10 - MC: CASM Equipment Workbook scheduled in Production. 
	2020-JUL-16 - MC: Edited the PDF to divide Shelves and Wirebaskets into a variety of sizes, and created a new
					  User input field for users to select appropriate fixture size (Code fields for shelves, dividers and baskets). 
	2020-JUL-22 - MC: Added in a section for Turns and Sales section, and added in a 24" code and value section
					  for Shelves and Wire Baskets. Also added in a 16" and 20" for Aerosol and Caulking fixtures.
*/

Libname Library "/sasdata/orm000/" Access=ReadOnly ;

Libname FCLIKB sqlsvr dsn="PCKB20172" user="sasadmin" password="77chevy$" DbMax_text=32000 Access=ReadOnly ;

Filename Equipmnt "/sasfiles/output/ord050/EquipmentWorkbook.xlsx" ;

Filename Formats "/sasdata/ord050/SasPgm/Prod/Ord050f.sas" ;

/* Filename Equipmnt "/sasfiles/output/oriadhoc/dor3381/CASM/EquipmentWorkbook.xlsx" ; */


%Include Formats ;

* Grab HABS Planogram details from CASM Space Planning Planograms table ;
Proc SQL ;
	Create Table Planograms As
		Select
			DbKey,
			Name,
			Desc1 As PlanDesc,
			Desc28 As PlanGroup,
			Cat(Round(Width / 12),"ft") As PlanSize,
			DatePart(Date2) As RemerchDate Format=ddmmmyy., 
			Capacity,
/* 			CapacityCost As RCC Format=Dollar32.2, */
/* 			CapacityRetail As SRP Format=Dollar32.2, */
			Notes,
			(UPPER(Notes) Like '%MERCHANDISING NOTE%') As MerchNoteFlag
		From FCLIKB.ix_spc_planogram
	Where
		BaseHeight > 0 And
		BaseWidth  > 0 And
		Status1 Like 'Live' And
		Desc29 Like '040 Genm' 
		
	Order By
		Name
		;
Quit ;

* Grab HABS Fixture Records from CASM Space Planning Fixture Records table ;
Proc SQL ;
	Create Table Fixtures As
		Select
			DbKey,
			DbParentPlanogramKey,
			Case
				When Upper(Name) Like '%BASE%' Then Upper(Cats(Put(Type, FixtureType.), ' - BASE'))
				When Upper(Name) Like '%VENDOR%' Then 'VENDOR FIXTURE'
				When Upper(Name) Like '%EXTENDER%' Then 'EXTENDER PANEL'
				When Upper(Name) Like '%CLIP%' Then 'CLIPSTRIP'
				When Upper(Name) Like '%SLAT%' Then 'SLATWALL'
				When Upper(Name) Like '%GRID%' Then 'WIRE GRID'
				When Upper(Name) Like '%AEROSOL%' Then 'AEROSOL RACK'
				When Upper(Name) Like '%CAULKING%' Then 'CAULKING RACK'
				When Upper(Name) Like '%ROCK%' Then 'ROCK MAPLE PANEL'
				When Upper(Name) Like '%RMAPLE%' Then 'ROCK MAPLE SHELF'
				When Upper(Name) Like '%BEAM SHELF%' Then 'BEAM SHELF'
				When Upper(Name) Like '%WIRE BASKET%' Then 'WIRE BASKET'
				When Upper(Name) Like '%WIRE SHELF%' Then 'WIRE SHELF'
				When Upper(Name) Like '%SOLID SHELF%' Then 'SOLID SHELF'
				When Upper(Name) Like '%XXX%' Then 'XXX'
				When Upper(Name) Like '%ZZZ%' Then 'ZZZ'
				Else Upper(Put(Type, FixtureType.)) 
			End As ModFixtureType,

			Case
				When Type In (4, 5, 9) Then 'HANGERS'
				When Type In (7, 8) Then 'PEGS'
				When Type In (6) Then 'BAR PEGS'
				Else ''
			End As PegNameForFixture,

			Cats(Put(Height, 32.1), 'in') As HeightS,
			Cats(Put(Width, 32.1), 'in') As WidthS,
			Cats(Put(Depth, 32.1), 'in') As DepthS,

			GrilleHeight,

			Cats(Put(Height, 32.1), 'in', Put(Width, 32.1), 'in', Put(Depth, 32.1), 'in') As ModFixtureHWD,

			Case
				When GrilleHeight > 0 Then Cats('Fence Height = ', Put(GrilleHeight, 32.1), 'in')
				Else ''
			End As FenceHeight,

			Case
				When GrilleHeight > 0 Then 1
				Else 0
			End As FenceCount,

			NumberOfDividers As Dividers,

			Cats(Put(DividerHeight, 32.1), 'in') As DividerHeights,
			Cats(Put(DividerWidth, 32.1), 'in') As DividerWidths,
			Cats(Put(DividerDepth, 32.1), 'in') As DividerDepths,

			Cats(Put(DividerHeight, 32.1), 'in', Put(DividerWidth, 32.1), 'in', Put(DividerDepth, 32.1)) As ModDividerHWD
			

		From FCLIKB.ix_spc_fixture
	Where 
		Type Not In (10, 11) And 
		Upper(Name) Not Like '%XXX%' And
		Upper(Name) Not Like '%ZZZ%' And
		Upper(Name) Not Like '%OBSTRUCTION%' And
		Upper(Name) Not Like '%SIGN%'
		;

Quit ;

* Get total ModFixtureType counts by Planogram group ;
Proc SQL ;
	Create Table FixtureCounts As
		Select
			DbParentPlanogramKey,
			ModFixtureType,
			Count(ModFixtureType) As FixtureCount,
			Sum(FenceCount) As FenceCount
		From Fixtures
	Where DbParentPlanogramKey In
		(Select DbKey From Planograms)
	Group By
		DbParentPlanogramKey,
		ModFixtureType
	Order By
		DbParentPlanogramKey
		;
			
Quit ;


*  Separate each fixture type into their own respective columns ;
Data FixtureRowToCol ;
	Set FixtureCounts ;

	By DbParentPlanogramKey ;

	If FixtureCount = . Then FixtureCount = 0 ;
		
	Select (ModFixtureType) ;
		When ("SHELF")  		  Shelf = FixtureCount ;
		When ("VENDOR FIXTURE")   VendorFixture = FixtureCount ;
		When ("EXTENDER PANEL")	  ExtenderPanel = FixtureCount ;
		When ("CLIPSTRIP")	      ClipStrip = FixtureCount ;
		When ("WIRE GRID")	      WireGrid = FixtureCount ;
		When ("AEROSOL RACK")	  AerosolRack = FixtureCount ;
		When ("CAULKING RACK")	  CaulkingRack = FixtureCount ;
		When ("ROCK MAPLE PANEL") RockMaplePanel = FixtureCount ;
		When ("ROCK MAPLE SHELF") RockMapleShelf = FixtureCount ;
		When ("BEAM SHELF")	      BeamShelf = FixtureCount ;
		When ("WIRE BASKET")	  WireBasket = FixtureCount ;
		When ("WIRE SHELF")		  WireShelf = FixtureCount ;
		When ("SOLID SHELF")	  SolidShelf = FixtureCount ;
		When ("BAR")			  Bar = FixtureCount ;
		When ("PEGBOARD")		  PegBoard = FixtureCount ;
		When ("SLATWALL")		  SlatWall = FixtureCount ;
		When ("GRAVITY FEED")     GravityFeed = FixtureCount ;
		When ("SHELF- BASE")      ShelfBase = FixtureCount ;
		Otherwise 		          Unknown = FixtureCount ;
	End ;

	Array toSummable _Numeric_ ;
	

	Drop
		ModFixtureType 
		FixtureCount
		;

Run ;

* Get Divider Counts from Fixtures Table ;
Proc SQL ;
	Create Table Divider As
		Select	Distinct
			DbParentPlanogramKey,
			Case
				When ModFixtureType Like '%WIRE BASKET%' Then "WIRE DIVIDER"
				Else "SHELF DIVIDER"
			End As DivType,
			Sum(Dividers) As Dividers
		From Fixtures
	Where 
		Dividers ~= 0 And
		Dividers ~= . And
		DbParentPlanogramKey In (Select DbKey From Planograms)
	Group By
		DbParentPlanogramKey,
		Case
			When ModFixtureType Like '%WIRE BASKET%' Then 1
			Else 2
		End
		;
Quit ;


Data Divider2 ;
	Set Divider ;

	Select (DivType) ;
		When ("SHELF DIVIDER")  ShelfDiv = Dividers ;
		When ("WIRE DIVIDER")  	WireDiv  = Dividers ;
		Otherwise			   	Unknowndiv  = Dividers ;
	End ;

	Drop 
		Dividers 
		;
Run ;

Proc SQL ;
	Create Table Divider3 As 
		Select
			DbParentPlanogramKey,
			Sum(ShelfDiv) As ShelfDiv,
			Sum(WireDiv) As WireDiv,
			Sum(UnknownDiv) As UnknownDiv
		From Divider2
	Group By
		DbParentPlanogramKey
		;
Quit ;


* Collapse Fixture table into a single record, as well as merge in Divider Table ;
Proc SQL ;
	Create Table FixtureFinal As
		Select Distinct
			Fix.DbParentPlanogramKey,
			
			Put(Sum(FenceCount), 32.) As Fences,

			Put(ShelfDiv, 32.) As ShelfDiv16,
			Put(ShelfDiv, 32.) As ShelfDiv18,
			Put(ShelfDiv, 32.) As ShelfDiv20,
			Put(ShelfDiv, 32.) As ShelfDiv22,
			Put(ShelfDiv, 32.) As ShelfDiv24,
			Put(ShelfDiv, 32.) As ShelfDiv27,

			Put(Sum(Shelf), 32.) As Shelf16,
			Put(Sum(Shelf), 32.) As Shelf18,
			Put(Sum(Shelf), 32.) As Shelf20,
			Put(Sum(Shelf), 32.) As Shelf22,
			Put(Sum(Shelf), 32.) As Shelf24,
			Put(Sum(Shelf), 32.) As Shelf27,			

			Put(WireDiv, 32.)  As WireDiv16,
			Put(WireDiv, 32.)  As WireDiv18,
			Put(WireDiv, 32.)  As WireDiv20,
			Put(WireDiv, 32.)  As WireDiv22,
			Put(WireDiv, 32.)  As WireDiv24,

			Put(Sum(WireBasket), 32.) As WireBasket16,
			Put(Sum(WireBasket), 32.) As WireBasket18,
			Put(Sum(WireBasket), 32.) As WireBasket20,
			Put(Sum(WireBasket), 32.) As WireBasket22,
			Put(Sum(WireBasket), 32.) As WireBasket24,

			Put(Sum(AerosolRack), 32.) As Aerosol16,
			Put(Sum(AerosolRack), 32.) As Aerosol20,

			Put(Sum(CaulkingRack), 32.) As Caulking16,
			Put(Sum(CaulkingRack), 32.) As Caulking20,

			Put(Sum(VendorFixture), 32.) As VendorFixture,
			Put(Sum(ExtenderPanel), 32.) As ExtenderPanel,

			Put(Sum(ClipStrip), 32.) As ClipStrip,
			Put(Sum(WireGrid), 32.) As WireGrid,
			Put(Sum(RockMaplePanel), 32.) As RockMaplePanel,
			Put(Sum(RockMapleShelf), 32.) As RockMapleShelf,
			Put(Sum(BeamShelf), 32.) As BeamShelf,
			
			Put(Sum(WireShelf), 32.) As WireShelf,
			Put(Sum(SolidShelf), 32.) As SolidShelf,
			Put(Sum(Bar), 32.) As Bar,
			Put(Sum(PegBoard), 32.) As PegBoard,
			Put(Sum(SlatWall), 32.) As SlatWall,
			Put(Sum(GravityFeed), 32.) As GravityFeed,
			Put(Sum(ShelfBase), 32.) As ShelfBase,
			Put(Sum(Unknown), 5.) As Unknown
		From FixtureRowToCol As Fix
	Left Join
		Divider3 As Div On
			Fix.DbParentPlanogramKey = Div.DbParentPlanogramKey
	Group By 
		Fix.DbParentPlanogramKey
		;
Quit ;



*************************************************************************** ;


* Get PEG counts in Position Table ;
Proc SQL ;
	Create Table Position As
		Select
			Pos.DbParentFixtureKey,
			Pos.DbParentPlanogramKey,
			PegNameForFixture,

			Case
				When Pos.PegId ~= '' Then Upper(Pos.PegId)
				When Prod.PegId ~= '' Then Upper(Prod.PegId)
				Else 'UNKNOWN'
			End As TypeOrUnknown,

			Case
				When Prod.Pegholes = 0 Or Prod.Pegholes = . Then 1
				Else Prod.Pegholes
			End As PegsPerFacing,
			
			HFacings,
			VFacings,
			Units
		From FCLIKB.ix_spc_position As Pos
	Inner Join 
		FCLIKB.ix_spc_product As Prod On 
			Prod.DbKey = Pos.DbParentProductKey
	Right Join 
		Fixtures As Fix On
			Fix.DbKey = Pos.DbParentFixtureKey And
			Fix.DbParentPlanogramKey = Pos.DbParentPlanogramKey
	Where 
		DbParentFixtureKey ~= . And
		Upper(Pos.PegId) Not Like 'CLIP'
		;

	Create Table Position As
		Select 
			DbParentFixtureKey,
			DbParentPlanogramKey,
			TypeOrUnknown,
			Case
				When PegNameForFixture Like 'PEGS' Then HFacings * VFacings * PegsPerFacing
				When PegNameForFixture Like 'BAR PEGS' Then HFacings * PegsPerFacing
				Else Units
			End As PeggedUnitCount,
			
			Upper(Cats(PegNameForFixture, '~', TypeOrUnknown)) As GroupBy,
			PegNameForFixture
		From Position
	Where 
		PegNameForFixture ~= ''
		;

	Create Table Position As
		Select Distinct
			DbParentPlanogramKey,
			PegNameForFixture As ModFixtureType,
			TypeOrUnknown,
			Sum(PeggedUnitCount) As Number
		From Position As Pos
	Group By
		DbParentPlanogramKey,
		GroupBy
		;
Quit ;


* Convert each Peg Type to have their own respective columns ;
Data PositionColToRow ;
	Set Position ;
	
	Select (TypeOrUnknown) ;
		When ("PEG3S")  Hook3  = Number ;
		When ("PEG4S")  Hook4  = Number ;
		When ("PEG6S")  Hook6  = Number ;
		When ("PEG8")   Hook8  = Number ;
		When ("PEG9S")  Hook9  = Number ;
		When ("PEG12S") Hook12 = Number ;

		When("PEG2")      PegHook2 = Number ;
		When("PEG3")      PegHook3 = Number ;
		When("PEG4")      PegHook4 = Number ;
		When("PEG6")      PegHook6 = Number ;
		When("PEG9")      PegHook9 = Number ;
		When("PEG12")     PegHook12 = Number ;
		When("PEG15")     PegHook15 = Number ;
		When("PEG16")     PegHook16 = Number ;
		When("PEG18")     PegHook18 = Number ;

		When("PEG12SH")   PegHookHD12 = Number ;
		When("PEG16SH")   PegHookHD16 = Number ;

		When("****")     PegHook = Number ;

		When("BDSMA")    Bdsma = Number ;
		When("HOE")      Hoe = Number ;
		When("6WG")      SWG = Number ;
		When("CM")       Cm  = Number ;
		When("HANGERS")  Hangers = Number ;
		When("DHANDLE")  DHandle = Number ;
		When("CH")       Ch      = Number ;
		When("PEG12SCB") Peg12Scb = Number ;
		When("BDMDA")    Bdmda = Number ;
		When("PD")       Pd = Number ;
		When("SHOVEL")   Shovel = Number ;
		When("HAMMER")   Hammer = Number ;
		When("CLIP")     Clip = Number ;
		When("RACK")     Rack = Number ;
		When("JH")       Jh = Number ;
		When("DH10S")    Dh10s = Number ;

		When("16BH")    BH16 = Number ;
		Otherwise 	    UnknownPeg = Number ;
	End ;	

	Drop
		ModFixtureType
		Number
		;
Run ;

* Collapse Peg table into a single record by Planogram ;
Proc SQL ;
	Create Table PositionFinal As
		Select
			DbParentPlanogramKey,
			Put(Sum(Hook3), 32.) As Hook3,
			Put(Sum(Hook4), 32.) As Hook4,
			Put(Sum(Hook6), 32.) As Hook6,
			Put(Sum(Hook8), 32.) As Hook8,			
			Put(Sum(Hook9), 32.) As Hook9,
			Put(Sum(Hook12), 32.) As Hook12,
			Put(Sum(PegHook2), 32.) As PegHook2,
			Put(Sum(PegHook3), 32.) As PegHook3,
			Put(Sum(PegHook4), 32.) As PegHook4,
			Put(Sum(PegHook6), 32.) As PegHook6,
			Put(Sum(PegHook9), 32.) As PegHook9,
			Put(Sum(PegHook12), 32.) As PegHook12,
			Put(Sum(PegHook15), 32.) As PegHook15,
			Put(Sum(PegHook16), 32.) As PegHook16,
			Put(Sum(PegHook18), 32.) As PegHook18,
			Put(Sum(PegHookHD12), 32.) As PegHookHD12,
			Put(Sum(PegHookHD16), 32.)  As PegHookHD16,
			Put(Sum(PegHook), 32.) As PegHook,
			Put(Sum(Bdsma), 32.) As Bdsma,
			Put(Sum(Hoe), 32.) As Hoe,
			Put(Sum(SWG), 32.) As SWG,
			Put(Sum(Cm), 32.) As Cm,
			Put(Sum(Hangers), 32.) As Hangers,
			Put(Sum(DHandle), 32.) As DHandle,
			Put(Sum(Ch), 32.) As Ch,
			Put(Sum(Peg12Scb), 32.) As Peg12Scb,
			Put(Sum(Bdmda), 32.) As Bdmda,
			Put(Sum(Pd), 32.) As Pd,
			Put(Sum(Shovel), 32.) As Shovel,
			Put(Sum(Hammer), 32.) As Hammer,
			Put(Sum(Clip), 32.) As Clip,
			Put(Sum(Rack), 32.) As Rack,
			Put(Sum(Jh), 32.) As Jh,
			Put(Sum(Dh10s), 32.) As Dh10s,
			Put(Sum(BH16), 32.) As BH16,
			Put(Sum(UnknownPeg), 32.) As UnknownPeg
		From PositionColToRow
	Where DbParentPlanogramKey In (Select DbKey From Planograms)
	Group By
		DbParentPlanogramKey
		;
Quit ;

* Merge Planogram and Fixture Tables ;
Proc SQL ;
	Create Table PlanFixture As
		Select 
			Plan.DbKey,
			PlanGroup,
			Name,
			PlanDesc,
			PlanSize,
			RemerchDate,

			MerchNoteFlag,
			Fix.*,
			Pos.*
		From Planograms As Plan
	Left Join
		FixtureFinal As Fix On
			Fix.DbParentPlanogramKey = Plan.DbKey
	Left Join
		PositionFinal As Pos On
			Pos.DbParentPlanogramKey = Plan.DbKey
	Order By
		Plan.DbKey
		;
Quit ;

* Get RCC And SRP Pricing from Performance table;
Proc SQL ;
	Create Table RCCSRP As
		Select
			Plan.dbKey,
			Sum(Perf.Capacity * Prod.UnitCost) As ActRCC,
			Sum(Perf.Capacity * Prod.Price)    As ActSRP
		From FCLIKB.ix_spc_performance As Perf
	Inner Join
		FCLIKB.ix_spc_planogram As Plan On
			Plan.dbKey = Perf.dbParentPlanogramKey
	Inner Join
		FCLIKB.ix_spc_product As Prod On
			Prod.dbKey = Perf.DbParentProductKey
	Where 
		Perf.Capacity > 0
	Group By
		Plan.dbKey
		;
Quit ;



Proc SQL ;
	Create Table EquipmentWorkbookV2 As
		Select
			"" As Turns Format=$32. Length=32,
			"" As PlanFlag,

			"" As ShelfCode16,
			"" As ShelfCode18,
			"" As ShelfCode20,
			"" As ShelfCode22,
			"" As ShelfCode24,
			"" As ShelfCode27,
			
			"" As WireCode16,
			"" As WireCode18,
			"" As WireCode20,
			"" As WireCode22,
			"" As WireCode24,

			"" As AerosolCode16,
			"" As AerosolCode20,

			"" As CaulkingCode16,
			"" As CaulkingCode20,

			EW.*,
			Put(ActRCC, 32.2) As ActRCC,
			Put(ActSRP, 32.2) As ActSRP,
			Put((ActSRP - ActRCC) / ActSRP, Percent32.2) As Margin
		From PlanFixture As EW
	Left Join 
		RCCSRP As RS On
			EW.DbKey = RS.DbKey
	Where
		PlanGroup Not In ("PBE" "ARC") And
		Upper(SubStr(Name, 1)) Not Like "I%" 
	Order By
		PlanGroup,
		Compress(Name, '_', 'D'),
		Input(Compress(Name, '_', 'A'), 10.)
		;
	
Quit ;


/*
	Divide Plan groups into smaller pieces to avoid Excel formula truncation 
		- The maximum string format length for numeric column fields is $32. Anything numeric with format higher than $32 is read as string in the Excel sheet,
		  causing the formulas to not work.

*/
Data EquipmentWorkbookV3a ;
	Set EquipmentWorkbookV2 ;

	By PlanGroup ;

	Where PlanGroup Not In ("PBE" "ARC") ;

	Retain
		Counter 2
		SubGroup 0
		;

	If First.PlanGroup Then Counter = 2 ;

	If Last.Plangroup Then Do ;
		SubGroup = 0 ;
	End ;
	

	If Counter > 96 Then Do ;
		SubGroup + 1 ;
		Counter = 2 ;
	End ;

	If SubGroup > 0 Then SubPlanGroup = Cats(PlanGroup, "_", SubGroup) ;
	Else SubPlanGroup = PlanGroup ;

	Counter + 1 ;

	Drop
		PlanGroup
		Counter
		SubGroup
		;

Run ;

Proc SQL ;
	Create Table EquipmentWorkbookV3a As
		Select
			*
		From EquipmentWorkbookV3a
	Order By
		SubPlanGroup,
		Compress(Name, '_', 'D'),
		Input(Compress(Name, '_', 'A'), 10.)
		;
	
Quit ;

Data EquipmentWorkbookV3 ;
	Set EquipmentWorkbookV3a ;
	By SubPlanGroup ;

	Retain 
		Counter 3 
		;

	Sales = Cats("=",Put("ActSRP", $CellPos.), Counter, "*", Put("Turns", $CellPos.), Counter) ;

	Output ;
	

	If Last.SubPlanGroup Then Do ;

		Name = "" ;
	 	PlanSize = "" ;
	    PlanDesc = "Total" ;
		RemerchDate = . ;

		ActRCC = %CreatePlanFormula(ActRCC) ;
	    ActSRP = %CreatePlanFormula(ActSRP) ;

		Margin = Trim(Cats("=(", Put("ActSRP", $CellPos.), Counter + 1, "-", Put("ActRCC", $CellPos.), Counter + 1, ")/", Put("ActSRP", $CellPos.), Counter + 1, "*100")) ;
		
		Turns= Trim(Cats("=", Put("Sales", $CellPos.), Counter + 1, "/", Put("ActSRP", $CellPos.), Counter + 1)) ;

		Sales =  %CreatePlanFormula(Sales) ;

		MerchNoteFlag = "" ;
		
		Shelf16 = %CreateSWFormula(L, Shelf16)
		Shelf18 = %CreateSWFormula(M, Shelf18)
		Shelf20 = %CreateSWFormula(N, Shelf20)
		Shelf22 = %CreateSWFormula(O, Shelf22)
		Shelf24 = %CreateSWFormula(P, Shelf24)
		Shelf27 = %CreateSWFormula(Q, Shelf27)
		
		ShelfDiv16 = %CreateSWFormula(L, ShelfDiv16)
		ShelfDiv18 = %CreateSWFormula(M, ShelfDiv18)
		ShelfDiv20 = %CreateSWFormula(N, ShelfDiv20)
		ShelfDiv22 = %CreateSWFormula(O, ShelfDiv22)
		ShelfDiv24 = %CreateSWFormula(P, ShelfDiv24)
		ShelfDiv27 = %CreateSWFormula(Q, ShelfDiv27)
		
		WireBasket16 = %CreateSWFormula(R, WireBasket16)
		WireBasket18 = %CreateSWFormula(S, WireBasket18)
		WireBasket20 = %CreateSWFormula(T, WireBasket20)
		WireBasket22 = %CreateSWFormula(U, WireBasket22)
		WireBasket24 = %CreateSWFormula(V, WireBasket24)
		
		WireDiv16 = %CreateSWFormula(R, WireDiv16)
		WireDiv18 = %CreateSWFormula(S, WireDiv18)
		WireDiv20 = %CreateSWFormula(T, WireDiv20)
		WireDiv22 = %CreateSWFormula(U, WireDiv22)
		WireDiv24 = %CreateSWFormula(V, WireDiv24)

		Aerosol16 = %CreateSWFormula(W, Aerosol16)
		Aerosol20 =	%CreateSWFormula(X, Aerosol20)

		Caulking16 = %CreateSWFormula(Y, Caulking16)
		Caulking20 = %CreateSWFormula(Z, Caulking20)

		PegHook   =  %CreatePlanFormula(PegHook)
		PegHook2  =  %CreatePlanFormula(PegHook2)
		PegHook3  =  %CreatePlanFormula(PegHook3)
		PegHook4  =  %CreatePlanFormula(PegHook4)
		PegHook6  =  %CreatePlanFormula(PegHook6)
		PegHook9  =  %CreatePlanFormula(PegHook9)
		PegHook12 =  %CreatePlanFormula(PegHook12)
		PegHook15 =  %CreatePlanFormula(PegHook15)
		PegHook16 =  %CreatePlanFormula(PegHook16)
		PegHook18 =  %CreatePlanFormula(PegHook18)
		
		Hook3  =  %CreatePlanFormula(Hook3)
		Hook4  =  %CreatePlanFormula(Hook4)
		Hook6  =  %CreatePlanFormula(Hook6)
		Hook8  =  %CreatePlanFormula(Hook8)
		Hook9  =  %CreatePlanFormula(Hook9)
		Hook12 =  %CreatePlanFormula(Hook12)
		
		PegHookHD12 = %CreatePlanFormula(PegHookHD12)
		PegHookHD16 = %CreatePlanFormula(PegHookHD16)
		
		BH16 	   = %CreatePlanFormula(BH16)
		
		Bdsma 	   = %CreatePlanFormula(Bdsma)
		Hoe 	   = %CreatePlanFormula(Hoe)
		SWG 	   = %CreatePlanFormula(SWG)
		Cm         = %CreatePlanFormula(Cm)
		Hangers    = %CreatePlanFormula(Hangers)
		DHandle    = %CreatePlanFormula(DHandle)
		Ch 		   = %CreatePlanFormula(Ch)
		Peg12Scb   = %CreatePlanFormula(Peg12Scb)
		Bdmda 	   = %CreatePlanFormula(Bdmda)
		Pd 		   = %CreatePlanFormula(Pd)
		Shovel 	   = %CreatePlanFormula(Shovel)
		Hammer 	   = %CreatePlanFormula(Hammer)
		Clip 	   = %CreatePlanFormula(Clip)
		Rack       = %CreatePlanFormula(Rack)
		Jh 		   = %CreatePlanFormula(Jh)
		Dh10s 	   = %CreatePlanFormula(Dh10s)
		UnknownPeg = %CreatePlanFormula(UnknownPeg)
		
		Dividers		= %CreatePlanFormula(Dividers)
		Fences			= %CreatePlanFormula(Fences)
		Shelf			= %CreatePlanFormula(Shelf)
		BeamShelf		= %CreatePlanFormula(BeamShelf)
		RockMapleShelf	= %CreatePlanFormula(RockMapleShelf)
		SolidShelf		= %CreatePlanFormula(SolidShelf)
		WireShelf		= %CreatePlanFormula(WireShelf)
		ExtenderPanel	= %CreatePlanFormula(ExtenderPanel)
		RockMaplePanel  = %CreatePlanFormula(RockMaplePanel)

		AerosolRack   	= %CreatePlanFormula(AerosolRack)
		CaulkingRack  	= %CreatePlanFormula(CaulkingRack)

		WireBasket		= %CreatePlanFormula(WireBasket)
		ClipStrip     	= %CreatePlanFormula(ClipStrip)
		WireGrid      	= %CreatePlanFormula(WireGrid)
		Bar           	= %CreatePlanFormula(Bar)
		PegBoard      	= %CreatePlanFormula(PegBoard)
		SlatWall  		= %CreatePlanFormula(SlatWall)
		GravityFeed   	= %CreatePlanFormula(GravityFeed)
		ShelfBase     	= %CreatePlanFormula(ShelfBase)
		VendorFixture	= %CreatePlanFormula(VendorFixture)

		Output ;
	End ;

	Counter + 1 ;

	If Last.SubPlanGroup Then Counter = 3 ;

Run ;

* Create Summary tab list that tallies up the totals for each Excel tabs ;
Data PlanList ;
	Set EquipmentWorkbookV3 Nobs=Nobs;
	
	By SubPlanGroup ;

	Retain
		UniqueCount 2 ;

	If Last.SubPlanGroup Then Do ;
		Summaries = "Summary" ;
		Counter + 1 ;
		UniqueCount + 1 ;	

		ActRCC = %CreateSummarySumFormula(ActRCC)
	    ActSRP = %CreateSummarySumFormula(ActSRP) 

		Margin = %CreateSummarySumFormula(Margin)

		Turns  = %CreateSummarySumFormula(Turns)

		Sales  = %CreateSummarySumFormula(Sales)

		Shelf16 = %CreateSummarySumFormula(Shelf16)
		Shelf18 = %CreateSummarySumFormula(Shelf18)
		Shelf20 = %CreateSummarySumFormula(Shelf20)
		Shelf22 = %CreateSummarySumFormula(Shelf22)
		Shelf24 = %CreateSummarySumFormula(Shelf24)
		Shelf27 = %CreateSummarySumFormula(Shelf27)
		
		ShelfDiv16 = %CreateSummarySumFormula(ShelfDiv16)
		ShelfDiv18 = %CreateSummarySumFormula(ShelfDiv18)
		ShelfDiv20 = %CreateSummarySumFormula(ShelfDiv20)
		ShelfDiv22 = %CreateSummarySumFormula(ShelfDiv22)
		ShelfDiv24 = %CreateSummarySumFormula(ShelfDiv24)
		ShelfDiv27 = %CreateSummarySumFormula(ShelfDiv27)
		
		WireBasket16 = %CreateSummarySumFormula(WireBasket16)
		WireBasket18 = %CreateSummarySumFormula(WireBasket18)
		WireBasket20 = %CreateSummarySumFormula(WireBasket20)
		WireBasket22 = %CreateSummarySumFormula(WireBasket22)
		WireBasket24 = %CreateSummarySumFormula(WireBasket24)
		
		WireDiv16 = %CreateSummarySumFormula(WireDiv16)
		WireDiv18 = %CreateSummarySumFormula(WireDiv18)
		WireDiv20 = %CreateSummarySumFormula(WireDiv20)
		WireDiv22 = %CreateSummarySumFormula(WireDiv22)
		WireDiv24 = %CreateSummarySumFormula(WireDiv24)

		Aerosol16 = %CreateSummarySumFormula(Aerosol16)
		Aerosol20 = %CreateSummarySumFormula(Aerosol20)

		Caulking16 = %CreateSummarySumFormula(Caulking16)
		Caulking20 = %CreateSummarySumFormula(Caulking20)

		PegHook   = %CreateSummarySumFormula(PegHook)
		PegHook2  = %CreateSummarySumFormula(PegHook2)
		PegHook3  = %CreateSummarySumFormula(PegHook3)
		PegHook4  = %CreateSummarySumFormula(PegHook4)
		PegHook6  = %CreateSummarySumFormula(PegHook6)
		PegHook9  = %CreateSummarySumFormula(PegHook9)
		PegHook12 = %CreateSummarySumFormula(PegHook12)
		PegHook15 = %CreateSummarySumFormula(PegHook15)
		PegHook16 = %CreateSummarySumFormula(PegHook16)
		PegHook18 = %CreateSummarySumFormula(PegHook18)
		
		Hook3  = %CreateSummarySumFormula(Hook3)
		Hook4  = %CreateSummarySumFormula(Hook4)
		Hook6  = %CreateSummarySumFormula(Hook6)
		Hook8  = %CreateSummarySumFormula(Hook8)
		Hook9  = %CreateSummarySumFormula(Hook9)
		Hook12 = %CreateSummarySumFormula(Hook12)
		
		PegHookHD12 = %CreateSummarySumFormula(PegHookHD12)
		PegHookHD16 = %CreateSummarySumFormula(PegHookHD16)
		
		BH16 	   = %CreateSummarySumFormula(BH16)
		Bdsma 	   = %CreateSummarySumFormula(Bdsma)
		Hoe 	   = %CreateSummarySumFormula(Hoe)
		SWG 	   = %CreateSummarySumFormula(SWG)
		Cm         = %CreateSummarySumFormula(Cm)
		Hangers    = %CreateSummarySumFormula(Hangers)
		DHandle    = %CreateSummarySumFormula(DHandle)
		Ch 		   = %CreateSummarySumFormula(Ch)
		Peg12Scb   = %CreateSummarySumFormula(Peg12Scb)
		Bdmda 	   = %CreateSummarySumFormula(Bdmda)
		Pd 		   = %CreateSummarySumFormula(Pd)
		Shovel 	   = %CreateSummarySumFormula(Shovel)
		Hammer 	   = %CreateSummarySumFormula(Hammer)
		Clip 	   = %CreateSummarySumFormula(Clip)
		Rack       = %CreateSummarySumFormula(Rack)
		Jh 		   = %CreateSummarySumFormula(Jh)
		Dh10s 	   = %CreateSummarySumFormula(Dh10s)
		UnknownPeg = %CreateSummarySumFormula(UnknownPeg)
		
		Fences			= %CreateSummarySumFormula(Fences)
		BeamShelf		= %CreateSummarySumFormula(BeamShelf)
		RockMapleShelf	= %CreateSummarySumFormula(RockMapleShelf)
		SolidShelf		= %CreateSummarySumFormula(SolidShelf)
		WireShelf		= %CreateSummarySumFormula(WireShelf)
		ExtenderPanel	= %CreateSummarySumFormula(ExtenderPanel)
		RockMaplePanel  = %CreateSummarySumFormula(RockMaplePanel)
		AerosolRack   	= %CreateSummarySumFormula(AerosolRack)
		CaulkingRack  	= %CreateSummarySumFormula(CaulkingRack)
		WireBasket		= %CreateSummarySumFormula(WireBasket)
		ClipStrip     	= %CreateSummarySumFormula(ClipStrip)
		WireGrid      	= %CreateSummarySumFormula(WireGrid)
		Bar           	= %CreateSummarySumFormula(Bar)
		PegBoard      	= %CreateSummarySumFormula(PegBoard)
		SlatWall  		= %CreateSummarySumFormula(SlatWall)
		GravityFeed   	= %CreateSummarySumFormula(GravityFeed)
		ShelfBase     	= %CreateSummarySumFormula(ShelfBase)
		VendorFixture	= %CreateSummarySumFormula(VendorFixture)

		Output ;
	End ;

	If _n_ = Nobs Then Do ;
		SubPlanGroup = "Total" ;
		Counter = UniqueCount ;

		ActRCC = %CreateSummaryTotalFormula(ActRCC)
	    ActSRP = %CreateSummaryTotalFormula(ActSRP) 

		Margin = Trim(Cats("=(C", Counter + 1, "-B", Counter + 1, ")/C", Counter + 1, "*100")) ;

		Turns  = Trim(Cats("=F", Counter + 1, "/C", Counter + 1)) ;

		Sales  = %CreateSummaryTotalFormula(Sales)

		Shelf16 = %CreateSummaryTotalFormula(Shelf16)
		Shelf18 = %CreateSummaryTotalFormula(Shelf18)
		Shelf20 = %CreateSummaryTotalFormula(Shelf20)
		Shelf22 = %CreateSummaryTotalFormula(Shelf22)
		Shelf24 = %CreateSummaryTotalFormula(Shelf24)
		Shelf27 = %CreateSummaryTotalFormula(Shelf27)
		
		ShelfDiv16 = %CreateSummaryTotalFormula(ShelfDiv16)
		ShelfDiv18 = %CreateSummaryTotalFormula(ShelfDiv18)
		ShelfDiv20 = %CreateSummaryTotalFormula(ShelfDiv20)
		ShelfDiv22 = %CreateSummaryTotalFormula(ShelfDiv22)
		ShelfDiv24 = %CreateSummaryTotalFormula(ShelfDiv24)
		ShelfDiv27 = %CreateSummaryTotalFormula(ShelfDiv27)
		
		WireBasket16 = %CreateSummaryTotalFormula(WireBasket16)
		WireBasket18 = %CreateSummaryTotalFormula(WireBasket18)
		WireBasket20 = %CreateSummaryTotalFormula(WireBasket20)
		WireBasket22 = %CreateSummaryTotalFormula(WireBasket22)
		WireBasket24 = %CreateSummaryTotalFormula(WireBasket24)

		WireDiv16 = %CreateSummaryTotalFormula(WireDiv16)
		WireDiv18 = %CreateSummaryTotalFormula(WireDiv18)
		WireDiv20 = %CreateSummaryTotalFormula(WireDiv20)
		WireDiv22 = %CreateSummaryTotalFormula(WireDiv22)
		WireDiv24 = %CreateSummaryTotalFormula(WireDiv24)

		Aerosol16 = %CreateSummaryTotalFormula(Aerosol16)
		Aerosol20 = %CreateSummaryTotalFormula(Aerosol20)

		Caulking16 = %CreateSummaryTotalFormula(Caulking16)
		Caulking20 = %CreateSummaryTotalFormula(Caulking20)
		
		PegHook     = %CreateSummaryTotalFormula(PegHook)
		PegHook2    = %CreateSummaryTotalFormula(PegHook2)
		PegHook3    = %CreateSummaryTotalFormula(PegHook3)
		PegHook4    = %CreateSummaryTotalFormula(PegHook4)
		PegHook6    = %CreateSummaryTotalFormula(PegHook6)
		PegHook9    = %CreateSummaryTotalFormula(PegHook9)
		PegHook12   = %CreateSummaryTotalFormula(PegHook12)
		PegHook15   = %CreateSummaryTotalFormula(PegHook15)
		PegHook16   = %CreateSummaryTotalFormula(PegHook16)
		PegHook18   = %CreateSummaryTotalFormula(PegHook18)
		
		Hook3       = %CreateSummaryTotalFormula(Hook3) 
		Hook4       = %CreateSummaryTotalFormula(Hook4) 
		Hook6       = %CreateSummaryTotalFormula(Hook6) 
		Hook8       = %CreateSummaryTotalFormula(Hook8) 
		Hook9       = %CreateSummaryTotalFormula(Hook9) 
		Hook12      = %CreateSummaryTotalFormula(Hook12) 
		PegHookHD12 = %CreateSummaryTotalFormula(PegHookHD12)
		PegHookHD16 = %CreateSummaryTotalFormula(PegHookHD16)
		
		BH16 	    = %CreateSummaryTotalFormula(BH16)
		Bdsma 	    = %CreateSummaryTotalFormula(Bdsma)
		Hoe 	    = %CreateSummaryTotalFormula(Hoe)
		SWG 	    = %CreateSummaryTotalFormula(SWG)
		Cm          = %CreateSummaryTotalFormula(Cm)
		Hangers     = %CreateSummaryTotalFormula(Hangers)
		DHandle     = %CreateSummaryTotalFormula(DHandle)
		Ch 		    = %CreateSummaryTotalFormula(Ch)
		Peg12Scb    = %CreateSummaryTotalFormula(Peg12Scb)
		Bdmda 	    = %CreateSummaryTotalFormula(Bdmda)
		Pd 		    = %CreateSummaryTotalFormula(Pd)
		Shovel 	    = %CreateSummaryTotalFormula(Shovel)
		Hammer 	    = %CreateSummaryTotalFormula(Hammer)
		Clip 	    = %CreateSummaryTotalFormula(Clip)
		Rack        = %CreateSummaryTotalFormula(Rack)
		Jh 		    = %CreateSummaryTotalFormula(Jh)
		Dh10s 	    = %CreateSummaryTotalFormula(Dh10s)
		UnknownPeg  = %CreateSummaryTotalFormula(UnknownPeg)
		
		Fences			= %CreateSummaryTotalFormula(Fences)
		BeamShelf		= %CreateSummaryTotalFormula(BeamShelf)
		RockMapleShelf	= %CreateSummaryTotalFormula(RockMapleShelf)
		SolidShelf		= %CreateSummaryTotalFormula(SolidShelf)
		WireShelf		= %CreateSummaryTotalFormula(WireShelf)
		ExtenderPanel	= %CreateSummaryTotalFormula(ExtenderPanel)
		RockMaplePanel  = %CreateSummaryTotalFormula(RockMaplePanel)
		AerosolRack   	= %CreateSummaryTotalFormula(AerosolRack)
		CaulkingRack  	= %CreateSummaryTotalFormula(CaulkingRack)
		WireBasket		= %CreateSummaryTotalFormula(WireBasket)
		ClipStrip     	= %CreateSummaryTotalFormula(ClipStrip)
		WireGrid      	= %CreateSummaryTotalFormula(WireGrid)
		Bar           	= %CreateSummaryTotalFormula(Bar)
		PegBoard      	= %CreateSummaryTotalFormula(PegBoard)
		SlatWall  		= %CreateSummaryTotalFormula(SlatWall)
		GravityFeed   	= %CreateSummaryTotalFormula(GravityFeed)
		ShelfBase     	= %CreateSummaryTotalFormula(ShelfBase)
		VendorFixture	= %CreateSummaryTotalFormula(VendorFixture)

		Output ;
	End ;

	Keep
		Summaries

		ActRCC
		ActSRP

		Margin

		Turns 
		Sales 

		Shelf16
		Shelf18
		Shelf20
		Shelf22
		Shelf24
		Shelf27

		ShelfDiv16 
		ShelfDiv18 
		ShelfDiv20 
		ShelfDiv22 
		ShelfDiv24
		ShelfDiv27 

		WireBasket16
		WireBasket18
		WireBasket20
		WireBasket22
		WireBasket24

		WireDiv16
		WireDiv18
		WireDiv20
		WireDiv22
		WireDiv24

		Aerosol16
		Aerosol20

		Caulking16
		Caulking20

		SubPlanGroup

		PegHook	  PegHook2  PegHook3 
		PegHook4  PegHook6  PegHook9 
		PegHook12 PegHook15 PegHook16
		PegHook18 
	
		Hook3  Hook4  Hook6 
		Hook8  Hook9  Hook12

		PegHookHD12 PegHookHD16
		
		BH16    Bdsma   Hoe    SWG      Cm        
		Hangers DHandle Ch 	   Peg12Scb Bdmda 	  
		Pd 		Shovel 	Hammer Clip 	Rack      
		Jh 		Dh10s 	UnknownPeg

		Dividers    Fences 		   Shelf			
		BeamShelf   RockMapleShelf SolidShelf		
		WireShelf   ExtenderPanel  RockMaplePanel  
		AerosolRack CaulkingRack   WireBasket		
		ClipStrip   WireGrid       Bar           	
		PegBoard    SlatWall  	   GravityFeed   	
		ShelfBase   VendorFixture	
		;
	
Run ;



Options NoByline ;

Ods Listing Close ;
Ods Excel File=Equipmnt Options(
	Sheet_Interval="bygroup" 
	sheet_name="#byval1" 
	Absolute_Column_Width="10,
							17,5,51,21,14,14,8,8,8,17,
							 4,4,4,4,4,4,
							  4,4,4,4,4,
							   7,7,7,7,
							     4,4,4,4,4,4,4,4,4,4,4,4,
								  4,4,4,4,4,4,4,4,4,4,
									4,4,4,4,
								9,3,3,3,3,3,4,4,4,4,
							 	 3,3,3,3,3,4,
							  	  8,8,
							   	   16,14,4,26,10,8,9,14,18,18,16,8,8,8,17,15,27,24,
									7,11,12,11,10,15,12,10,9,9,5,9,14,10,15") ;
	Proc Report Data=EquipmentWorkbookV3 Nowd NoCenter Missing Out=Temp ;
		By SubPlanGroup ;

		Format
			MerchNoteFlag Bool.
			RemerchDate ddmmmyy.
			;

		Label
			PlanFlag = "Code Plan"
			Name = "Planogram Name"
			PlanDesc = "Description"
			PlanSize = "Size"
			RemerchDate = "Remerchandising Date"
			ActRCC = "Total RCC"
			ActSRP = "Total SRP"

			MerchNoteFlag = "Has Merchandising Note?"

			ShelfCode16 = '16"'
			ShelfCode18 = '18"'
			ShelfCode20 = '20"'
			ShelfCode22 = '22"'
			ShelfCode24 = '24"'
			ShelfCode27 = '27"'

			WireCode16 = '16"'
			WireCode18 = '18"'
			WireCode20 = '20"'
			WireCode22 = '22"'
			WireCode24 = '24"'

			AerosolCode16 = '16"'
			AerosolCode20 = '20"'

			CaulkingCode16 = '16"'
			CaulkingCode20 = '20"'

			%OutLabels ;
			;

		Column
			PlanFlag

			("" Name PlanSize PlanDesc RemerchDate ActRCC ActSRP Margin Turns Sales MerchNoteFlag)

			("Code - Shelf & Dividers" ShelfCode16 ShelfCode18 ShelfCode20 ShelfCode22 ShelfCode24 ShelfCode27)
			("Code - Baskets & Dividers" WireCode16 WireCode18 WireCode20 WireCode22 WireCode24)
			("Code - Aerosol"  AerosolCode16 AerosolCode20)			
			("Code - Caulking" CaulkingCode16 CaulkingCode20)		
	
			%ReportColOrder ;
			;

		Define PlanDesc / Display Style(column)={vjust=t just=c Width=3in} ;

		Compute PlanDesc ;
			If Upcase(PlanDesc) = "TOTAL" Then Do ;
				Call Define(_ROW_, 'style', 'style={font_weight=bold background=lightyellow}');
			End ;
		Endcomp ;
	Run ;

	Ods Excel Options(Sheet_Name="Summary"
		Absolute_Column_Width="10,
								14,14,8,8,8,
								4,4,4,4,4,4,4,4,4,4,4,4,
								 4,4,4,4,4,4,4,4,4,4,
								 5,5,5,5,
								  9,3,3,3,3,3,4,4,4,4,
							 	   3,3,3,3,3,4,
							  		8,8,
							   		 16,14,4,26,10,8,9,14,18,18,16,8,8,8,17,15,27,24,
									  7,11,12,11,10,15,12,9,9,7,5,10,9,12,10,14") ;	

	Proc Report Data=PlanList Nowd NoCenter Missing Out=Temp ;

		Label
			SubPlanGroup = "Plan"

			ActRCC = "Total RCC"
			ActSRP = "Total SRP"

			%OutLabels 
			;

		Column
			SubPlanGroup

			(ActRCC ActSRP Margin Turns Sales)

			%ReportColOrder
			;

		Define SubPlanGroup / Display Center ;

		Compute SubPlanGroup ;
			If Upcase(SubPlanGroup) = "TOTAL" Then Do ;
				Call Define(_ROW_, 'style', 'style={font_weight=bold background=lightyellow}');
			End ;
		Endcomp ;

	Run ;
Ods Listing Close ;
Ods _ALL_ Close ;


